package me.wetterbericht.json;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class JsonData {

    private final static Gson GSON_EXCLUSION = new GsonBuilder().setPrettyPrinting().setExclusionStrategies(new GsonExcludeExclusionStrategy()).serializeNulls().create();

    private final static Gson GSON = new GsonBuilder().setPrettyPrinting().serializeNulls().create();

    private final static Gson GSON_NOT_PRETTY = new GsonBuilder().setExclusionStrategies(new GsonExcludeExclusionStrategy()).serializeNulls().create();

    private final static Gson GSON_NOT_PRETTY_EXCLUSION = new GsonBuilder().setExclusionStrategies(new GsonExcludeExclusionStrategy()).serializeNulls().create();

    private Gson gson;

    private JsonObject jsonObject = new JsonObject();

    public JsonData() {
        this.gson = GSON;
    }

    public JsonData(JsonObject jsonObject) {
        this.jsonObject = jsonObject;
        this.gson = GSON;
    }

    public JsonData useExclusionStrategy() {
        if (gson.equals(GSON_NOT_PRETTY) || gson.equals(GSON_NOT_PRETTY_EXCLUSION)) {
            this.gson = GSON_NOT_PRETTY_EXCLUSION;
        } else {
            this.gson = GSON_EXCLUSION;
        }
        return this;
    }

    public JsonData unpretty() {
        if (gson.equals(GSON_EXCLUSION) || gson.equals(GSON_NOT_PRETTY_EXCLUSION)) {
            this.gson = GSON_NOT_PRETTY_EXCLUSION;
        } else {
            this.gson = GSON_NOT_PRETTY;
        }
        return this;
    }




    public static JsonData fromJsonFile(String path) {
        return fromJsonFile(new File(path));
    }

    public static JsonData fromJsonFile(File file) {
        return fromJsonString(loadFile(file));
    }

    public static JsonData fromInputStream(InputStream inputStream) {
        return fromJsonString(loadFromInputStream(inputStream));
    }

    public static JsonData fromJsonString(String string) {
        JsonObject jsonObject = GSON.fromJson(string, JsonObject.class);
        return new JsonData(jsonObject);
    }

    private static String loadFromInputStream(InputStream inputStream)  {
        try {
            byte[] data = new byte[inputStream.available()];
            inputStream.read(data);
            inputStream.close();
            return new String(data, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }


    private static String loadFile(File file) {
        if (!file.exists())
            return "";
        try {
            FileInputStream fis = new FileInputStream(file);
            byte[] data = new byte[(int) file.length()];
            fis.read(data);
            fis.close();

            return new String(data, "UTF-8");
        } catch (IOException e) {
            // TODO: handle exception
        }
        return "";
    }

    public JsonData append(String property, String value) {
        if (property == null)
            return this;
        jsonObject.addProperty(property, value);
        return this;
    }

    public JsonData append(String property, Object value) {
        if (property == null)
            return this;
        jsonObject.add(property, gson.toJsonTree(value));
        return this;
    }

    public JsonData append(String property, Number value) {
        if (property == null)
            return this;
        jsonObject.addProperty(property, value);
        return this;
    }

    public JsonData append(String property, Boolean value) {
        if (property == null)
            return this;
        jsonObject.addProperty(property, value);
        return this;
    }

    public int getInt(String property) {
        if (!jsonObject.has(property)) return -1;
        return jsonObject.get(property).getAsInt();
    }

    public long getLong(String property) {
        if (!jsonObject.has(property)) return -1;
        return jsonObject.get(property).getAsLong();
    }

    public Boolean getBoolean(String property) {
        if (!jsonObject.has(property)) return false;
        return jsonObject.get(property).getAsBoolean();
    }

    public <T> T getObject(String property, Class<T> clazz) {
        if (!jsonObject.has(property)) return null;
        return gson.fromJson(jsonObject.get(property), clazz);
    }

    public String getString(String property) {
        if (!jsonObject.has(property)) return null;
        return jsonObject.get(property).getAsString();
    }


    public void saveAsFile(String path) {
        saveJsonElementAsFile(new File(path));
    }

    public void saveAsFile(File file) {
        saveJsonElementAsFile(file);
    }

    public boolean saveJsonElementAsFile(String path) {
        return saveJsonElementAsFile(new File(path));
    }

    public boolean saveJsonElementAsFile(File file) {
        File dir = file.getParentFile();
        if (dir != null && !dir.exists()) {
            dir.mkdirs();
        }
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(getJsonStringAsBytes());
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (IOException e) {
            return false;
        }
        return false;
    }

    public String getAsJsonString() {
        return gson.toJson(jsonObject);
    }

    public byte[] getJsonStringAsBytes() {
        return getAsJsonString().getBytes(StandardCharsets.UTF_8);
    }


    public String getAsJsonString(Object object) {
        return gson.toJson(gson.toJsonTree(object));
    }
}
