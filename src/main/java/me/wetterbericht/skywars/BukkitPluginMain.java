package me.wetterbericht.skywars;



import lombok.Getter;
import me.wetterbericht.skywars.command.CommandManager;
import me.wetterbericht.skywars.listener.EventManager;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class BukkitPluginMain extends JavaPlugin {



    @Getter
    private static BukkitPluginMain instance;

    @Override
    public void onEnable() {
        instance = this;
        new Skywars();
        new CommandManager().registerCommands();
        new EventManager().registerEvents();
    }
}
