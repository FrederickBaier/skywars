package me.wetterbericht.skywars.setupgame;

import com.google.common.collect.Lists;
import me.wetterbericht.skywars.game.Game;
import me.wetterbericht.skywars.utils.SerializableLocation;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

@Getter
@Setter
public class SetupGame {

    private SerializableLocation pos1;
    private SerializableLocation pos2;

    private String name;
    private int playersPerTeam;
    private SerializableLocation spectatorSpawn;
    private ArrayList<SetupTeamSpawn> teams = Lists.newArrayList();
    private ArrayList<SetupChestPosition> chests = Lists.newArrayList();

    public SetupGame(String name){
        this.name = name;
    }


    public void addChest(SetupChestPosition setupChestPosition){
        chests.add(setupChestPosition);
    }

    public void clearChests(){
        chests.clear();
    }

    public void addTeam(SetupTeamSpawn setupTeamSpawn){
        teams.add(setupTeamSpawn);
    }

    public void clearTeams(){
        teams.clear();
    }

    public Game toGame(){
        Game game = new Game(name, playersPerTeam, spectatorSpawn.toBukkitLocation());
        for (SetupChestPosition setupChestPosition : chests){
            game.addChest(setupChestPosition.toChest());
        }

        for (SetupTeamSpawn setupTeamSpawn : teams){
            game.addTeam(setupTeamSpawn.toTeam());
        }
        return game;
    }


}
