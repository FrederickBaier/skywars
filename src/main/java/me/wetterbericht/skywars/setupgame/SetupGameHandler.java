package me.wetterbericht.skywars.setupgame;


import me.wetterbericht.json.JsonData;
import me.wetterbericht.skywars.Skywars;

import java.io.File;

public class SetupGameHandler {


    public boolean gameExists(String name){
        return new File(Skywars.GAMES_DIR + name + ".json").exists();
    }

    public SetupGame loadGame(String name){
        return loadGame(new File(Skywars.GAMES_DIR + name + ".json"));
    }

    public SetupGame loadGame(File file){
        return JsonData.fromJsonFile(file).getObject("data", SetupGame.class);
    }

    public void saveGame(SetupGame game){
        new JsonData().append("data", game).saveAsFile(Skywars.GAMES_DIR + game.getName() + ".json");
    }

}
