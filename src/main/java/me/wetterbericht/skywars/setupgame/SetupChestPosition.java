package me.wetterbericht.skywars.setupgame;

import me.wetterbericht.skywars.game.ChestPosition;
import me.wetterbericht.skywars.items.ChestType;
import me.wetterbericht.skywars.utils.SerializableLocation;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class SetupChestPosition {

    private ChestType chestType;
    private SerializableLocation serializableLocation;

    public ChestPosition toChest(){
        return new ChestPosition(chestType, serializableLocation.toBukkitLocation());
    }


}
