package me.wetterbericht.skywars.setupgame;

import me.wetterbericht.skywars.game.TeamSpawn;
import me.wetterbericht.skywars.utils.SerializableLocation;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class SetupTeamSpawn {

    private int number;
    private SerializableLocation spawnLocation;

    public TeamSpawn toTeam(){
        return new TeamSpawn(number, spawnLocation.toBukkitLocation());
    }

}
