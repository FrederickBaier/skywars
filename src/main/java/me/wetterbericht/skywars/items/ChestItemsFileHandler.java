package me.wetterbericht.skywars.items;

import me.wetterbericht.json.JsonData;
import me.wetterbericht.skywars.Skywars;

import java.io.File;

public class ChestItemsFileHandler {

    private File file = new File(Skywars.PLUGIN_MAIN_DIR + "items.json");

    public ChestItemsManager loadFile(){
        if (!file.exists()){
            new JsonData().append("data", new ChestItemsManager()).saveAsFile(file);
            return new ChestItemsManager();
        }
        return JsonData.fromJsonFile(file).getObject("data", ChestItemsManager.class);
    }

    public void saveToFile(ChestItemsManager chestItemsManager){
       new JsonData().append("data", chestItemsManager).saveAsFile(file);
    }

}
