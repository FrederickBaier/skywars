package me.wetterbericht.skywars.items;

import com.google.common.collect.Lists;
import me.wetterbericht.inventoryapi.utils.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class ChestItemsManager {

    private ArrayList<ChestItem> normalItems = Lists.newArrayList();

    private ArrayList<ChestItem> betterItems = Lists.newArrayList();


    public ChestItemsManager() {
        normalItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.WOOD).setAmount(54), ChestItemType.BLOCK));
        normalItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.STONE).setAmount(54), ChestItemType.BLOCK));
        normalItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.BRICK).setAmount(54), ChestItemType.BLOCK));
        //LEATHER
        normalItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.LEATHER_HELMET).setAmount(1), ChestItemType.ARMOR));
        normalItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.LEATHER_CHESTPLATE).setAmount(1), ChestItemType.ARMOR));
        normalItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.LEATHER_LEGGINGS).setAmount(1), ChestItemType.ARMOR));
        normalItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.LEATHER_BOOTS).setAmount(1), ChestItemType.ARMOR));
        //gold
        normalItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.GOLD_HELMET).setAmount(1), ChestItemType.ARMOR));
        normalItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.GOLD_CHESTPLATE).setAmount(1), ChestItemType.ARMOR));
        normalItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.GOLD_LEGGINGS).setAmount(1), ChestItemType.ARMOR));
        normalItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.GOLD_BOOTS).setAmount(1), ChestItemType.ARMOR));
        //iron
        normalItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.IRON_HELMET).setAmount(1), ChestItemType.ARMOR));
        normalItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.IRON_CHESTPLATE).setAmount(1), ChestItemType.ARMOR));
        normalItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.IRON_LEGGINGS).setAmount(1), ChestItemType.ARMOR));
        normalItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.IRON_BOOTS).setAmount(1), ChestItemType.ARMOR));
        //weapons
        normalItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.IRON_SWORD).setAmount(1), ChestItemType.WEAPON));
        normalItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.WOOD_SWORD).setAmount(1).addEnchantment(Enchantment.DAMAGE_ALL, 1), ChestItemType.WEAPON));
        normalItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.STONE_SWORD).setAmount(1).addEnchantment(Enchantment.DAMAGE_ALL, 1), ChestItemType.WEAPON));
        normalItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.GOLD_PICKAXE).setAmount(1), ChestItemType.WEAPON));
        normalItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.IRON_PICKAXE).setAmount(1), ChestItemType.WEAPON));
        normalItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.DIAMOND_PICKAXE).setAmount(1), ChestItemType.WEAPON));
        normalItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.GOLD_AXE).setAmount(1), ChestItemType.WEAPON));
        normalItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.IRON_AXE).setAmount(1), ChestItemType.WEAPON));
        normalItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.DIAMOND_AXE).setAmount(1), ChestItemType.WEAPON));
        //food
        normalItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.COOKED_BEEF).setAmount(7), ChestItemType.FOOD));
        normalItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.COOKED_MUTTON).setAmount(14), ChestItemType.FOOD));
        normalItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.RAW_CHICKEN).setAmount(27), ChestItemType.FOOD));
        normalItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.PUMPKIN_PIE).setAmount(4), ChestItemType.FOOD));
        normalItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.CAKE).setAmount(3), ChestItemType.FOOD));
        //ore
        normalItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.IRON_INGOT).setAmount(7), ChestItemType.ORE));
        normalItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.IRON_INGOT).setAmount(4), ChestItemType.ORE));
        normalItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.DIAMOND).setAmount(5), ChestItemType.ORE));
        normalItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.DIAMOND).setAmount(6), ChestItemType.ORE));
        normalItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.GOLD_INGOT).setAmount(8), ChestItemType.ORE));
        normalItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.GOLD_INGOT).setAmount(3), ChestItemType.ORE));
        //special
        normalItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.WEB).setAmount(7), ChestItemType.SPECIAL));
        normalItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.FLINT_AND_STEEL).setAmount(1), ChestItemType.SPECIAL));
        normalItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.FLINT).setAmount(2), ChestItemType.SPECIAL));
        normalItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.ANVIL).setAmount(1), ChestItemType.SPECIAL));
        normalItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.WORKBENCH).setAmount(5), ChestItemType.SPECIAL));
        normalItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.COAL).setAmount(3), ChestItemType.SPECIAL));
        normalItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.STICK).setAmount(1), ChestItemType.SPECIAL));
        normalItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.WATER_BUCKET).setAmount(1), ChestItemType.SPECIAL));
        normalItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.LAVA_BUCKET).setAmount(1), ChestItemType.SPECIAL));
        //-------------------
        //BETTER ITEMS
        //-------------------
        betterItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.WOOD).setAmount(54), ChestItemType.BLOCK));
        betterItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.STONE).setAmount(54), ChestItemType.BLOCK));
        betterItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.BRICK).setAmount(54), ChestItemType.BLOCK));
        //gold
        betterItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.GOLD_HELMET).setAmount(1), ChestItemType.ARMOR));
        betterItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.GOLD_CHESTPLATE).setAmount(1), ChestItemType.ARMOR));
        betterItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.GOLD_LEGGINGS).setAmount(1), ChestItemType.ARMOR));
        betterItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.GOLD_BOOTS).setAmount(1), ChestItemType.ARMOR));
        //iron
        betterItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.IRON_HELMET).setAmount(1), ChestItemType.ARMOR));
        betterItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.IRON_CHESTPLATE).setAmount(1), ChestItemType.ARMOR));
        betterItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.IRON_LEGGINGS).setAmount(1), ChestItemType.ARMOR));
        betterItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.IRON_BOOTS).setAmount(1), ChestItemType.ARMOR));
        //diamond
        betterItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.DIAMOND_HELMET).setAmount(1), ChestItemType.ARMOR));
        betterItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.DIAMOND_CHESTPLATE).setAmount(1), ChestItemType.ARMOR));
        betterItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.DIAMOND_LEGGINGS).setAmount(1), ChestItemType.ARMOR));
        betterItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.DIAMOND_BOOTS).setAmount(1), ChestItemType.ARMOR));
        //weapons
        betterItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.IRON_SWORD).setAmount(1), ChestItemType.WEAPON));
        betterItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.WOOD_SWORD).setAmount(1).addEnchantment(Enchantment.DAMAGE_ALL, 3), ChestItemType.WEAPON));
        betterItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.STONE_SWORD).setAmount(1).addEnchantment(Enchantment.DAMAGE_ALL, 2), ChestItemType.WEAPON));
        betterItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.DIAMOND_SWORD).setAmount(1), ChestItemType.WEAPON));
        betterItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.IRON_PICKAXE).setAmount(1).addEnchantment(Enchantment.DIG_SPEED, 2), ChestItemType.WEAPON));
        betterItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.DIAMOND_PICKAXE).setAmount(1).addEnchantment(Enchantment.DIG_SPEED, 1), ChestItemType.WEAPON));
        betterItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.IRON_AXE).setAmount(1).addEnchantment(Enchantment.DIG_SPEED, 2), ChestItemType.WEAPON));
        betterItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.DIAMOND_AXE).setAmount(1).addEnchantment(Enchantment.DIG_SPEED, 1), ChestItemType.WEAPON));
        //food
        betterItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.COOKED_BEEF).setAmount(31), ChestItemType.FOOD));
        betterItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.COOKED_MUTTON).setAmount(21), ChestItemType.FOOD));
        betterItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.PUMPKIN_PIE).setAmount(16), ChestItemType.FOOD));
        betterItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.CAKE).setAmount(9), ChestItemType.FOOD));
        //ore
        betterItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.IRON_INGOT).setAmount(11), ChestItemType.ORE));
        betterItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.IRON_INGOT).setAmount(6), ChestItemType.ORE));
        betterItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.DIAMOND).setAmount(7), ChestItemType.ORE));
        betterItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.DIAMOND).setAmount(6), ChestItemType.ORE));
        betterItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.GOLD_INGOT).setAmount(12), ChestItemType.ORE));
        betterItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.GOLD_INGOT).setAmount(5), ChestItemType.ORE));
        //special
        betterItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.WEB).setAmount(7), ChestItemType.SPECIAL));
        betterItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.FLINT_AND_STEEL).setAmount(1), ChestItemType.SPECIAL));
        betterItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.FLINT).setAmount(2), ChestItemType.SPECIAL));
        betterItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.ANVIL).setAmount(4), ChestItemType.SPECIAL));
        betterItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.WORKBENCH).setAmount(5), ChestItemType.SPECIAL));
        betterItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.COAL).setAmount(5), ChestItemType.SPECIAL));
        betterItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.STICK).setAmount(8), ChestItemType.SPECIAL));
        betterItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.WATER_BUCKET).setAmount(1), ChestItemType.SPECIAL));
        betterItems.add(new ChestItem(new ItemBuilder().setMaterial(Material.LAVA_BUCKET).setAmount(1), ChestItemType.SPECIAL));
    }

    public ItemStack getRandomNormalItem(ChestItemType chestItemType) {
        Random rnd = new Random();
        List<ChestItem> chestItems = normalItems.stream().filter(chestItem -> chestItem.getChestItemType() == chestItemType).collect(Collectors.toList());
        return chestItems.get(rnd.nextInt(chestItems.size())).getItemBuilder().buildItemStack();
    }

    public ItemStack getRandomBetterItem(ChestItemType chestItemType) {
        Random rnd = new Random();
        List<ChestItem> chestItems = betterItems.stream().filter(chestItem -> chestItem.getChestItemType() == chestItemType).collect(Collectors.toList());
        return chestItems.get(rnd.nextInt(chestItems.size())).getItemBuilder().buildItemStack();
    }

    public ItemStack getItem(ChestType chestType, ChestItemType chestItemType) {
        if (chestType == ChestType.NORMAL) {
            return getRandomNormalItem(chestItemType);
        } else {
            return getRandomBetterItem(chestItemType);
        }
    }

    public List<ChestItem> getAll(ChestType chestType, ChestItemType chestItemType){
        if (chestType == ChestType.NORMAL) {
            return normalItems.stream().filter(chestItem -> chestItem.getChestItemType() == chestItemType).collect(Collectors.toList());
        } else {
            return betterItems.stream().filter(chestItem -> chestItem.getChestItemType() == chestItemType).collect(Collectors.toList());
        }
    }

    public void fillInventory(Inventory inventory, ChestType chestType) {
        Random rnd = new Random();
        List<Integer> usedSlots = Lists.newArrayList();
        for (int i = 0; i < 12; i++) {
            int index = rnd.nextInt(27);
            while (usedSlots.contains(index)) {
                index = rnd.nextInt(27);
            }
            usedSlots.add(index);
            if (i <= 3) {
                inventory.setItem(index, getItem(chestType, ChestItemType.ARMOR));
            }
            if (i > 3 && i <= 5) {
                inventory.setItem(index, getItem(chestType, ChestItemType.BLOCK));
            }
            if (i > 5 && i <= 6) {
                inventory.setItem(index, getItem(chestType, ChestItemType.FOOD));
            }
            if (i > 6 && i <= 8) {
                inventory.setItem(index, getItem(chestType, ChestItemType.ORE));
            }
            if (i > 8 && i <= 10) {
                inventory.setItem(index, getItem(chestType, ChestItemType.WEAPON));
            }
            if (i > 10) {
                inventory.setItem(index, getItem(chestType, ChestItemType.SPECIAL));
            }
        }
    }

    public void removeChestItem(ChestItem chestItem, ChestType chestType) {
        if (chestType == ChestType.NORMAL){
            normalItems.remove(chestItem);
        } else {
            betterItems.remove(chestItem);
        }
    }

    public void addChestItem(ChestItem chestItem, ChestType chestType) {
        if (chestType == ChestType.NORMAL){
            normalItems.add(chestItem);
        } else {
            betterItems.add(chestItem);
        }
    }

}
