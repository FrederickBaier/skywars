package me.wetterbericht.skywars.items;

import me.wetterbericht.inventoryapi.utils.ItemCreator;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class Items {

    public static final ItemStack VOTE_ITEM = ItemCreator.create(Material.ITEM_FRAME, "§eVote");
    public static final ItemStack TEAMS_ITEM = ItemCreator.create(Material.BED, "§eTeams");
    public static final ItemStack KITS_ITEM = ItemCreator.create(Material.CHEST, "§eKits");
    public static final ItemStack SPEC_COMPASS_ITEM = ItemCreator.create(Material.COMPASS, "§eTeleporter");

}
