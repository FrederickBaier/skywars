package me.wetterbericht.skywars.items;

import lombok.AllArgsConstructor;
import lombok.Getter;
import me.wetterbericht.inventoryapi.utils.ItemBuilder;

@Getter
@AllArgsConstructor
public class ChestItem {

    private ItemBuilder itemBuilder;
    private ChestItemType chestItemType;

}
