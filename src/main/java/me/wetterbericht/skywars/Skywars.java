package me.wetterbericht.skywars;

import com.google.common.collect.Lists;
import lombok.Setter;
import me.wetterbericht.skywars.config.ConfigData;
import me.wetterbericht.skywars.config.ConfigLoader;
import lombok.Getter;
import me.wetterbericht.skywars.config.SetupConfigData;
import me.wetterbericht.skywars.executor.SkywarsExecutor;
import me.wetterbericht.skywars.external.SkywarsModuleManager;
import me.wetterbericht.skywars.game.Game;
import me.wetterbericht.skywars.items.ChestItemsManager;
import me.wetterbericht.skywars.items.ChestItemsFileHandler;
import me.wetterbericht.skywars.kits.KitManager;

import me.wetterbericht.skywars.particle.ParticleManager;
import me.wetterbericht.skywars.scoreboard.ScoreboardTabList;
import me.wetterbericht.skywars.setupgame.SetupGame;
import me.wetterbericht.skywars.setupgame.SetupGameHandler;
import me.wetterbericht.skywars.utils.*;
import me.wetterbericht.skywars.vote.TeamsManager;
import me.wetterbericht.skywars.vote.VoteManager;
import me.wetterbericht.worldloader.WorldLoader;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;

import java.io.File;
import java.util.ArrayList;
import java.util.Random;

public class Skywars {

    public static final String PLUGIN_MAIN_DIR = "plugins/Skywars/";
    public static final String GAMES_DIR = PLUGIN_MAIN_DIR + "games/";
    public static final String MODULES_DIR = PLUGIN_MAIN_DIR + "modules/";

    public static String PREFIX;
    public static boolean SETUP = false;

    @Getter
    private static Skywars instance;



    //@Getter
    //private Messages messages;

    @Getter
    private SetupGameHandler setupGameLoader;

    @Getter
    private ConfigData configData;

    @Getter
    private VoteManager voteManager;

    @Getter
    private TeamsManager teamsManager;

    @Getter
    private ChestItemsManager chestItems;

    @Getter
    private KitManager kitManager;

    @Getter
    private KillsManager killsManager = new KillsManager();

    @Getter
    @Setter
    private Game game = null;

    @Getter
    private SkywarsExecutor skywarsExecutor;

    @Getter
    private ParticleManager particleManager = new ParticleManager();

    @Getter
    private SkywarsModuleManager skywarsModuleManager;


    @Getter
    @Setter
    private GameState gameState = GameState.STARTING;

    @Getter
    private ArrayList<SetupGame> availableGames = Lists.newArrayList();

    public Skywars() {
        new File(GAMES_DIR).mkdirs();
        instance = this;
        //load games
        SetupConfigData setupConfigData = new ConfigLoader().loadConfig();
        if (setupConfigData == null) {
            this.configData = new ConfigData(null, 0, 0);
        } else {
            this.configData = setupConfigData.toConfigData();
        }
        Bukkit.setDefaultGameMode(GameMode.ADVENTURE);
        this.skywarsModuleManager = new SkywarsModuleManager();
        Skywars.PREFIX = this.skywarsModuleManager.getSkywarsNameColorAdapter().getPrefix();
        this.skywarsModuleManager.getSkywarsCloudAdapter().onSkywarsLoad(this.configData.getMaxPlayers());
        this.setupGameLoader = new SetupGameHandler();
        this.chestItems = new ChestItemsFileHandler().loadFile();
        this.skywarsExecutor = new SkywarsExecutor();
        this.kitManager = new KitManager();
        this.particleManager.startParticleScheduler();

        loadGames();
        this.voteManager = new VoteManager();
        this.teamsManager = new TeamsManager();
        WorldLoader.loadWorlds();
        ScoreboardTabList.registerTeams();
        new FoodScheduler().run();




        /*
        try { messages = new LanguageManager().load();
            System.out.println(messages.toString());
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        */
        this.gameState = GameState.LOBBY;
    }


    private void loadGames(){
        for (File file : new File(GAMES_DIR).listFiles()){
            if (file.getName().endsWith(".json")){
               SetupGame setupGame = setupGameLoader.loadGame(file);
               if (setupGame.getPlayersPerTeam() == this.configData.getPlayersPerTeam() && setupGame.getTeams().size() == this.configData.getTeams()){
                   this.availableGames.add(setupGame);
               }
            }
        }
        if (this.availableGames.size() > 9){
            ArrayList<SetupGame> list = Lists.newArrayList();
            Random rnd = new Random();
            for (int i = 0; i < 9 ; i++){
                list.add(this.availableGames.remove(rnd.nextInt(this.availableGames.size() - 1)));
            }
            this.availableGames = list;
        }
    }

}
