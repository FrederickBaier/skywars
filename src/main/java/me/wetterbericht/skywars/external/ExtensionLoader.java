package me.wetterbericht.skywars.external;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;

public class ExtensionLoader<C> {

    public C LoadClass(File jar, String classpath, Class<C> parentClass) throws IOException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException, InstantiationException {
        try {
            File[] plugins = new File("plugins/").listFiles();
            List<URL> urls = new ArrayList<>();
            for (File plugin : plugins) {
                if (plugin.getName().endsWith(".jar")) {
                    urls.add(plugin.toURI().toURL());
                }
            }
            urls.add(jar.toURI().toURL());
            //urls.add(getClass().getProtectionDomain().getCodeSource().getLocation()
            //        .toURI().toURL());

            URLClassLoader urlClassLoader = new URLClassLoader(urls.toArray(new URL[urls.size()]), getClass().getClassLoader());
            Class<?> clazz = Class.forName(classpath, true, urlClassLoader);
            Class<? extends C> newClass = clazz.asSubclass(parentClass);
            // Apparently its bad to use Class.newInstance, so we use
            // newClass.getConstructor() instead
            Constructor<? extends C> constructor = newClass.getConstructor();
            return constructor.newInstance();

        } catch (ClassNotFoundException e) {
           throw e;
        } catch (MalformedURLException e) {
            throw e;
        } catch (NoSuchMethodException e) {
            throw e;
        } catch (InvocationTargetException e) {
            throw e;
        } catch (IllegalAccessException e) {
            throw e;
        } catch (InstantiationException e) {
            throw e;
        }
    }
}