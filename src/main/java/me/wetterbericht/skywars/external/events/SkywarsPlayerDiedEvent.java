package me.wetterbericht.skywars.external.events;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

@Getter
@AllArgsConstructor
public class SkywarsPlayerDiedEvent extends Event {

    private static final HandlerList handlers = new HandlerList();

    /**
     * The player who died
     */
    private Player player;
    /**
     * The player who killed the died player or null if there is no killer.
     */
    private Player killer;

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }
}
