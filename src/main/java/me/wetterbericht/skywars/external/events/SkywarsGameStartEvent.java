package me.wetterbericht.skywars.external.events;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import me.wetterbericht.skywars.game.Game;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

@Getter
public class SkywarsGameStartEvent extends Event implements Cancellable {

    private static final HandlerList handlers = new HandlerList();

    private Game game;

    private boolean cancelled;

    @Setter
    private String cancelMessage = "Game start was cancelled by an module.";

    public SkywarsGameStartEvent(Game game){
        this.game = game;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }


    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }
}
