package me.wetterbericht.skywars.external.events;

import lombok.AllArgsConstructor;
import lombok.Getter;
import me.wetterbericht.skywars.game.Game;
import me.wetterbericht.skywars.game.Team;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

@AllArgsConstructor
@Getter
public class SkywarsGameEndedEvent extends Event {

    private static final HandlerList handlers = new HandlerList();

    private Game game;

    private Team winnerTeam;

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }
}
