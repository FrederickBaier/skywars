package me.wetterbericht.skywars.external;

import com.google.common.collect.Lists;
import me.wetterbericht.json.JsonData;
import me.wetterbericht.skywars.Skywars;
import org.bukkit.Bukkit;

import java.io.File;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;

public class SkywarsModuleLoader {

    public List<SkywarsModule> loadSkywarsModules() {
        File modulesDir = new File(Skywars.MODULES_DIR);
        modulesDir.mkdirs();
        List<File> jarFiles = Lists.newArrayList();
        for (File jar : modulesDir.listFiles()) {
            if (jar.getName().endsWith(".jar")) {
                jarFiles.add(jar);
            }
        }
        List<SkywarsModule> list = new ArrayList<>();
        for (File jarFile : jarFiles) {
            try {
                JsonData jsonData = JsonData.fromInputStream(getInputStreamFromRecource(jarFile, "module.json"));
                SkywarsModuleConfig skywarsModuleConfig = jsonData.getObject("module", SkywarsModuleConfig.class);
                SkywarsModule skywarsModule = new ExtensionLoader<SkywarsModule>().LoadClass(jarFile, skywarsModuleConfig.getMain(), SkywarsModule.class);
                list.add(skywarsModule);
                Bukkit.getConsoleSender().sendMessage("[Skywars] Module " + skywarsModuleConfig.getName() + ":" + skywarsModuleConfig.getVersion() + " by " + skywarsModuleConfig.getAuthor() + " was loaded successfully.");
            } catch (Exception e) {
                Bukkit.getConsoleSender().sendMessage("[Skywars] An error occurred while loading module " + jarFile.getName() + ":");
                e.printStackTrace();
            }
        }
        return list;
    }

    private static InputStream getInputStreamFromRecource(File file, String pathToRecource) throws MalformedURLException {
        return new URLClassLoader(new URL[]{file.toURI().toURL()}).getResourceAsStream(pathToRecource);
    }


}
