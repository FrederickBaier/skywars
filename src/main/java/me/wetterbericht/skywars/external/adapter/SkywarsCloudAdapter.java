package me.wetterbericht.skywars.external.adapter;

import me.wetterbericht.skywars.external.SkywarsAdapter;
import me.wetterbericht.skywars.game.Game;

public interface SkywarsCloudAdapter extends SkywarsAdapter {


    void onSkywarsLoad(int maxPlayers);

    void onGameSelected(Game game);

     void changeToIngame();

     //void changeToEnd();

     void onNoGameSelected();



}
