package me.wetterbericht.skywars.external.adapter;

import org.bukkit.entity.Player;

public class DefaultSkywarsCoinsAdapterImpl implements SkywarsCoinsAdapter{
    @Override
    public void addCoins(Player p, int amount) {

    }

    @Override
    public void removeCoins(Player p, int amount) {

    }

    @Override
    public int getCoins(Player p) {
        return 0;
    }

    @Override
    public String getCoinsName() {
        return "Coins";
    }
}
