package me.wetterbericht.skywars.external.adapter;

import org.bukkit.entity.Player;

public class DefaultSkywarsNameColorAdapterImpl implements SkywarsNameColorAdapter{
    @Override
    public String getGroupColor(Player p, Player receiver) {
        return "§e";
    }

    @Override
    public String getName(Player p, Player receiver) {
        return p.getName();
    }

    @Override
    public String getGroupPrefixWithUsername(Player p, Player receiver) {
        return "§e" + p.getName();
    }

    @Override
    public String getPrefix() {
        return "§aSkywars §8» §7";
    }
}
