package me.wetterbericht.skywars.external.adapter;

import me.wetterbericht.skywars.external.SkywarsAdapter;
import org.bukkit.entity.Player;

public interface SkywarsNameColorAdapter extends SkywarsAdapter {

    String getGroupColor(Player p, Player receiver);

    String getName(Player p, Player receiver);

    String getGroupPrefixWithUsername(Player p, Player receiver);

    String getPrefix();

}
