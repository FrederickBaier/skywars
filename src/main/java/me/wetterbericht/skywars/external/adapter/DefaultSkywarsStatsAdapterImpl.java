package me.wetterbericht.skywars.external.adapter;

import com.google.common.collect.Lists;
import me.wetterbericht.skywars.database.Stats;

import java.util.UUID;

public class DefaultSkywarsStatsAdapterImpl implements SkywarsStatsAdapter{
    @Override
    public Stats getStats(UUID playerUUID) {
        return new Stats(playerUUID, 0, 0, 0, Lists.newArrayList(), 0);
    }

    @Override
    public void saveStats(Stats stats) {

    }

    @Override
    public boolean hasStats(UUID playerUUID) {
        return false;
    }
}
