package me.wetterbericht.skywars.external.adapter;

import me.wetterbericht.skywars.database.Stats;
import me.wetterbericht.skywars.external.SkywarsAdapter;

import java.util.UUID;

public interface SkywarsStatsAdapter extends SkywarsAdapter {

    Stats getStats(UUID playerUUID);

    void saveStats(Stats stats);

    boolean hasStats(UUID playerUUID);




}
