package me.wetterbericht.skywars.external.adapter;

import me.wetterbericht.skywars.external.SkywarsAdapter;
import org.bukkit.entity.Player;

public interface SkywarsCoinsAdapter extends SkywarsAdapter {

    void addCoins(Player p, int amount);

    void removeCoins(Player p, int amount);

    int getCoins(Player p);

    String getCoinsName();


}
