package me.wetterbericht.skywars.external.adapter;

import me.wetterbericht.skywars.game.Game;

public class DefaultSkywarsCloudAdapterImpl implements SkywarsCloudAdapter{
    @Override
    public void onSkywarsLoad(int maxPlayers) {

    }

    @Override
    public void onGameSelected(Game game) {

    }

    @Override
    public void changeToIngame() {

    }

    @Override
    public void onNoGameSelected() {

    }
}
