package me.wetterbericht.skywars.external;

import java.util.List;

public abstract class SkywarsModule {

    public abstract List<SkywarsAdapter> getSkywarsAdapters();

}
