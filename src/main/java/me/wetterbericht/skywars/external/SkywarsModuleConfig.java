package me.wetterbericht.skywars.external;

import lombok.Getter;

@Getter
public class SkywarsModuleConfig {

    private String name;

    private String version;

    private String author;

    private String main;


}
