package me.wetterbericht.skywars.external;

import lombok.Getter;
import me.wetterbericht.skywars.external.adapter.*;

import java.util.List;

@Getter
public class SkywarsModuleManager {

    private SkywarsCloudAdapter skywarsCloudAdapter;

    private SkywarsCoinsAdapter skywarsCoinsAdapter;

    private SkywarsStatsAdapter skywarsStatsAdapter;

    private SkywarsNameColorAdapter skywarsNameColorAdapter;

    public SkywarsModuleManager() {
        List<SkywarsModule> list = new SkywarsModuleLoader().loadSkywarsModules();
        System.out.println("[Skywars] Modules found: " + list.size());
        for (SkywarsModule skywarsModule : list) {
            for (SkywarsAdapter skywarsAdapter : skywarsModule.getSkywarsAdapters()) {
                if (SkywarsCloudAdapter.class.isAssignableFrom(skywarsAdapter.getClass())) {
                    if (skywarsCloudAdapter == null) {
                        skywarsCloudAdapter = (SkywarsCloudAdapter) skywarsAdapter;
                    }
                }
                if (SkywarsCoinsAdapter.class.isAssignableFrom(skywarsAdapter.getClass())) {
                    if (skywarsCoinsAdapter == null) {
                        skywarsCoinsAdapter = (SkywarsCoinsAdapter) skywarsAdapter;
                    }
                }
                if (SkywarsStatsAdapter.class.isAssignableFrom(skywarsAdapter.getClass())) {
                    if (skywarsStatsAdapter == null) {
                        skywarsStatsAdapter = (SkywarsStatsAdapter) skywarsAdapter;
                    }
                }
                if (SkywarsNameColorAdapter.class.isAssignableFrom(skywarsAdapter.getClass())) {
                    if (skywarsNameColorAdapter == null) {
                        skywarsNameColorAdapter = (SkywarsNameColorAdapter) skywarsAdapter;
                    }
                }
            }
         }
        if (skywarsCloudAdapter == null) {
            skywarsCloudAdapter = new DefaultSkywarsCloudAdapterImpl();
            System.out.println("[Skywars] Using DefaultSkywarsCloudAdapterImpl");
        }
        if (skywarsCoinsAdapter == null) {
            skywarsCoinsAdapter = new DefaultSkywarsCoinsAdapterImpl();
            System.out.println("[Skywars] Using DefaultSkywarsCoinsAdapterImpl");
        }
        if (skywarsStatsAdapter == null) {
            skywarsStatsAdapter = new DefaultSkywarsStatsAdapterImpl();
            System.out.println("[Skywars] Using DefaultSkywarsStatsAdapterImpl");
        }
        if (skywarsNameColorAdapter == null) {
            skywarsNameColorAdapter = new DefaultSkywarsNameColorAdapterImpl();
            System.out.println("[Skywars] Using DefaultSkywarsNameColorAdapterImpl");
        }

    }
}
