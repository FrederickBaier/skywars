package me.wetterbericht.skywars.listener;

import me.wetterbericht.skywars.BukkitPluginMain;
import me.wetterbericht.skywars.listener.listeners.*;

public class EventManager {

    private BukkitPluginMain bukkitPluginMain = BukkitPluginMain.getInstance();

    public void registerEvents() {
        bukkitPluginMain.getServer().getPluginManager().registerEvents(new PlayerJoinListener(), bukkitPluginMain);
        bukkitPluginMain.getServer().getPluginManager().registerEvents(new PlayerQuitListener(), bukkitPluginMain);
        bukkitPluginMain.getServer().getPluginManager().registerEvents(new PlayerLoginListener(), bukkitPluginMain);
        bukkitPluginMain.getServer().getPluginManager().registerEvents(new PlayerInteractListener(), bukkitPluginMain);
        bukkitPluginMain.getServer().getPluginManager().registerEvents(new PlayerMoveListener(), bukkitPluginMain);
        bukkitPluginMain.getServer().getPluginManager().registerEvents(new EntityDamageListener(), bukkitPluginMain);
        bukkitPluginMain.getServer().getPluginManager().registerEvents(new EntityDamageByEntityListener(), bukkitPluginMain);
        bukkitPluginMain.getServer().getPluginManager().registerEvents(new PlayerDeathListener(), bukkitPluginMain);
        bukkitPluginMain.getServer().getPluginManager().registerEvents(new PlayerAchievementAwardedListener(), bukkitPluginMain);
        bukkitPluginMain.getServer().getPluginManager().registerEvents(new PlayerDropItemListener(), bukkitPluginMain);
        bukkitPluginMain.getServer().getPluginManager().registerEvents(new InventoryClickListener(), bukkitPluginMain);
        bukkitPluginMain.getServer().getPluginManager().registerEvents(new WeatherChangeListener(), bukkitPluginMain);
        bukkitPluginMain.getServer().getPluginManager().registerEvents(new PlayerPickUpItemListener(), bukkitPluginMain);
        bukkitPluginMain.getServer().getPluginManager().registerEvents(new PlayerChatListener(), bukkitPluginMain);
        bukkitPluginMain.getServer().getPluginManager().registerEvents(new EntitySpawnListener(), bukkitPluginMain);
        bukkitPluginMain.getServer().getPluginManager().registerEvents(new WaterListener(), bukkitPluginMain);
        //bukkitPluginMain.getServer().getPluginManager().registerEvents(new BlocklBreakListener(), bukkitPluginMain);

    }

}
