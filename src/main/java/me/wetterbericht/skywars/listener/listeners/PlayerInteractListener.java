package me.wetterbericht.skywars.listener.listeners;

import me.wetterbericht.skywars.Skywars;
import me.wetterbericht.skywars.items.Items;
import me.wetterbericht.skywars.spectator.SpectatorInventory;
import me.wetterbericht.skywars.utils.GameState;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class PlayerInteractListener implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractEvent e){
        Player p = e.getPlayer();
        ItemStack item = e.getItem();
        if (item != null && item.hasItemMeta() && item.getItemMeta().hasDisplayName()){
            if (item.getItemMeta().getDisplayName().equals(Items.VOTE_ITEM.getItemMeta().getDisplayName())){
                e.setCancelled(true);
                e.getPlayer().openInventory(Skywars.getInstance().getVoteManager().getVoteInventory());
            }
            if (item.getItemMeta().getDisplayName().equals(Items.TEAMS_ITEM.getItemMeta().getDisplayName())){
                e.setCancelled(true);
                e.getPlayer().openInventory(Skywars.getInstance().getTeamsManager().getTeamsInventory(e.getPlayer()));
            }
            if (item.getItemMeta().getDisplayName().equals(Items.KITS_ITEM.getItemMeta().getDisplayName())){
                e.setCancelled(true);
                e.getPlayer().openInventory(Skywars.getInstance().getKitManager().getInventory(e.getPlayer()));
            }
            if (item.getItemMeta().getDisplayName().equals(Items.SPEC_COMPASS_ITEM.getItemMeta().getDisplayName())){
                e.setCancelled(true);
                SpectatorInventory.openSpecInventory(e.getPlayer());
            }
        }
        if (Skywars.getInstance().getGameState() != GameState.INGAME && !(e.getPlayer().getGameMode() == GameMode.CREATIVE)){
            e.setCancelled(true);
            return;
        }
        if (Skywars.getInstance().getGameState() == GameState.INGAME && Skywars.getInstance().getTeamsManager().getTeam(p) == null){
            e.setCancelled(true);
            return;
        }
        if (Skywars.getInstance().getSkywarsExecutor().isFreezePlayers()){
            e.setCancelled(true);
        }
    }

}
