package me.wetterbericht.skywars.listener.listeners;




import me.wetterbericht.skywars.BukkitPluginMain;
import me.wetterbericht.skywars.Skywars;
import me.wetterbericht.skywars.database.Stats;
import me.wetterbericht.skywars.external.adapter.SkywarsNameColorAdapter;
import me.wetterbericht.skywars.external.events.SkywarsPlayerDiedEvent;
import me.wetterbericht.skywars.game.Team;
import me.wetterbericht.skywars.items.Items;
import me.wetterbericht.skywars.scoreboard.ScoreboardTabList;
import me.wetterbericht.skywars.utils.GameState;
import me.wetterbericht.skywars.utils.LastDamager;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

public class PlayerDeathListener implements Listener {

    private SkywarsNameColorAdapter skywarsNameColorAdapter = Skywars.getInstance().getSkywarsModuleManager().getSkywarsNameColorAdapter();

    @EventHandler
    public void on(PlayerDeathEvent e) {
        if (Skywars.getInstance().getGameState() != GameState.INGAME)
            return;
        e.setKeepInventory(true);
        Player p = e.getEntity();
        e.setDeathMessage(null);
        Player lastDamager = LastDamager.getLastDamager(p);
        if (lastDamager != null) {
            lastDamager.sendMessage(Skywars.PREFIX + "Du hast §e" + skywarsNameColorAdapter.getName(p, lastDamager) + " §7getötet.");
            p.sendMessage(Skywars.PREFIX + "Du wurdest von §e" + skywarsNameColorAdapter.getName(lastDamager, p) + " §7getötet.");
            Bukkit.getOnlinePlayers().forEach(player -> player.sendMessage(Skywars.PREFIX + "§e" + skywarsNameColorAdapter.getName(p, player) + " §7wurde von §e" + skywarsNameColorAdapter.getName(lastDamager, player)+ " §7getötet."));
            lastDamager.playSound(lastDamager.getLocation(), Sound.LEVEL_UP, 1, 1);
            lastDamager.sendMessage(Skywars.PREFIX + "+§e10 " + Skywars.getInstance().getSkywarsModuleManager().getSkywarsCoinsAdapter().getCoinsName());
            Skywars.getInstance().getKillsManager().addKill(lastDamager);
            Bukkit.getScheduler().runTaskAsynchronously(BukkitPluginMain.getInstance(), () -> {
                Stats stats = Skywars.getInstance().getSkywarsModuleManager().getSkywarsStatsAdapter().getStats(lastDamager.getUniqueId());
                stats.setKills(stats.getKills() + 1);
                Skywars.getInstance().getSkywarsModuleManager().getSkywarsStatsAdapter().saveStats(stats);
                Skywars.getInstance().getSkywarsModuleManager().getSkywarsCoinsAdapter().addCoins(lastDamager, 10);
            });
        } else {
            p.sendMessage(Skywars.PREFIX + "Du bist gestorben.");
            Bukkit.getOnlinePlayers().forEach(player -> player.sendMessage(Skywars.PREFIX + "§e" + skywarsNameColorAdapter.getName(p, player) + " §7ist gestorben."));
        }
        Bukkit.getScheduler().runTaskAsynchronously(BukkitPluginMain.getInstance(), () -> {
            Stats stats = Skywars.getInstance().getSkywarsModuleManager().getSkywarsStatsAdapter().getStats(lastDamager.getUniqueId());
            stats.setDeaths(stats.getDeaths() + 1);
            Skywars.getInstance().getSkywarsModuleManager().getSkywarsStatsAdapter().saveStats(stats);
            Skywars.getInstance().getSkywarsModuleManager().getSkywarsCoinsAdapter().addCoins(p, 5);
        });

        Team playerTeam = Skywars.getInstance().getTeamsManager().getTeam(p);
        playerTeam.removePlayer(p);
        p.setHealth(20);
        p.setGameMode(GameMode.ADVENTURE);
        p.setVelocity(new Vector(0, 0, 0));
        Bukkit.getOnlinePlayers().forEach(all -> {
            if (Skywars.getInstance().getTeamsManager().getTeam(all) != null) {
                all.hidePlayer(p);
            } else {
                p.showPlayer(all);
            }
        });
        p.sendMessage(Skywars.PREFIX + "+§e5 " + Skywars.getInstance().getSkywarsModuleManager().getSkywarsCoinsAdapter().getCoinsName());
        if (p.getLocation().getY() < 0) {
            p.teleport(Skywars.getInstance().getGame().getSpectatorSpawn());
        } else {
            for (ItemStack item : p.getInventory().getContents()) {
                if (item != null && item.getType() != Material.AIR)
                    p.getWorld().dropItemNaturally(p.getLocation(), item);
            }
            for (ItemStack item : p.getInventory().getArmorContents()) {
                if (item != null && item.getType() != Material.AIR)
                    p.getWorld().dropItemNaturally(p.getLocation(), item);
            }
        }
        p.setGameMode(GameMode.ADVENTURE);
        p.getInventory().clear();
        p.getInventory().setArmorContents(null);
        p.setAllowFlight(true);
        p.setFlying(true);
        p.playSound(p.getLocation(), Sound.ANVIL_LAND, 1, 1);
        Bukkit.getScheduler().runTaskLater(BukkitPluginMain.getInstance(), () -> {
            if (p.isOnline() && Skywars.getInstance().getGameState() == GameState.INGAME) {
                p.setAllowFlight(true);
                p.setFlying(true);
                p.getInventory().addItem(Items.SPEC_COMPASS_ITEM);
            }
        }, 5);
        ScoreboardTabList.setTeam(p, "Spectator");
        Bukkit.getPluginManager().callEvent(new SkywarsPlayerDiedEvent(p, lastDamager));
        Bukkit.getScheduler().runTaskLater(BukkitPluginMain.getInstance(), () -> Skywars.getInstance().getSkywarsExecutor().checkWinner(), 6);

        /*
        Bukkit.getScheduler().runTaskLater(BukkitPluginMain.getInstance(), () -> {
            if (Skywars.NICK_API) {
                NickPlayer nickPlayer = NickAPI.getInstance().getNickPlayer(p);
                if (!(nickPlayer instanceof EmptyNickPlayer))
                    NickAPI.getInstance().unnickPlayer(nickPlayer);
            }
        }, 10);
        */
    }

}
