package me.wetterbericht.skywars.listener.listeners;

import org.bukkit.block.Chest;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

public class BlockBreakListener implements Listener {

    @EventHandler
    public void on(BlockBreakEvent e) {
        if (e.getBlock().getState() instanceof Chest) {
            Chest chest = (Chest) e.getBlock().getState();
            if (!e.isCancelled()){
                for (ItemStack itemStack : chest.getBlockInventory().getContents()){
                    e.getBlock().getWorld().dropItemNaturally(e.getBlock().getLocation(), itemStack);
                }
            }
        }
    }

}
