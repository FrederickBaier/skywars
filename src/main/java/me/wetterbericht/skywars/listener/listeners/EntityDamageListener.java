package me.wetterbericht.skywars.listener.listeners;

import me.wetterbericht.skywars.Skywars;
import me.wetterbericht.skywars.utils.GameState;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class EntityDamageListener implements Listener {

    @EventHandler
    public void onLobbyFreeze(EntityDamageEvent e){
        if (Skywars.getInstance().getGameState() != GameState.INGAME || Skywars.getInstance().getSkywarsExecutor().isFreezePlayers()){
            e.setCancelled(true);
            return;
        }
        if (!(e.getEntity() instanceof Player))
            return;
        Player p = (Player) e.getEntity();
        if (Skywars.getInstance().getTeamsManager().getTeam(p) == null){
            e.setCancelled(true);
        }
    }



}
