package me.wetterbericht.skywars.listener.listeners;


import me.wetterbericht.skywars.BukkitPluginMain;
import me.wetterbericht.skywars.Skywars;
import me.wetterbericht.skywars.database.Stats;
import me.wetterbericht.skywars.external.adapter.SkywarsNameColorAdapter;
import me.wetterbericht.skywars.game.Team;
import me.wetterbericht.skywars.scoreboard.ScoreboardManager;
import me.wetterbericht.skywars.scoreboard.ScoreboardTabList;
import me.wetterbericht.skywars.utils.GameState;
import me.wetterbericht.skywars.utils.LastDamager;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.util.Vector;

public class PlayerQuitListener implements Listener {

    private SkywarsNameColorAdapter skywarsNameColorAdapter = Skywars.getInstance().getSkywarsModuleManager().getSkywarsNameColorAdapter();

    @EventHandler
    public void onIngameQuit(PlayerQuitEvent e) {
        Player p = e.getPlayer();
        e.setQuitMessage(null);
        onquitkick(p);
    }

    @EventHandler
    public void onIngameQuit(PlayerKickEvent e) {
        Player p = e.getPlayer();
        e.setLeaveMessage(null);
        onquitkick(p);
    }


    private void onquitkick(Player p) {
        Team playerTeam = Skywars.getInstance().getTeamsManager().getTeam(p);
        if (Skywars.getInstance().getGameState() != GameState.INGAME) {
            if (Skywars.getInstance().getGameState() == GameState.LOBBY) {
                Bukkit.getScheduler().runTaskLater(BukkitPluginMain.getInstance(), () -> {
                    if (Skywars.getInstance().getGameState() == GameState.LOBBY)
                        Bukkit.getOnlinePlayers().forEach(ScoreboardManager::updateScoreboard);
                }, 5);

                if (playerTeam != null) {
                    playerTeam.removePlayer(p);
                    ScoreboardTabList.removeFromTeam(p);
                }
            }
            if (Skywars.getInstance().getGameState() != GameState.STOPPING) {
                Bukkit.getOnlinePlayers().forEach(player -> player.sendMessage("§8« " + skywarsNameColorAdapter.getGroupColor(p, player) + skywarsNameColorAdapter.getName(p, player) + " §7hat das Spiel verlassen."));
            }
            return;
        }

        if (Skywars.getInstance().getTeamsManager().getTeam(p) == null){
            return;
        }
        ScoreboardTabList.removeFromTeam(p);
        Player lastDamager = LastDamager.getLastDamager(p);
        if (lastDamager != null) {
            lastDamager.sendMessage(Skywars.PREFIX + "Du hast §e" + skywarsNameColorAdapter.getName(p, lastDamager) + " §7getötet.");
            p.sendMessage(Skywars.PREFIX + "Du wurdest von §e" + skywarsNameColorAdapter.getName(lastDamager, p) + " §7getötet.");
            Bukkit.getOnlinePlayers().forEach(player -> player.sendMessage(Skywars.PREFIX + "§e" + skywarsNameColorAdapter.getName(p, player) + " §7wurde von §e" + skywarsNameColorAdapter.getName(lastDamager, player) + " §7getötet."));
            lastDamager.sendMessage(Skywars.PREFIX + "+§e10 Coins");
            Skywars.getInstance().getKillsManager().addKill(lastDamager);
            Bukkit.getScheduler().runTaskAsynchronously(BukkitPluginMain.getInstance(), () -> {
                Skywars.getInstance().getSkywarsModuleManager().getSkywarsCoinsAdapter().addCoins(lastDamager, 10);
                Stats stats = Skywars.getInstance().getSkywarsModuleManager().getSkywarsStatsAdapter().getStats(p.getUniqueId());
                if (stats != null) {
                    stats.setKills(stats.getKills() + 1);
                    Skywars.getInstance().getSkywarsModuleManager().getSkywarsStatsAdapter().saveStats(stats);
                }
            });
        } else {
            p.sendMessage(Skywars.PREFIX + "Du bist gestorben.");
            Bukkit.getOnlinePlayers().forEach(player -> player.sendMessage(Skywars.PREFIX + "§e" + skywarsNameColorAdapter.getName(p, player) + " §7ist gestorben."));

        }

        playerTeam.removePlayer(p);
        p.setHealth(20);
        p.setGameMode(GameMode.ADVENTURE);
        p.setVelocity(new Vector(0, 0, 0));
        p.setAllowFlight(true);
        p.setFlying(true);
        if (p.getLocation().getY() < 0) p.teleport(Skywars.getInstance().getGame().getSpectatorSpawn());
        Bukkit.getOnlinePlayers().forEach(all -> {
            if (Skywars.getInstance().getTeamsManager().getTeam(all) != null) {
                all.hidePlayer(p);
            } else {
                p.showPlayer(all);
            }
        });
        Skywars.getInstance().getSkywarsExecutor().checkWinner();
    }


}


