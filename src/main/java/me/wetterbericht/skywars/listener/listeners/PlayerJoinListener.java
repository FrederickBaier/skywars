package me.wetterbericht.skywars.listener.listeners;


import me.wetterbericht.skywars.BukkitPluginMain;
import me.wetterbericht.skywars.Skywars;
import me.wetterbericht.skywars.database.Stats;
import me.wetterbericht.skywars.external.adapter.SkywarsNameColorAdapter;
import me.wetterbericht.skywars.items.Items;
import me.wetterbericht.skywars.scoreboard.ScoreboardManager;
import me.wetterbericht.skywars.scoreboard.ScoreboardTabList;
import me.wetterbericht.skywars.utils.GameState;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoinListener implements Listener {
    private SkywarsNameColorAdapter skywarsNameColorAdapter = Skywars.getInstance().getSkywarsModuleManager().getSkywarsNameColorAdapter();


    @EventHandler(priority = EventPriority.LOWEST)
    public void on(PlayerJoinEvent e) {
        Player p = e.getPlayer();

        //update selected kit
        Stats stats = Skywars.getInstance().getSkywarsModuleManager().getSkywarsStatsAdapter().getStats(p.getUniqueId());
        if (stats != null){
            Skywars.getInstance().getKitManager().selectKit(p, Skywars.getInstance().getKitManager().getKitsContainer().getKits().get(stats.getKit()));
        } else {
            Skywars.getInstance().getKitManager().selectDefaultKit(p);
        }
        p.setHealth(20);
        p.setFoodLevel(25);
        p.setLevel(0);
        p.getInventory().clear();
        p.getInventory().setArmorContents(null);
        if (Bukkit.getOnlinePlayers().size() == Skywars.getInstance().getConfigData().getMaxPlayers()) {
            //SimpleServerGroup simpleServerGroup = CloudAPI.getInstance().getServerGroupData(CloudAPI.getInstance().getGroup());
            //CloudAPI.getInstance().startGameServer(simpleServerGroup, CloudServer.getInstance().getTemplate());
        }
        e.setJoinMessage(null);
        Bukkit.getOnlinePlayers().forEach(ScoreboardManager::updateScoreboard);
        if (Skywars.getInstance().getGameState() == GameState.LOBBY) {
            Bukkit.getScheduler().runTaskLater(BukkitPluginMain.getInstance(), () -> Bukkit.getOnlinePlayers().forEach(player -> player.sendMessage("§8» " + skywarsNameColorAdapter.getGroupColor(p, player) + skywarsNameColorAdapter.getName(p, player) + " §7hat das Spiel betreten.")), 5);
            p.getInventory().setItem(0, Items.VOTE_ITEM);
            p.getInventory().setItem(1, Items.TEAMS_ITEM);
            p.getInventory().setItem(2, Items.KITS_ITEM);
            Skywars.getInstance().getSkywarsExecutor().checkEnoughPlayers();
            Bukkit.getScheduler().runTaskLater(BukkitPluginMain.getInstance(), () -> {
                p.setGameMode(GameMode.ADVENTURE);
                p.teleport(Skywars.getInstance().getConfigData().getLobbyLocation());
            }, 2);
            ScoreboardTabList.setPlayerInPermissionTeam(p);
        } else {
            Bukkit.getOnlinePlayers().forEach(all -> {
                if (Skywars.getInstance().getTeamsManager().getTeam(all) != null) {
                    all.hidePlayer(p);
                }
            });
            p.setGameMode(GameMode.ADVENTURE);
            Bukkit.getScheduler().runTaskLater(BukkitPluginMain.getInstance(), () -> {
                p.setGameMode(GameMode.ADVENTURE);
                p.teleport(Skywars.getInstance().getGame().getSpectatorSpawn());
                p.setAllowFlight(true);
                p.setFlying(true);
            }, 4);
            ScoreboardTabList.setTeam(p, "Spectator");
        }
    }
}
