package me.wetterbericht.skywars.listener.listeners;

import me.wetterbericht.skywars.Skywars;
import me.wetterbericht.skywars.utils.GameState;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;


public class PlayerLoginListener implements Listener {


    @EventHandler
    public void on(PlayerLoginEvent e){
        Player p = e.getPlayer();
        System.out.println(Bukkit.getOnlinePlayers().size());
        if (Skywars.getInstance().getGameState() == GameState.LOBBY){
            //Spieler ist noch nicht auf dem Server
            System.out.println("MaxPlayers: " + Skywars.getInstance().getConfigData().getMaxPlayers());
            if (Skywars.getInstance().getConfigData().getMaxPlayers() != 0 && Bukkit.getOnlinePlayers().size() == Skywars.getInstance().getConfigData().getMaxPlayers()){
                if (!p.hasPermission("skywars.fulljoin"))
                    e.disallow(PlayerLoginEvent.Result.KICK_FULL, Skywars.PREFIX + "Der Server ist voll.");
                Player kick = getCanKickPlayer();
                if (kick == null){
                    e.disallow(PlayerLoginEvent.Result.KICK_FULL, Skywars.PREFIX + "Der Server ist voll.");
                    return;
                }
                kick.kickPlayer(Skywars.PREFIX + "Du wurdest um einem höherrangigen Spieler platz zu machen gekickt.");
            }
        }
        if (Skywars.getInstance().getGameState() == GameState.STARTING){
            e.disallow(PlayerLoginEvent.Result.KICK_OTHER, Skywars.PREFIX + "Der Server startet noch.");
        }
        if (Skywars.getInstance().getGameState() == GameState.ENDING){
            e.disallow(PlayerLoginEvent.Result.KICK_OTHER, Skywars.PREFIX + "Die Runde ist bereits vorbei.");
        }
    }

    public Player getCanKickPlayer(){
        return Bukkit.getOnlinePlayers().stream().filter(player-> !player.hasPermission("skywars.fulljoin")).findFirst().orElse(null);
    }

}
