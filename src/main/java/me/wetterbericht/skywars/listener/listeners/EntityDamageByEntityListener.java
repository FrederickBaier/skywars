package me.wetterbericht.skywars.listener.listeners;

import me.wetterbericht.skywars.Skywars;
import me.wetterbericht.skywars.game.Team;
import me.wetterbericht.skywars.utils.GameState;
import me.wetterbericht.skywars.utils.LastDamager;
import me.wetterbericht.skywars.vote.TeamsManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class EntityDamageByEntityListener implements Listener {

    @EventHandler
    public void on(EntityDamageByEntityEvent e){
        if (Skywars.getInstance().getGameState() != GameState.INGAME)
            return;
        if (e.getDamager() instanceof Player && e.getEntity() instanceof Player){
            Player damager = (Player) e.getDamager();
            Player player = (Player) e.getEntity();
            TeamsManager teamsManager = Skywars.getInstance().getTeamsManager();
            Team teamDamager = teamsManager.getTeam(damager);
            if (teamDamager == null){
                e.setCancelled(true);
                return;
            }
            Team teamPlayer = teamsManager.getTeam(player);
            if (teamDamager != null && teamPlayer != null){
                if (teamDamager.equals(teamPlayer)) {
                    e.setCancelled(true);
                } else {
                    LastDamager.putLastDamager(player, damager);
                }
            }
        }
    }

}
