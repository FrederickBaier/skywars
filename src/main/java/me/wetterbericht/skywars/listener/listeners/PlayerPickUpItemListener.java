package me.wetterbericht.skywars.listener.listeners;

import me.wetterbericht.skywars.Skywars;
import me.wetterbericht.skywars.utils.GameState;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerPickupItemEvent;

public class PlayerPickUpItemListener implements Listener {

    @EventHandler
    public void on(PlayerPickupItemEvent e){
        if (Skywars.getInstance().getGameState() != GameState.INGAME)
            e.setCancelled(true);
        if (Skywars.getInstance().getTeamsManager().getTeam(e.getPlayer()) == null)
            e.setCancelled(true);
    }

}
