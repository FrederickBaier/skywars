package me.wetterbericht.skywars.listener.listeners;

import me.wetterbericht.skywars.Skywars;
import me.wetterbericht.skywars.external.adapter.SkywarsNameColorAdapter;
import me.wetterbericht.skywars.game.Team;
import me.wetterbericht.skywars.utils.GameState;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class PlayerChatListener implements Listener {

    private SkywarsNameColorAdapter skywarsNameColorAdapter = Skywars.getInstance().getSkywarsModuleManager().getSkywarsNameColorAdapter();

    @EventHandler
    public void on(AsyncPlayerChatEvent e) {
        Player p = e.getPlayer();
        if (e.isCancelled())
            return;
        e.setCancelled(true);
        if (Skywars.getInstance().getGameState() != GameState.INGAME) {
            Bukkit.getOnlinePlayers().forEach(player -> player.sendMessage("§8● " + skywarsNameColorAdapter.getGroupPrefixWithUsername(p, player) + " §8» §f" + e.getMessage()));
        } else {
            Team playerTeam = Skywars.getInstance().getTeamsManager().getTeam(p);
            System.out.println(playerTeam);
            if (playerTeam == null) {
                Bukkit.getOnlinePlayers().forEach(player -> {
                    if (Skywars.getInstance().getTeamsManager().getTeam(player) == null) {
                        player.sendMessage("§7" + skywarsNameColorAdapter.getName(p, player) + " §8» §f" + e.getMessage());
                    }
                });
                return;
            }
            if (Skywars.getInstance().getConfigData().getPlayersPerTeam() == 1) {
                Bukkit.getOnlinePlayers().forEach(player -> player.sendMessage("§8● " + skywarsNameColorAdapter.getGroupPrefixWithUsername(p, player) + " §8» §f" + e.getMessage()));
                return;
            }
            if (e.getMessage().startsWith("@a") || e.getMessage().startsWith("@all") || e.getMessage().startsWith("@")) {
                if (e.getMessage().startsWith("@a"))
                    e.setMessage(e.getMessage().replaceFirst("@a", ""));
                if (e.getMessage().startsWith("@"))
                    e.setMessage(e.getMessage().replaceFirst("@", ""));
                if (e.getMessage().startsWith("@all"))
                    e.setMessage(e.getMessage().replaceFirst("@all", ""));
                while (e.getMessage().startsWith(" ")) {
                    e.setMessage(e.getMessage().replaceFirst(" ", ""));
                }
                Bukkit.getOnlinePlayers().forEach(player -> player.sendMessage("§7[@all] " + "§e" + skywarsNameColorAdapter.getName(p, player) + " §8» §f" + e.getMessage()));

            } else {
                playerTeam.getPlayers().forEach(player -> player.sendMessage("§e" + skywarsNameColorAdapter.getName(p, player) + " §8» §f" + e.getMessage()));
            }
        }
    }

}
