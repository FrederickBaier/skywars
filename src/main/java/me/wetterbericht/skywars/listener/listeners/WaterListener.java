package me.wetterbericht.skywars.listener.listeners;

import me.wetterbericht.skywars.Skywars;
import me.wetterbericht.skywars.executor.SkywarsExecutor;
import me.wetterbericht.skywars.utils.GameState;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockFromToEvent;
import org.bukkit.event.block.BlockPhysicsEvent;


public class WaterListener implements Listener {

    SkywarsExecutor skywarsExecutor = Skywars.getInstance().getSkywarsExecutor();

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onBlockPhysics(BlockPhysicsEvent event) {
        if (Skywars.getInstance().getGameState() == GameState.LOBBY || skywarsExecutor.isFreezePlayers()) {
            Material mat = event.getBlock().getType();
            if (mat == Material.STATIONARY_WATER) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onBlockFromTo(BlockFromToEvent event) {
        if (Skywars.getInstance().getGameState() == GameState.LOBBY || skywarsExecutor.isFreezePlayers()) {
            Block block = event.getToBlock();
            if (block.getType() == Material.WATER) {
                event.setCancelled(true);
            }
        }
    }
}
