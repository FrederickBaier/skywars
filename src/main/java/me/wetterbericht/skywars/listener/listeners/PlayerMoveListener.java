package me.wetterbericht.skywars.listener.listeners;

import me.wetterbericht.skywars.Skywars;
import me.wetterbericht.skywars.executor.SkywarsExecutor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class PlayerMoveListener implements Listener {

    SkywarsExecutor skywarsExecutor = Skywars.getInstance().getSkywarsExecutor();

    @EventHandler
    public void onMove(PlayerMoveEvent e){
        if (skywarsExecutor.isFreezePlayers()){
            if (e.getFrom().getBlockX() != e.getTo().getBlockX() || e.getFrom().getBlockZ() != e.getTo().getBlockZ()){
                e.getPlayer().teleport(e.getFrom());
            }
        }
    }
}
