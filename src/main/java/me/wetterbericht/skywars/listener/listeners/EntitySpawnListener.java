package me.wetterbericht.skywars.listener.listeners;


import net.minecraft.server.v1_8_R3.EntityLiving;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntitySpawnEvent;

public class EntitySpawnListener implements Listener {

    @EventHandler
    public void on(CreatureSpawnEvent e){
        if (!(e.getEntity() instanceof Player))
            e.setCancelled(true);
    }

}
