package me.wetterbericht.skywars.listener.listeners;

import me.wetterbericht.skywars.Skywars;
import me.wetterbericht.skywars.utils.GameState;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;

public class PlayerDropItemListener implements Listener {

    @EventHandler
    public void on(PlayerDropItemEvent e){
        Player p = e.getPlayer();
        if (p.getGameMode() == GameMode.CREATIVE)
            return;
        if (Skywars.getInstance().getGameState() != GameState.INGAME)
            e.setCancelled(true);
    }

}
