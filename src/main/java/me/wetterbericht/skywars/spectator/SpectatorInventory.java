package me.wetterbericht.skywars.spectator;


import me.wetterbericht.inventoryapi.api.InventoryBuilder;
import me.wetterbericht.inventoryapi.api.InventoryItem;
import me.wetterbericht.inventoryapi.api.clicklistener.ClickListener;
import me.wetterbericht.skywars.Skywars;
import me.wetterbericht.skywars.external.adapter.SkywarsNameColorAdapter;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

public class SpectatorInventory {

    private static SkywarsNameColorAdapter skywarsNameColorAdapter = Skywars.getInstance().getSkywarsModuleManager().getSkywarsNameColorAdapter();

    public static void openSpecInventory(Player p){
        int playersAlive = Skywars.getInstance().getTeamsManager().getPlayersAliveCount();
        int size = ((playersAlive / 9) + 1) * 9;
        InventoryBuilder inventoryBuilder = new InventoryBuilder(Skywars.PREFIX + "Teleporter", size);
        for (Player all : Bukkit.getOnlinePlayers()){
            if (Skywars.getInstance().getTeamsManager().getTeam(all) == null)
                continue;
            String name = skywarsNameColorAdapter.getName(all, p);
            String color = skywarsNameColorAdapter.getGroupColor(all, p);
            inventoryBuilder.addInventoryItem(new InventoryItem(getPlayersHead(color, name)).addListener((p1, clickAction) -> {
                if (!all.isOnline() || Skywars.getInstance().getTeamsManager().getTeam(all) == null){
                    p.sendMessage(Skywars.PREFIX + "Dieser Spieler lebt nicht mehr.");
                    return;
                }
                p.closeInventory();
                p.sendMessage(Skywars.PREFIX + "Du wurdest zu " + color + name + " §7teleportiert.");
                p.teleport(all.getLocation());
            }));
        }
        inventoryBuilder.buildInv();
        p.openInventory(inventoryBuilder.getInventory());
    }

    private static ItemStack getPlayersHead(String color, String name){
        ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1, (short) SkullType.PLAYER.ordinal());
        SkullMeta meta = (SkullMeta) skull.getItemMeta();
        meta.setOwner(name);
        meta.setDisplayName(color + name);
        skull.setItemMeta(meta);
        return skull;
    }

}
