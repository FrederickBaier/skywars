package me.wetterbericht.skywars.game;

import com.google.common.collect.Lists;
import lombok.Getter;
import me.wetterbericht.skywars.Skywars;
import org.bukkit.entity.Player;

import java.util.ArrayList;

@Getter
public class Team {

    private int number;
    private ArrayList<Player> players = Lists.newArrayList();

    public Team(int number){
        this.number = number;
    }

    public void addPlayer(Player p){
        players.add(p);
    }

    public void removePlayer(Player p){
        players.remove(p);
    }

    public boolean isFull(){
        return players.size() >= Skywars.getInstance().getConfigData().getPlayersPerTeam();
    }

}
