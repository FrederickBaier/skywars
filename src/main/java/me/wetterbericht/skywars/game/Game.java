package me.wetterbericht.skywars.game;

import com.google.common.collect.Lists;
import lombok.Getter;
import org.bukkit.Location;

import java.util.ArrayList;

@Getter
public class Game {

    private String name;
    private int playersPerTeam;
    private Location spectatorSpawn;
    private ArrayList<TeamSpawn> teams = Lists.newArrayList();
    private ArrayList<ChestPosition> chestPositions = Lists.newArrayList();

    public Game(String name, int playersPerTeam, Location spectatorSpawn){
        this.playersPerTeam = playersPerTeam;
        this.spectatorSpawn = spectatorSpawn;
        this.name = name;
    }

    public void addTeam(TeamSpawn teamSpawn){
        teams.add(teamSpawn);
    }

    public void addChest(ChestPosition chestPosition){
        chestPositions.add(chestPosition);
    }

    public int getMaxPlayers(){
        return teams.size() * playersPerTeam;
    }

    public Location getTeamSpawn(int number){
        return teams.stream().filter(teamSpawn -> teamSpawn.getNumber() == number).findFirst().get().getSpawnLocation();
    }

}
