package me.wetterbericht.skywars.game;

import me.wetterbericht.skywars.items.ChestType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.bukkit.Location;

@Getter
@AllArgsConstructor
public class ChestPosition {

    private ChestType chestType;
    private Location location;

}
