package me.wetterbericht.skywars.game;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.bukkit.Location;

@Getter
@AllArgsConstructor
public class TeamSpawn {

    private int number;
    private Location spawnLocation;

}
