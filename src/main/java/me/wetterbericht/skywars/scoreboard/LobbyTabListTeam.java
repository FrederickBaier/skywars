package me.wetterbericht.skywars.scoreboard;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LobbyTabListTeam {

    private String name;

    private String prefix;

    private String permission;

}
