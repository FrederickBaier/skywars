package me.wetterbericht.skywars.scoreboard;

import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class LobbyTabListTeamManager {

    @Getter
    private ArrayList<LobbyTabListTeam> lobbyTabListTeams = Lists.newArrayList();

    @Getter
    @Setter
    private LobbyTabListTeam allPermissionTabListTeam;


    public void addLobbyTablistTeam(LobbyTabListTeam lobbyTabListTeam){
        lobbyTabListTeams.add(lobbyTabListTeam);
    }

    public void removeLobbyTablistTeam(LobbyTabListTeam lobbyTabListTeam){
        lobbyTabListTeams.remove(lobbyTabListTeam);
    }

    public LobbyTabListTeam getLobbyTabListTeamByName(String name){
        return lobbyTabListTeams.stream().filter(lobbyTabListTeam -> lobbyTabListTeam.getName().equalsIgnoreCase(name)).findFirst().orElse(null);
    }

    public LobbyTabListTeam getLobbyTabListTeam(Player p){
        if (p.hasPermission(allPermissionTabListTeam.getPermission())){
            return allPermissionTabListTeam;
        }
        return lobbyTabListTeams.stream().filter(lobbyTabListTeam -> p.hasPermission(lobbyTabListTeam.getPermission())).findFirst().orElse(null);
    }

}
