package me.wetterbericht.skywars.scoreboard;

import me.wetterbericht.json.JsonData;
import me.wetterbericht.skywars.Skywars;

import java.io.File;

public class LobbyTablistTeamManagerFileHandler {

    private File file = new File(Skywars.PLUGIN_MAIN_DIR + "lobbyTabList.json");

    public void saveToFile(LobbyTabListTeamManager lobbyTabListTeamManager){
        new JsonData().append("data", lobbyTabListTeamManager).saveAsFile(file);
    }

    public LobbyTabListTeamManager loadLobbyTablistTeamManagerFile(){
        if (!file.exists())
            return null;
        return JsonData.fromJsonFile(file).getObject("data", LobbyTabListTeamManager.class);
    }


}
