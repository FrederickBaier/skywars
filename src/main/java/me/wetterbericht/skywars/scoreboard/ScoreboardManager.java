package me.wetterbericht.skywars.scoreboard;

import me.wetterbericht.scoreboardapi.ScoreboardEntry;
import me.wetterbericht.scoreboardapi.SimplePlayerScoreboardManager;
import me.wetterbericht.scoreboardapi.SimpleScoreboard;
import me.wetterbericht.skywars.BukkitPluginMain;
import me.wetterbericht.skywars.Skywars;
import me.wetterbericht.skywars.utils.GameState;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class ScoreboardManager {

    private static SimplePlayerScoreboardManager simplePlayerScoreboardManagerLobby = new SimplePlayerScoreboardManager();


    public static void updateScoreboard(Player player) {
        if (simplePlayerScoreboardManagerLobby.hasScoreboard(player)) {
            SimpleScoreboard simpleScoreboard = simplePlayerScoreboardManagerLobby.getScorebaord(player);
            //Map
            simpleScoreboard.getFirstScoreboardEntry(6).setChangeableText("§e" + (Skywars.getInstance().getGame() == null ? "Voting..." : Skywars.getInstance().getGame().getName()));
            //Kit
            if (Skywars.getInstance().getKitManager().hasSelectedKit(player)){
                simpleScoreboard.getFirstScoreboardEntry(3).setChangeableText("§e" + Skywars.getInstance().getKitManager().getSelectedKit(player).getName());
            } else {
                simpleScoreboard.getFirstScoreboardEntry(3).setChangeableText("§e-");
            }
            //Player
            simpleScoreboard.getFirstScoreboardEntry(1).setChangeableText("§7" + (Skywars.getInstance().getGameState() == GameState.LOBBY ? "Spieler" : "Kills"));
            if (Skywars.getInstance().getGameState() == GameState.LOBBY)
                simpleScoreboard.getFirstScoreboardEntry(0).setChangeableText("§e" + Bukkit.getOnlinePlayers().size() + "§7/§e" + Skywars.getInstance().getConfigData().getMaxPlayers());
            else
                simpleScoreboard.getFirstScoreboardEntry(0).setChangeableText("§e" + Skywars.getInstance().getKillsManager().getKills(player));
        } else {
            SimpleScoreboard simpleScoreboard = simplePlayerScoreboardManagerLobby.getNewScoreboard(player, "§8» §aSkywars §8«");
            simpleScoreboard.addScoreboardEntry(8, "   ");
            simpleScoreboard.addScoreboardEntry(7, "§7Map");
            simpleScoreboard.addScoreboardEntry(6, "§8» ", "§e" + (Skywars.getInstance().getGame() == null ? "Voting..." : Skywars.getInstance().getGame().getName()));
            simpleScoreboard.addScoreboardEntry(5, "  ");
            simpleScoreboard.addScoreboardEntry(4, "§7Kit");
            simpleScoreboard.addScoreboardEntry(3, "§8» ", "§e" + Skywars.getInstance().getKitManager().getSelectedKit(player).getName());
            simpleScoreboard.addScoreboardEntry(2, " ");
            simpleScoreboard.addScoreboardEntry(1, "", "§7Spieler");
            simpleScoreboard.addScoreboardEntry(0, "§8» ", "§e" + Bukkit.getOnlinePlayers().size() + "§7/§e" + Skywars.getInstance().getConfigData().getMaxPlayers());
        }
    }

}
/*
public static void updateIngameScoreboard(Player player){

        if (simplePlayerScoreboardManagerLobby.hasScoreboard(player)){
            simplePlayerScoreboardManagerLobby.getScorebaord(player).destroySidebar(player);
        }

        if (simplePlayerScoreboardManagerIngame.hasScoreboard(player)){

                SimpleScoreboard simpleScoreboard = simplePlayerScoreboardManagerIngame.getScorebaord(player);
                simpleScoreboard.getFirstScoreboardEntry(6).setChangeableText("§e" + (Skywars.getInstance().getGame() == null ? "Voting..." : Skywars.getInstance().getGame().getName()));
                ScoreboardEntry scoreboardEntry = simpleScoreboard.getFirstScoreboardEntry(3);
                if (Skywars.getInstance().getTeamsManager().getTeam(player) != null) {
                if (scoreboardEntry == null){
                System.out.println("create Kit update for Player: " + player.getName());
                simpleScoreboard.addScoreboardEntry(4, "§7Kit");
                simpleScoreboard.addScoreboardEntry(3, "§8» ", "§e" + Skywars.getInstance().getKitManager().getSelectedKit(player).getName());
                simpleScoreboard.addScoreboardEntry(2, " ");
                } else {
                System.out.println("update kit for Player: " + player.getName());
                scoreboardEntry.setChangeableText("§e" + Skywars.getInstance().getKitManager().getSelectedKit(player).getName());
                }

                } else {
                if (scoreboardEntry != null){
                simpleScoreboard.removeScoreboardEntry(simpleScoreboard.getFirstScoreboardEntry(4));
                simpleScoreboard.removeScoreboardEntry(simpleScoreboard.getFirstScoreboardEntry(3));
                simpleScoreboard.removeScoreboardEntry(simpleScoreboard.getFirstScoreboardEntry(2));
                }
                }
                simpleScoreboard.getFirstScoreboardEntry(0).setChangeableText("§e" + Skywars.getInstance().getKillsManager().getKills(player));

                } else {
                System.out.println("Creating new scorebaord for Player: " + player.getName());
                SimpleScoreboard simpleScoreboard = simplePlayerScoreboardManagerIngame.getNewScoreboard(player, "§8» §aSkywars §8«");
                simpleScoreboard.addScoreboardEntry(8, "   ");
                simpleScoreboard.addScoreboardEntry(7, "§7Map");
                simpleScoreboard.addScoreboardEntry(6, "§8» ", "§e" + (Skywars.getInstance().getGame() == null ? "Voting..." : Skywars.getInstance().getGame().getName()));
                simpleScoreboard.addScoreboardEntry(5, "  ");
                if (Skywars.getInstance().getTeamsManager().getTeam(player) != null) {
                System.out.println("Kit for Player: " + player.getName());
                simpleScoreboard.addScoreboardEntry(4, "§7Kit");
                simpleScoreboard.addScoreboardEntry(3, "§8» ", "§e" + Skywars.getInstance().getKitManager().getSelectedKit(player).getName());
                simpleScoreboard.addScoreboardEntry(2, " ");
                }
                simpleScoreboard.addScoreboardEntry(1, "§7Kills");
                simpleScoreboard.addScoreboardEntry(0, "§8» ", "§e" + Skywars.getInstance().getKillsManager().getKills(player));
                }
                }
 */