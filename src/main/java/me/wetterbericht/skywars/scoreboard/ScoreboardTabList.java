package me.wetterbericht.skywars.scoreboard;


import lombok.Getter;
import me.wetterbericht.skywars.Skywars;
import me.wetterbericht.skywars.utils.GameState;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

public class ScoreboardTabList {

    @Getter
    private static LobbyTabListTeamManager lobbyTabListTeamManager = new LobbyTablistTeamManagerFileHandler().loadLobbyTablistTeamManagerFile();

    public static void registerTeams() {
        if (lobbyTabListTeamManager == null)
            lobbyTabListTeamManager = new LobbyTabListTeamManager();
        final Scoreboard s = Bukkit.getScoreboardManager().getMainScoreboard();
        for (final Team t : s.getTeams()) {
            t.unregister();
        }
        for (me.wetterbericht.skywars.game.Team team : Skywars.getInstance().getTeamsManager().getTeams()) {
            registerTeam("Team" + team.getNumber(), "§eTeam " + (team.getNumber() + 1) + " §8| §7");
        }
        for (LobbyTabListTeam lobbyTabListTeam : lobbyTabListTeamManager.getLobbyTabListTeams()) {
            registerTeam(lobbyTabListTeam.getName(), lobbyTabListTeam.getPrefix());
        }
        if (lobbyTabListTeamManager.getAllPermissionTabListTeam() != null)
            registerTeam(lobbyTabListTeamManager.getAllPermissionTabListTeam().getName(), lobbyTabListTeamManager.getAllPermissionTabListTeam().getPrefix());
        registerTeam("Spectator", "§7Spec" + " §8| §7");
    }

    private static void registerTeam(final String name, final String prefix) {
        final Scoreboard s = Bukkit.getScoreboardManager().getMainScoreboard();
        s.registerNewTeam(name).setPrefix(prefix);
    }

    public static void setTeam(Player p, String teamName) {
        Scoreboard s = Bukkit.getScoreboardManager().getMainScoreboard();
        Team t = s.getTeam(teamName);
        if (t != null) {
            t.addEntry(p.getName());
        }
        //UPDATE SB FOR ALL PLAYERS
        Bukkit.getOnlinePlayers().forEach(player -> ScoreboardManager.updateScoreboard(player));
    }

    public static void removeFromTeam(Player p) {
        Scoreboard s = Bukkit.getScoreboardManager().getMainScoreboard();
        Team t = s.getEntryTeam(p.getName());
        if (t != null) {
            t.removeEntry(p.getName());
        }
    }

    public static void setPlayerInPermissionTeam(Player p) {
        LobbyTabListTeam lobbyTabListTeam = lobbyTabListTeamManager.getLobbyTabListTeam(p);
        if (lobbyTabListTeam != null) {
            setTeam(p, lobbyTabListTeam.getName());
        }
    }

}
