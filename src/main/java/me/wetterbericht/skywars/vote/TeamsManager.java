package me.wetterbericht.skywars.vote;

import com.google.common.collect.Lists;
import lombok.Getter;
import me.wetterbericht.inventoryapi.api.InventoryBuilder;
import me.wetterbericht.inventoryapi.api.InventoryItem;
import me.wetterbericht.inventoryapi.utils.ItemCreator;
import me.wetterbericht.skywars.Skywars;
import me.wetterbericht.skywars.external.adapter.SkywarsNameColorAdapter;
import me.wetterbericht.skywars.game.Team;
import me.wetterbericht.skywars.scoreboard.ScoreboardTabList;
import org.bukkit.DyeColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import java.util.ArrayList;

public class TeamsManager {

    private SkywarsNameColorAdapter skywarsNameColorAdapter = Skywars.getInstance().getSkywarsModuleManager().getSkywarsNameColorAdapter();

    @Getter
    private Team[] teams;

    public TeamsManager(){
        this.teams = new Team[Skywars.getInstance().getConfigData().getTeams()];
        for (int i = 0; i < this.teams.length; i++){
            this.teams[i] = new Team(i);
        }
    }

    public Inventory getTeamsInventory(Player forPlayer) {
        InventoryBuilder inventoryBuilder = new InventoryBuilder(Skywars.PREFIX + "§eTeams", 9);
        inventoryBuilder.fillAll(ItemCreator.createGlass(" ", DyeColor.BLACK));
        double plus = 8 / (getTemsSize() - 1);
        double index = 0;
        for (Team team : teams) {
            ArrayList<String> playerNames = Lists.newArrayList();
            team.getPlayers().forEach(player -> playerNames.add("§8» §e" + skywarsNameColorAdapter.getName(player, forPlayer)));
            inventoryBuilder.setInventoryItem(Math.round((float) index), new InventoryItem(ItemCreator.createWoolWithLore("§7Team §e" + (team.getNumber() + 1), DyeColor.WHITE, 1, playerNames)).addListener((p, clickAction) -> {
                if (team.isFull()){
                    p.sendMessage(Skywars.PREFIX + "Dieses Team ist voll.");
                    return;
                }
                for (Team t : teams) {
                    t.removePlayer(p);
                }
                team.addPlayer(p);
                ScoreboardTabList.setTeam(p, "Team" + team.getNumber());
                p.sendMessage(Skywars.PREFIX + "Du bist nun in Team §e" + (team.getNumber() + 1) + "§7.");
                p.closeInventory();
            }));
            index += plus;
        }
        inventoryBuilder.buildInv();
        return inventoryBuilder.getInventory();
    }

    public int getTemsSize(){
        return teams.length;
    }

    public Team getTeam(Player p){
        for (Team team : teams){
            if (team.getPlayers().contains(p)){
                return team;
            }
        }
        return null;
    }

    public int getPlayersAliveCount(){
        int count = 0;
        for (Team team : teams){
            count += team.getPlayers().size();
        }
        return count;
    }

}
