package me.wetterbericht.skywars.vote;

import com.google.common.collect.Lists;
import me.wetterbericht.inventoryapi.api.InventoryBuilder;
import me.wetterbericht.inventoryapi.api.InventoryItem;

import me.wetterbericht.inventoryapi.utils.ItemCreator;
import me.wetterbericht.skywars.Skywars;
import me.wetterbericht.skywars.game.Game;
import me.wetterbericht.skywars.setupgame.SetupGame;
import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.Random;

public class VoteManager {

    private Skywars skywars = Skywars.getInstance();

    private HashMap<SetupGame, Integer> votes = new HashMap<>();
    private HashMap<Player, SetupGame> playerVotes = new HashMap<>();

    private boolean finished;

    public VoteManager() {
        for (SetupGame setupGame : skywars.getAvailableGames()) {
            this.votes.put(setupGame, 0);
        }
    }

    public void resetFinishedVoting(){
        this.finished = false;
    }

    public Inventory getVoteInventory() {
        InventoryBuilder inventoryBuilder = new InventoryBuilder(Skywars.PREFIX + "Vote", 9);
        inventoryBuilder.fillAll(ItemCreator.createGlass(" ", DyeColor.BLACK));
        for (int i = 0; i < this.skywars.getAvailableGames().size(); i++) {
            SetupGame setupGame = this.skywars.getAvailableGames().get(i);
            inventoryBuilder.setInventoryItem(i, new InventoryItem(ItemCreator.create(Material.EMPTY_MAP, "§e" + setupGame.getName())).addListener((p, clickAction) -> {
                if (this.finished) {
                    p.closeInventory();
                    return;
                }
                if (playerVotes.containsKey(p)) {
                    SetupGame playerVotedGame = this.playerVotes.get(p);
                    this.votes.put(playerVotedGame, votes.get(playerVotedGame) - 1);
                }
                this.playerVotes.put(p, setupGame);
                this.votes.put(setupGame, this.votes.get(setupGame) + 1);
                p.playSound(p.getLocation(), Sound.NOTE_PLING, 0.7F, 0.5F);
            }));
        }
        inventoryBuilder.buildInv();
        return inventoryBuilder.getInventory();
    }

    public Game getWinner() {
        this.finished = true;
        Optional<Integer> maxVotesOptional = this.votes.values().stream().reduce(Integer::max);
        if (!maxVotesOptional.isPresent()) {
            Bukkit.getLogger().warning("No Games found. Can't start Game. Please add Maps first.");
            return null;
        }
        List<SetupGame> mostVotedGames = Lists.newArrayList();
        votes.forEach((setupGame, integer) -> {
            if (integer == maxVotesOptional.get()) {
                mostVotedGames.add(setupGame);
            }
        });
        if (mostVotedGames.size() == 1)
            return mostVotedGames.get(0).toGame();
        Random rnd = new Random();
        return mostVotedGames.get(rnd.nextInt(mostVotedGames.size() - 1)).toGame();
    }

}
