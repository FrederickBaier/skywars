package me.wetterbericht.skywars.database;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
public class Stats {

    private UUID uuid;

    private int kills;

    private int deaths;

    private int playedGames;

    private List<Integer> playerKits;

    private int kit;
}
