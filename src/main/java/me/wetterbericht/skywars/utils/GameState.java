package me.wetterbericht.skywars.utils;

public enum GameState {

    STARTING,LOBBY,INGAME, ENDING, STOPPING

}
