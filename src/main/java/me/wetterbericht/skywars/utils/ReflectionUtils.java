package me.wetterbericht.skywars.utils;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ReflectionUtils {

    public static final String VERSION = org.bukkit.Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3];

    public static Class<?> reflectCraftClazz(String suffix) {
        try {
            return Class.forName("org.bukkit.craftbukkit." + VERSION + "." + suffix);
        } catch (Exception ex) {
            try {
                return Class.forName("org.bukkit.craftbukkit." + suffix);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static Class<?> reflectMinecraftClazz(String suffix) {
        try {
            String version = org.bukkit.Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3];
            return Class.forName("net.minecraft.server." + version + "." + suffix);
        } catch (Exception ex) {
            try {
                return Class.forName("net.minecraft.server." + suffix);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    protected static void setValue(Class<?> clazz, Object obj, Object value, String name) {
        System.out.println("set Class " + clazz);
        try {
            Field field = clazz.getDeclaredField(name);
            field.setAccessible(true);
            field.set(obj, value);
        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    protected static Object getValue(Class<?> clazz, Object obj, String name) {
        try {
            Field field = clazz.getDeclaredField(name);
            field.setAccessible(true);
            return field.get(obj);
        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected Object invokeMethod(Class<?> clazz, Object obj, String name, Class<?>[] list, Object... parm) {
        try {
            Method m = clazz.getDeclaredMethod(name, list);
            m.setAccessible(true);
            return m.invoke(obj, parm);
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected Object invokeConstructor(Class<?> clazz, Class<?>[] list, Object... parm) {
        try {
            Constructor<?> constructor = clazz.getConstructor(list);
            return constructor.newInstance(parm);
        } catch (NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }


}

