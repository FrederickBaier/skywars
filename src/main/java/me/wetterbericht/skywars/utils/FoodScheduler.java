package me.wetterbericht.skywars.utils;

import me.wetterbericht.skywars.BukkitPluginMain;
import me.wetterbericht.skywars.Skywars;
import org.bukkit.Bukkit;
import org.bukkit.Difficulty;

public class FoodScheduler {


    public void run() {
        Bukkit.getScheduler().scheduleSyncRepeatingTask(BukkitPluginMain.getInstance(), () -> {
            if (Skywars.getInstance().getGameState() != GameState.INGAME) {
                Bukkit.getOnlinePlayers().forEach(player -> player.setFoodLevel(25));
            } else {
                Bukkit.getOnlinePlayers().forEach(player -> {
                    if (Skywars.getInstance().getTeamsManager().getTeam(player) == null)
                        player.setFoodLevel(25);
                    player.getWorld().setDifficulty(Difficulty.NORMAL);
                    player.getWorld().setTime(2000);
                });
            }
        }, 40, 40);
    }

}
