package me.wetterbericht.skywars.utils;

import java.util.Arrays;
import java.util.List;

public class RotationUtils {

	

	
	public static boolean isAngleBetween(float checkAngle, float angle1, float angle2) {
		float[] array = getYawValuesBetween(angle1, angle2);
		System.out.println("Angle Beween return:" + (checkAngle > array[0] && checkAngle < array[1] || checkAngle > array[2] && checkAngle < array[3]));
		return checkAngle > array[0] && checkAngle < array[1] || checkAngle > array[2] && checkAngle < array[3];
	}
	
	/**
	 * 
	 * @param arg1 from 180 to -180
	 * @param arg2 from 180 to -180
	 * @return
	 */
	private static float[] getYawValuesBetween(float arg1, float arg2) {
		float difference = arg1 - arg2;
		if (difference < 0) {
			difference = -difference;
		}
		System.out.println("difference: " + difference);
		if (difference > 180) {
			//return new float[] {von, bis, oder von, bis};
			return new float[] {-180, Math.min(arg2, arg1), Math.max(arg2, arg1), 180};
		} else {
			return new float[] {Math.min(arg2, arg1), Math.max(arg2, arg1), 0 , 0};
		}
	}
	


	public static float getNewAngle(float angle) {
		angle %= 360.0F;
		if (angle >= 180.0F) {
			angle -= 360.0F;
		}

		if (angle < -180.0F) {
			angle += 360.0F;
		}

		return angle;
	}
	
	public static int getTrend(float from, float to) {
		float delta = from - to;
		if (delta < 0) {
			delta += 360;
		}
		return delta > 180 ? +1 : -1;
		
	}
	
	
	 public static float getDistanceBetweenAngles(float angle1, float angle2) {
	      float angle = Math.abs(angle1 - angle2) % 360.0F;
	      if(angle > 180.0F) {
	         angle = 360.0F - angle;
	      }

	      return angle;
	   }

}
