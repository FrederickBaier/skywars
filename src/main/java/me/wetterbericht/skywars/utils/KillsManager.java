package me.wetterbericht.skywars.utils;

import org.bukkit.entity.Player;

import java.util.HashMap;

public class KillsManager {

    private HashMap<Player, Integer> playerKills = new HashMap<>();


    public void addKill(Player player){
        if (!playerKills.containsKey(player)){
            playerKills.put(player, 1);
        } else {
            playerKills.put(player, playerKills.get(player) + 1);
        }
    }

    public int getKills(Player player){
        if (!playerKills.containsKey(player))
            return 0;
        return playerKills.get(player);
    }

}
