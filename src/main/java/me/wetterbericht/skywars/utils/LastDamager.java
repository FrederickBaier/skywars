package me.wetterbericht.skywars.utils;

import org.bukkit.entity.Player;

import java.util.HashMap;

public class LastDamager {

    private static HashMap<Player, Player> lastDamager = new HashMap<>();
    private static HashMap<Player, Long> lastUpdate = new HashMap<>();

    public static void putLastDamager(Player target, Player damager){
        lastDamager.put(target, damager);
        lastUpdate.put(target, System.currentTimeMillis());
    }

    public static Player getLastDamager(Player target){
        Long millis = lastUpdate.get(target);
        if (millis == null)
            return null;
        if (System.currentTimeMillis() - millis > (1000 * 30))
            return null;
        return lastDamager.get(target);
    }


}
