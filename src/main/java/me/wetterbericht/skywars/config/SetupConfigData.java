package me.wetterbericht.skywars.config;

import me.wetterbericht.skywars.utils.SerializableLocation;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class SetupConfigData {

    private SerializableLocation lobbyLocation;

    private int teams;
    private int playersPerTeam;


    //private String language;

    public ConfigData toConfigData(){
        return new ConfigData(lobbyLocation.toBukkitLocation(), teams, playersPerTeam);
    }

}
