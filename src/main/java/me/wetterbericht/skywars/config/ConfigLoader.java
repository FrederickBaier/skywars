package me.wetterbericht.skywars.config;

import me.wetterbericht.json.JsonData;
import me.wetterbericht.skywars.Skywars;

import java.io.File;

public class ConfigLoader {

    public SetupConfigData loadConfig(){
        File file = new File(Skywars.PLUGIN_MAIN_DIR + "config.json");
        if (!file.exists())
            return null;
        return JsonData.fromJsonFile(file).getObject("data", SetupConfigData.class);
    }

    public void saveConfig(SetupConfigData setupConfigData){
        new JsonData().append("data", setupConfigData).saveAsFile(Skywars.PLUGIN_MAIN_DIR + "config.json");
    }

}
