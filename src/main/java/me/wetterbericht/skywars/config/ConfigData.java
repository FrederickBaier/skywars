package me.wetterbericht.skywars.config;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.bukkit.Location;

@Getter
@AllArgsConstructor
public class ConfigData {

    private Location lobbyLocation;

    private int teams;
    private int playersPerTeam;

    public int getMaxPlayers(){
        return  teams * playersPerTeam;
    }

    //private String language;

}
