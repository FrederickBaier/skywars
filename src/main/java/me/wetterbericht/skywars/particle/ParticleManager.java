package me.wetterbericht.skywars.particle;

import me.wetterbericht.skywars.BukkitPluginMain;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class ParticleManager {

    private HashMap<Location, Effect> effects = new HashMap<>();

    public void addEffect(Location loc, Effect effect){
        effects.put(loc, effect);
    }

    public void startParticleScheduler(){
        Bukkit.getScheduler().scheduleSyncRepeatingTask(BukkitPluginMain.getInstance(), () -> {
            sendParticles();
        }, 5, 5);
    }

    private void sendParticles(){
        for (Location loc : effects.keySet()){
            Effect effect = effects.get(loc);
            loc.getWorld().playEffect(loc, effect, 0);
        }
    }


}
