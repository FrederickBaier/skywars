package me.wetterbericht.skywars.executor;

import com.google.common.collect.Lists;
import lombok.Getter;
import me.wetterbericht.inventoryapi.utils.ItemBuilder;
import me.wetterbericht.skywars.BukkitPluginMain;
import me.wetterbericht.skywars.Skywars;
import me.wetterbericht.skywars.autosetup.Direction;
import me.wetterbericht.skywars.database.Stats;
import me.wetterbericht.skywars.external.events.SkywarsGameEndedEvent;
import me.wetterbericht.skywars.external.events.SkywarsGameStartEvent;
import me.wetterbericht.skywars.game.ChestPosition;
import me.wetterbericht.skywars.game.Game;
import me.wetterbericht.skywars.game.Team;
import me.wetterbericht.skywars.kits.Kit;
import me.wetterbericht.skywars.schematic.IslandPosition;
import me.wetterbericht.skywars.schematic.MapSchematicFile;
import me.wetterbericht.skywars.schematic.MapSchematics;
import me.wetterbericht.skywars.scoreboard.ScoreboardManager;
import me.wetterbericht.skywars.scoreboard.ScoreboardTabList;
import me.wetterbericht.skywars.utils.GameState;
import me.wetterbericht.skywars.utils.SerializableLocation;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.ComponentBuilder;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class SkywarsExecutor {

    private Skywars skywarsInstance = Skywars.getInstance();

    private boolean countdownStarted = false;

    private int count = 60;

    private int bukkitCD;

    @Getter
    private boolean freezePlayers = false;

    private boolean enoughPlayersOnline() {
        return Bukkit.getOnlinePlayers().size() >= skywarsInstance.getConfigData().getPlayersPerTeam() * 2;
    }

    public void checkEnoughPlayers() {
        if (enoughPlayersOnline()) {
            if (!countdownStarted && !Skywars.SETUP) {
                countdownStarted = true;
                startCountDown();
            }
        }
    }

    public boolean forceStart() {
        if (skywarsInstance.getGameState() == GameState.LOBBY) {
            if (this.count <= 10 || count == 60)
                return false;
            this.count = 10;
            return true;
        } else {
            return false;
        }
    }

    private void startCountDown() {

        bukkitCD = Bukkit.getScheduler().scheduleSyncRepeatingTask(BukkitPluginMain.getInstance(), () -> {
            if (Skywars.SETUP) {
                Bukkit.getScheduler().cancelTask(bukkitCD);
                Bukkit.getOnlinePlayers().forEach(player -> player.sendMessage(Skywars.PREFIX + "Der Countdown wurde aufgrund des Setup-Modes abgebrochen."));
                return;
            }
            Bukkit.getOnlinePlayers().forEach(player -> player.setLevel(count));
            if (count == 60 || count == 50 || count == 40 || count == 30 || count == 20 || count == 10 || count == 5 || count == 4 || count == 3 || count == 2 || count == 1) {
                Bukkit.getOnlinePlayers().forEach(player -> player.sendMessage(Skywars.PREFIX + "Das Spiel startet in §e" + count + " §7Sekunden."));
                Bukkit.getOnlinePlayers().forEach(player -> player.playSound(player.getLocation(), Sound.NOTE_BASS, 1, 1));
            }
            if (count == 10) {
                skywarsInstance.setGame(skywarsInstance.getVoteManager().getWinner());
                Game game = skywarsInstance.getGame();
                if (game == null) {
                    count = 60;
                    Bukkit.getScheduler().cancelTask(bukkitCD);
                    Bukkit.getOnlinePlayers().forEach(player -> player.sendMessage(Skywars.PREFIX + "§cEs wurde noch kein Spiel eingerichtet."));
                    return;
                }
                skywarsInstance.getSkywarsModuleManager().getSkywarsCloudAdapter().onGameSelected(game);
                //buildMap(game);
                Bukkit.getOnlinePlayers().forEach(player -> {
                    player.sendMessage(" ");
                    player.sendMessage("    §7Map§8: §e" + game.getName());
                    player.sendMessage(" ");
                    ScoreboardManager.updateScoreboard(player);
                });
            }

            if (count == 0) {
                fillChests();
                Bukkit.getScheduler().cancelTask(bukkitCD);
                count = 60;
                SkywarsGameStartEvent skywarsGameStartEvent = new SkywarsGameStartEvent(Skywars.getInstance().getGame());
                Bukkit.getPluginManager().callEvent(skywarsGameStartEvent);
                if (!enoughPlayersOnline() || skywarsGameStartEvent.isCancelled()) {
                    Skywars.getInstance().setGame(null);
                    skywarsInstance.getSkywarsModuleManager().getSkywarsCloudAdapter().onNoGameSelected();
                    skywarsInstance.getVoteManager().resetFinishedVoting();
                    countdownStarted = false;
                    Bukkit.getOnlinePlayers().forEach(player -> ScoreboardManager.updateScoreboard(player));
                    if (skywarsGameStartEvent.isCancelled()) {
                        Bukkit.getOnlinePlayers().forEach(player -> {
                                    player.sendMessage(Skywars.PREFIX + skywarsGameStartEvent.getCancelMessage());
                                }
                        );
                    } else {
                        Bukkit.getOnlinePlayers().forEach(player -> {
                                    player.sendMessage(Skywars.PREFIX + "Es sind nicht genügend Spieler online um das Spiel zu starten.");
                                }
                        );
                    }
                    return;
                }
                skywarsInstance.setGameState(GameState.INGAME);
                skywarsInstance.getSkywarsModuleManager().getSkywarsCloudAdapter().changeToIngame();
                //CloudServer.getInstance().setServerStateAndUpdate(ServerState.INGAME);
                setPlayersInTeams();
                teleportPlayers();
                addPlayersPlayedGame();
                givePlayerKits();
                freezePlayers = true;
                startNoMoveCountDown();
                Bukkit.getOnlinePlayers().forEach(player -> {
                            player.sendMessage(Skywars.PREFIX + "§eDas Spiel started jetzt.");
                            player.playSound(player.getLocation(), Sound.NOTE_PLING, 1, 1);
                        }
                );
                return;
            }
            count--;
        }, 20, 20);
    }

    private void buildMap(Game game) {
        File file = new File(Skywars.GAMES_DIR + game.getName() + ".swschematic");
        try {
            MapSchematics mapSchematics = new MapSchematicFile(file).loadMapSchematic();
            SerializableLocation middlelocation = mapSchematics.getMiddleLocation();
            mapSchematics.getMiddleIsland().paste(Direction.UP, middlelocation.toBukkitLocation("Void"), (int) 9.5 * 20);
            for (IslandPosition islandPosition : mapSchematics.getIslandPositions()) {
                mapSchematics.getIsland().paste(islandPosition.getDirection(), islandPosition.getSerializableLocation().toBukkitLocation("Void"), (int) 9.5 * 20);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void addPlayersPlayedGame() {
        Bukkit.getScheduler().runTaskAsynchronously(BukkitPluginMain.getInstance(), () -> Bukkit.getOnlinePlayers().forEach(player -> {
            Stats stats = Skywars.getInstance().getSkywarsModuleManager().getSkywarsStatsAdapter().getStats(player.getUniqueId());
            stats.setPlayedGames(stats.getPlayedGames() + 1);
            Skywars.getInstance().getSkywarsModuleManager().getSkywarsStatsAdapter().saveStats(stats);
        }));
    }

    private void givePlayerKits() {
        for (Player p : Bukkit.getOnlinePlayers()) {
            Kit kit = Skywars.getInstance().getKitManager().getSelectedKit(p);
            p.getInventory().setArmorContents(kit.getArmorContents());
            for (ItemBuilder itemBuilder : kit.getInventoryItems()) {
                p.getInventory().addItem(itemBuilder.buildItemStack());
            }
        }
    }

    private void startNoMoveCountDown() {
        count = 5;
        bukkitCD = Bukkit.getScheduler().scheduleSyncRepeatingTask(BukkitPluginMain.getInstance(), () -> {
            if (count == 0) {
                freezePlayers = false;
                Bukkit.getScheduler().cancelTask(bukkitCD);
                Bukkit.getOnlinePlayers().forEach(player -> {
                    player.sendMessage(Skywars.PREFIX + "Die Schutzzeit ist vorbei.");
                    player.setGameMode(GameMode.SURVIVAL);
                    player.playSound(player.getLocation(), Sound.NOTE_PLING, 1, 1);
                });
            } else {
                Bukkit.getOnlinePlayers().forEach(player -> {
                    player.sendMessage(Skywars.PREFIX + "Die Schutzzeit endet in §e" + count + " §7Sekunden.");
                    player.playSound(player.getLocation(), Sound.NOTE_BASS, 1, 1);
                });
            }
            count--;
        }, 20, 20);
    }

    private void fillChests() {
        Game game = skywarsInstance.getGame();
        for (ChestPosition chestPosition : game.getChestPositions()) {
            Block block = chestPosition.getLocation().getBlock();
            if (!(block.getState() instanceof Chest)) {
                Bukkit.getLogger().warning("Can't find Chest on Location " + block.getLocation().toString());
                continue;
            }
            Chest chest = (Chest) block.getState();
            Inventory inv = chest.getBlockInventory();
            inv.clear();
            skywarsInstance.getChestItems().fillInventory(inv, chestPosition.getChestType());
        }
    }

    private void teleportPlayers() {
        for (Team team : skywarsInstance.getTeamsManager().getTeams()) {
            Location teamSpawn = skywarsInstance.getGame().getTeamSpawn(team.getNumber());
            team.getPlayers().forEach(player -> {
                player.teleport(teamSpawn);
                player.closeInventory();
                player.getInventory().clear();
                ScoreboardManager.updateScoreboard(player);
            });
        }
    }

    private void setPlayersInTeams() {
        List<Player> playersInTeams = Lists.newArrayList();
        for (Team team : skywarsInstance.getTeamsManager().getTeams()) {
            playersInTeams.addAll(team.getPlayers());
        }
        List<Player> playersWithoutTeam = Lists.newArrayList();
        playersWithoutTeam.addAll(Bukkit.getOnlinePlayers());
        playersWithoutTeam.removeAll(playersInTeams);
        playersWithoutTeam.forEach(player -> {
            for (Team team : skywarsInstance.getTeamsManager().getTeams()) {
                if (!team.isFull()) {
                    team.addPlayer(player);
                    player.sendMessage(Skywars.PREFIX + "Du bist nun in Team §e" + (team.getNumber() + 1) + "§7.");
                    ScoreboardTabList.setTeam(player, "Team" + team.getNumber());
                    break;
                }
            }
        });
    }

    public void checkWinner() {
        Team winner = null;
        for (Team team : skywarsInstance.getTeamsManager().getTeams()) {
            if (team.getPlayers().size() != 0) {
                if (winner != null)
                    return;
                winner = team;
            }
        }
        skywarsInstance.setGameState(GameState.ENDING);
        for (Player player : Bukkit.getOnlinePlayers()) {
            Bukkit.getOnlinePlayers().forEach(player::showPlayer);
            player.getInventory().clear();
            player.getInventory().setArmorContents(null);
            player.setGameMode(GameMode.ADVENTURE);
            player.teleport(skywarsInstance.getConfigData().getLobbyLocation());
            player.sendMessage(Skywars.PREFIX + "§eTeam " + (winner.getNumber() + 1) + " §7hat das Spiel gewonnen.");
            player.sendTitle("§eTeam " + (winner.getNumber() + 1), "§7hat das Spiel gewonnen");
            player.setFlying(false);
            player.setAllowFlight(false);
        }
        for (Player winnerPlayers : winner.getPlayers()) {
            Bukkit.getScheduler().runTaskAsynchronously(BukkitPluginMain.getInstance(), () -> skywarsInstance.getSkywarsModuleManager().getSkywarsCoinsAdapter().addCoins(winnerPlayers, 50));
            winnerPlayers.sendMessage(Skywars.PREFIX + "+§e50 " + Skywars.getInstance().getSkywarsModuleManager().getSkywarsCoinsAdapter().getCoinsName());
        }
        this.freezePlayers = false;
        Bukkit.getScheduler().cancelTask(bukkitCD);
        startEndCountdown();
        Bukkit.getPluginManager().callEvent(new SkywarsGameEndedEvent(Skywars.getInstance().getGame(), winner));
        /*
        Bukkit.getScheduler().runTaskLater(BukkitPluginMain.getInstance(), () -> {
            for (Player winnerPlayers : finalWinner.getPlayers()) {
                NickPlayer nickPlayer = NickAPI.getInstance().getNickPlayer(winnerPlayers);
                if (!(nickPlayer instanceof EmptyNickPlayer)) {
                    NickAPI.getInstance().unnickPlayer(nickPlayer);
                }
            }
        }, 2);
        */
    }

    private void startEndCountdown() {
        count = 10;
        bukkitCD = Bukkit.getScheduler().scheduleSyncRepeatingTask(BukkitPluginMain.getInstance(), () -> {
            if (count == 1) {
                Skywars.getInstance().setGameState(GameState.STOPPING);
            }
            if (count == 0) {
                Bukkit.getOnlinePlayers().forEach(player -> player.kickPlayer(Skywars.PREFIX + "Der Server startet jetzt neu."));
            } else {
                Bukkit.getOnlinePlayers().forEach(player -> player.sendMessage(Skywars.PREFIX + "Der Server startet in §e" + count + " §7Sekunden neu."));
            }
            if (count == -1) {
                Bukkit.getScheduler().cancelTask(bukkitCD);
                Bukkit.shutdown();
            }
            count--;
        }, 20, 20);
    }
}
