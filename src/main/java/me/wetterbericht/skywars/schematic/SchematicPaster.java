package me.wetterbericht.skywars.schematic;

import me.wetterbericht.skywars.BukkitPluginMain;
import me.wetterbericht.skywars.autosetup.Direction;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class SchematicPaster {

    private int cd;


    private List<Location> locations;

    private int index = 0;

    public void paste(HashMap<Location, BlockType> map, int ticks){
        this.locations = new ArrayList<>(map.keySet());
        int blocksPerTick = locations.size() / ticks;
        cd = Bukkit.getScheduler().scheduleSyncRepeatingTask(BukkitPluginMain.getInstance(), () -> {
            int bound = locations.size() < index + blocksPerTick ? locations.size() : index + blocksPerTick;
            for (int i = index; i < bound; i++){
                Location loc = locations.get(i);
                Block block = loc.getBlock();
                BlockType blockType = map.get(loc);
                if (map.get(loc).getId() == 54){
                    System.out.println("placing block with sub id " + blockType.getSubId());
                }
                block.setTypeIdAndData(blockType.getId(), (byte) blockType.getSubId(), true);
            }
            index += blocksPerTick;
            if (index >= locations.size()){
                Bukkit.getScheduler().cancelTask(cd);
            }
        }, 1, 1);
    }


}
