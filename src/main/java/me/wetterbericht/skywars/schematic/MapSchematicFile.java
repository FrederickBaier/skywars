package me.wetterbericht.skywars.schematic;

import me.wetterbericht.skywars.autosetup.Direction;
import me.wetterbericht.skywars.autosetup.Island2DLocation;
import me.wetterbericht.skywars.utils.SerializableLocation;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class MapSchematicFile {

    private final File file;

    public MapSchematicFile(File file) {
        this.file = file;
    }

    public MapSchematics loadMapSchematic() throws IOException {
        ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(file));
        SerializableLocation middleLocation = readLocation(objectInputStream);
        List<IslandPosition> islandPositions = new ArrayList<>();
        int locationsSize = objectInputStream.readInt();
        for (int i = 0; i < locationsSize; i++){
            Direction direction = Direction.getDirectionByNumber(objectInputStream.readInt());
            SerializableLocation serializableLocation = readLocation(objectInputStream);
            islandPositions.add(new IslandPosition(serializableLocation, direction));
        }
        return new MapSchematics(readSchematic(objectInputStream), readSchematic(objectInputStream), middleLocation, islandPositions);
    }

    //middle location, locations, middle schematic, island schematic

    public void saveMapSchematic(MapSchematics mapSchematics) throws IOException {
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(file));
        writeLocation(objectOutputStream, mapSchematics.getMiddleLocation());
        objectOutputStream.writeInt(mapSchematics.getIslandPositions().size());
        for (IslandPosition islandPosition : mapSchematics.getIslandPositions()) {
            objectOutputStream.writeInt(islandPosition.getDirection().getNumber());
            writeLocation(objectOutputStream, islandPosition.getSerializableLocation());
        }
        saveSchematic(objectOutputStream, mapSchematics.getMiddleIsland());
        saveSchematic(objectOutputStream, mapSchematics.getIsland());
        objectOutputStream.flush();
        objectOutputStream.close();
    }

    private void saveSchematic(ObjectOutputStream objectOutputStream, Schematic schematic) throws IOException {
        objectOutputStream.writeInt(schematic.getDifX());
        objectOutputStream.writeInt(schematic.getDifY());
        objectOutputStream.writeInt(schematic.getDifZ());
        objectOutputStream.writeInt(schematic.getTypes().size());
        for (BlockType blockType : schematic.getTypes()) {
            objectOutputStream.writeInt(blockType.getId());
            objectOutputStream.writeByte((byte) blockType.getSubId());
        }
    }

    private Schematic readSchematic(ObjectInputStream objectInputStream) throws IOException {
        int difX = objectInputStream.readInt();
        int difY = objectInputStream.readInt();
        int difZ = objectInputStream.readInt();
        int typesCount = objectInputStream.readInt();
        List<BlockType> list = new ArrayList<>();
        for (int i = 0 ; i < typesCount; i++){
            list.add(new BlockType(objectInputStream.readInt(), (int) objectInputStream.readByte()));
        }
        return new Schematic(difX, difY, difZ, list, null);
    }

    private void writeLocation(ObjectOutputStream objectOutputStream, SerializableLocation serializableLocation) throws IOException {
        objectOutputStream.writeInt((int) serializableLocation.getX());
        objectOutputStream.writeInt((int) serializableLocation.getY());
        objectOutputStream.writeInt((int) serializableLocation.getZ());
    }

    private SerializableLocation readLocation(ObjectInputStream objectInputStream) throws IOException {
        return new SerializableLocation(null, objectInputStream.readInt(), objectInputStream.readInt(), objectInputStream.readInt());
    }

}
