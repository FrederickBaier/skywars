package me.wetterbericht.skywars.schematic;

import lombok.AllArgsConstructor;
import lombok.Getter;
import me.wetterbericht.skywars.autosetup.Direction;
import me.wetterbericht.skywars.utils.SerializableLocation;

import java.util.List;

@AllArgsConstructor
@Getter
public class IslandPosition {

    private SerializableLocation serializableLocation;

    private Direction direction;

}
