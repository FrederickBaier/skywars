package me.wetterbericht.skywars.schematic;

import lombok.AllArgsConstructor;
import lombok.Getter;
import me.wetterbericht.skywars.utils.SerializableLocation;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Getter
public class MapSchematics {

    private Schematic middleIsland;

    private Schematic island;

    private SerializableLocation middleLocation;

    private List<IslandPosition> islandPositions = new ArrayList<>();

}
