package me.wetterbericht.skywars.schematic;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class BlockType {

    private int id;


    private int subId;


}
