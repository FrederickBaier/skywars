package me.wetterbericht.skywars.schematic;

import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import lombok.Getter;
import me.wetterbericht.skywars.BukkitPluginMain;
import me.wetterbericht.skywars.autosetup.Direction;
import me.wetterbericht.skywars.utils.SerializableLocation;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;

import java.util.ArrayList;
import java.util.HashMap;

import java.util.List;

@AllArgsConstructor
public class Schematic {

    public static Schematic EMPTY = new Schematic(0, 0, 0, Lists.newArrayList(), null);

    @Getter
    private int difX;

    @Getter
    private int difY;

    @Getter
    private int difZ;

    @Getter
    private List<BlockType> types;


    private Location testLoc;

    public void paste(Direction direction, Location pasteLoc, int ticks) {
        int index = 0;
        HashMap<Location, BlockType> map = new HashMap<>();
        switch (direction) {
            case UP:
                for (int x = 0; x <= getDifX(); x++) {
                    for (int z = 0; z <= getDifZ(); z++) {
                        for (int y = 0; y <= getDifY(); y++) {
                            BlockType blockType = types.get(index);
                            index++;
                            if (blockType.getId() == 0)
                                continue;
                            if (blockType.getId() == 54) {
                                int id = (((blockType.getSubId() - 2) + direction.getNumber()) % 4) + 2;
                                blockType.setSubId(id);
                            }
                            map.put(pasteLoc.clone().add(x, y, z), blockType);
                        }
                    }
                }
                break;
            case LEFT:
                for (int z = getDifX(); z >= 0; z--) {
                    for (int x = 0; x <= getDifZ(); x++) {
                        for (int y = 0; y <= getDifY(); y++) {
                            BlockType blockType = types.get(index);
                            index++;
                            if (blockType.getId() == 0)
                                continue;
                            if (blockType.getId() == 54) {
                                int id = (((blockType.getSubId() - 2) + direction.getNumber()) % 4) + 2;
                                blockType.setSubId(id);
                                if (testLoc == null) {
                                    System.out.println("Tes loc su id init " + blockType.getSubId());
                                    testLoc = pasteLoc.clone().add(x, y, z);
                                }
                            }
                            map.put(pasteLoc.clone().add(x, y, z), blockType);
                        }
                    }
                }
                break;
            case DOWN:
                for (int x = getDifX(); x >= 0; x--) {
                    for (int z = getDifZ(); z >= 0; z--) {
                        for (int y = 0; y <= getDifY(); y++) {
                            BlockType blockType = types.get(index);
                            index++;
                            if (blockType.getId() == 0)
                                continue;
                            if (blockType.getId() == 54) {
                                int id = (((blockType.getSubId() - 2) + direction.getNumber()) % 4) + 2;
                                blockType.setSubId(id);
                            }
                            map.put(pasteLoc.clone().add(x, y, z), blockType);
                        }
                    }
                }
                break;
            case RIGHT:
                for (int z = 0; z <= getDifX(); z++) {
                    for (int x = getDifZ(); x >= 0; x--) {
                        for (int y = 0; y <= getDifY(); y++) {
                            BlockType blockType = types.get(index);
                            index++;
                            if (blockType.getId() == 0)
                                continue;
                            if (blockType.getId() == 54) {
                                int id = (((blockType.getSubId() - 2) + direction.getNumber()) % 4) + 2;
                                blockType.setSubId(id);
                            }
                            map.put(pasteLoc.clone().add(x, y, z), blockType);
                        }
                    }
                }
                break;
        }
        if (testLoc != null) {
            BlockType blockType = map.get(map.keySet().stream().filter(location -> location.getX() == testLoc.getX() && location.getY() == testLoc.getY() && location.getZ() == testLoc.getZ()).findFirst().orElse(null));
            if (blockType == null){
                System.out.println("blockType = null");
            } else {
                System.out.println("TestLoc subid before paste : " + blockType.getSubId());
            }
        }
        new SchematicPaster().paste(map, ticks);
    }


    public static Schematic createNewSchematic(Location loc1, Location loc2) {
        int maxX = Math.max(loc1.getBlockX(), loc2.getBlockX());
        int minX = Math.min(loc1.getBlockX(), loc2.getBlockX());
        int maxZ = Math.max(loc1.getBlockZ(), loc2.getBlockZ());
        int minZ = Math.min(loc1.getBlockZ(), loc2.getBlockZ());
        int maxY = Math.max(loc1.getBlockY(), loc2.getBlockY());
        int minY = Math.min(loc1.getBlockY(), loc2.getBlockY());
        List<BlockType> types = new ArrayList<>();
        World world = loc1.getWorld();
        for (int x = minX; x <= maxX; x++) {
            for (int z = minZ; z <= maxZ; z++) {
                for (int y = minY; y <= maxY; y++) {
                    Block block = world.getBlockAt(x, y, z);
                    types.add(new BlockType(block.getTypeId(), block.getData()));
                }
            }
        }
        return new Schematic(maxX - minX, maxY - minY, maxZ - minZ, types, null);
    }


}
