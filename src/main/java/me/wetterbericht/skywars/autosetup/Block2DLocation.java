package me.wetterbericht.skywars.autosetup;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class Block2DLocation {

    private int x, z;

    public Block2DLocation plusX() {
        return new Block2DLocation(x + 1, z);
    }

    public Block2DLocation minusX() {
        return new Block2DLocation(x - 1, z);
    }

    public Block2DLocation plusZ() {
        return new Block2DLocation(x, z + 1);
    }

    public Block2DLocation minusZ() {
        return new Block2DLocation(x, z - 1);
    }

    public Block2DLocation getLocationInDirection(Direction direction) {
        switch (direction) {
            case RIGHT:
                return this.plusZ();
            case DOWN:
                return this.minusX();
            case LEFT:
                return this.minusZ();
            case UP:
                return this.plusX();
        }
        return null;
    }

    @Override
    public boolean equals(Object obj) {
        return (obj instanceof Block2DLocation) && ((Block2DLocation) obj).getZ() == this.getZ() && ((Block2DLocation) obj).getX() == this.getX();
    }
}
