package me.wetterbericht.skywars.autosetup;



import lombok.Getter;
import lombok.Setter;
import me.wetterbericht.skywars.utils.RotationUtils;

import java.util.ArrayList;
import java.util.List;

@Getter
public class Island2DLocation {

    private List<Block2DLocation> block2DLocations;

    private List<Block2DLocation> islandLocations = new ArrayList<>();

    private Direction lastDirection = Direction.RIGHT;

    @Setter
    private Direction direction;

    private float yaw;

    @Getter
    private Block2DLocation spawnPoint;

    private Block2DLocation startLocation;

    private Block2DLocation currentLocation;

    private int minX;

    private int maxX;

    private int minZ;

    private int maxZ;

    public Island2DLocation(List<Block2DLocation> block2DLocations, Block2DLocation currentLocation) {
        this(block2DLocations, currentLocation, false);
    }

    public Island2DLocation(List<Block2DLocation> block2DLocations, Block2DLocation currentLocation, boolean useAsSpawn) {
        this.block2DLocations = block2DLocations;
        if (useAsSpawn)
            this.spawnPoint = currentLocation;
        this.calculatePositionsFromIsland(currentLocation.getX(), currentLocation.getZ());
    }


    private void calculatePositionsFromIsland(int x, int z) {
        if (isAir(x, z)) {
            System.out.println("X:" + x + " Z: " + z + " is air");
            return;
        }
        while (!isAir(x, z))
            x++;
        x--;
        this.startLocation = new Block2DLocation(x, z);
        this.currentLocation = new Block2DLocation(x, z);
        searchNext();
        this.minX = islandLocations.stream().reduce((a, b) -> a.getX() < b.getX() ? a : b).get().getX();
        this.maxX = islandLocations.stream().reduce((a, b) -> a.getX() > b.getX() ? a : b).get().getX();
        this.minZ = islandLocations.stream().reduce((a, b) -> a.getZ() < b.getZ() ? a : b).get().getZ();
        this.maxZ = islandLocations.stream().reduce((a, b) -> a.getZ() > b.getZ() ? a : b).get().getZ();
    }

    private void searchNext() {
        islandLocations.add(currentLocation);
        Direction direction = lastDirection.prevDirection();
        while (isAir(direction)) {
            direction = direction.nextDirection();
        }
        currentLocation = currentLocation.getLocationInDirection(direction);
        if (currentLocation.equals(startLocation))
            return;
        lastDirection = direction;
        searchNext();
    }


    private boolean isAir(Direction direction) {
        return isAir(currentLocation.getLocationInDirection(direction));
    }

    private boolean isAir(int x, int z) {
        return !block2DLocations.stream().anyMatch(block2DLocation -> block2DLocation.getX() == x && block2DLocation.getZ() == z);
    }

    private boolean isAir(Block2DLocation block2DLocation2) {
        return !block2DLocations.stream().anyMatch(block2DLocation -> block2DLocation.equals(block2DLocation2));
    }

    public boolean containsLocation(Block2DLocation block2DLocation) {
        return block2DLocation.getX() >= minX && block2DLocation.getX() <= maxX && block2DLocation.getZ() >= minZ && block2DLocation.getZ() <= maxZ;
    }

    public List<Integer> getBlocksInDirection(Direction direction) {
        List<Integer> list = new ArrayList<>();
        switch (direction) {
            case UP:
                for (int x = minX; x <= maxX; x++) {
                    for (int z = minZ; z <= maxZ; z++) {
                        list.add(isAir(x, z) ? 0 : 1);
                    }
                    list.add(2);
                }
                break;
            case LEFT:
                for (int z = maxZ; z >= minZ; z--) {
                    for (int x = minX; x <= maxX; x++) {
                        list.add(isAir(x, z) ? 0 : 1);
                    }
                    list.add(2);
                }
                break;
            case DOWN:
                for (int x = maxX; x >= minX; x--) {
                    for (int z = maxZ; z >= minZ; z--) {
                        list.add(isAir(x, z) ? 0 : 1);
                    }
                    list.add(2);
                }
                break;
            case RIGHT:
                for (int z = minZ; z <= maxZ; z++) {
                    for (int x = maxX; x >= minX; x--) {
                        list.add(isAir(x, z) ? 0 : 1);
                    }
                    list.add(2);
                }
                break;
        }

        return list;
    }

    public void calculateSpawn(Island2DLocation island2DLocation, float yaw) {
        int rotations = (this.direction.getNumber() + 1) % 4;
        this.yaw = RotationUtils.getNewAngle(yaw + (90 * rotations));
        int difX = island2DLocation.getSpawnPoint().getX() - island2DLocation.minX;
        int difZ = island2DLocation.getSpawnPoint().getZ() - island2DLocation.minZ;
        switch (this.direction) {
            case UP:
                this.spawnPoint = new Block2DLocation(minX + difX, minZ + difZ);
                break;
            case LEFT:
                this.spawnPoint = new Block2DLocation(minX + difZ, maxZ - difX);
                break;
            case DOWN:
                this.spawnPoint = new Block2DLocation(maxX - difX, maxZ - difZ);
                break;
            case RIGHT:
                this.spawnPoint = new Block2DLocation(maxX - difZ, minZ + difX);
                break;
        }
    }

}
