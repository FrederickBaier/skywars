package me.wetterbericht.skywars.autosetup;

import lombok.Getter;

public enum Direction {

    RIGHT(0), DOWN(1), LEFT(2), UP(3);

    @Getter
    int number;

    private Direction(int number) {
        this.number = number;
    }

    public Direction nextDirection() {
        return Direction.getDirectionByNumber((number + 1) % 4);
    }

    public Direction prevDirection() {
        return Direction.getDirectionByNumber((number - 1) < 0 ? 3 : number - 1);
    }

    public static Direction getDirectionByNumber(int number) {
        for (Direction direction : Direction.values()) {
            if (direction.getNumber() == number)
                return direction;
        }
        return null;
    }

}

