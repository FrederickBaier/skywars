package me.wetterbericht.skywars.autosetup;

import lombok.Getter;
import me.wetterbericht.skywars.Skywars;
import org.bukkit.*;

import java.util.ArrayList;
import java.util.List;

/**
 * generates a 2D View for all currently loaded chunks
 */
public class World2D {

    private final World world;

    @Getter
    private List<Block2DLocation> notAirBlocks = new ArrayList<>();

    @Getter
    private List<Island2DLocation> islandLocations = new ArrayList<>();

    @Getter
    private int minX;

    @Getter
    private int maxX;

    @Getter
    private int minZ;

    @Getter
    private int maxZ;

    @Getter
    private Island2DLocation middleIsland;

    private Location location;

    public World2D(Location location) {
        this.location = location;
        this.world = location.getWorld();
    }

    public boolean startDetection(){
        System.out.println("Count loaded chunks: " + world.getLoadedChunks().length);
        Chunk firstChunk = this.world.getLoadedChunks()[0];
        int minX = firstChunk.getX() * 16;
        int minZ = firstChunk.getZ() * 16;
        int maxX = (firstChunk.getX() * 16) + 15;
        int maxZ = (firstChunk.getZ() * 16) + 15;
        for (Chunk chunk : world.getLoadedChunks()) {
            minX = Math.min(minX, chunk.getX() * 16);
            maxX = Math.max(maxX, (chunk.getX() * 16) + 15);
            minZ = Math.min(minZ, chunk.getZ() * 16);
            maxZ = Math.max(maxZ, (chunk.getZ() * 16) + 15);
        }
        System.out.println("minX: " + minX + " maxX: " + maxX + " minZ: " + minZ + " maxZ: " + maxZ);
        return startDetection(minX, maxX, minZ, maxZ);
    }

    public boolean startDetection(int minX, int maxX, int minZ, int maxZ){
        float yaw = location.getYaw();
        for (int x = minX; x <= maxX; x++) {
            for (int z = minZ; z <= maxZ; z++) {
                if (!isAirInYDirection(x, 80, z)) {
                    notAirBlocks.add(new Block2DLocation(x, z));
                }
            }
        }
        System.out.println("all not air blocks saved");
        //save all island
        Island2DLocation firstIsland = getPositionsFromIsland(location.getBlockX(), location.getBlockZ(), true);
        for (Block2DLocation block2DLocation : notAirBlocks){
            if (!containsLocationInAnyIsland(block2DLocation)){
                getPositionsFromIsland(block2DLocation.getX(), block2DLocation.getZ(), false);
            }
        }
        System.out.println("checking size");
        //check size
        for (Island2DLocation island2DLocation : this.islandLocations){
            if ((island2DLocation.getMaxX() - island2DLocation.getMinX()) * (island2DLocation.getMaxZ() - island2DLocation.getMinZ()) > 90 * 90){
                return false;
            }
        }
        System.out.println("first island detected: " + firstIsland.toString());
        for (Island2DLocation island2DLocation : this.islandLocations){
            Direction direction = Direction.UP;
            boolean equal = true;
            System.out.println("checking island at " + island2DLocation.getMaxX() + ":" + island2DLocation.getMaxZ());
            while(!firstIsland.getBlocksInDirection(Direction.UP).equals(island2DLocation.getBlocksInDirection(direction))){
                direction = direction.nextDirection();
                if (direction == Direction.UP){
                    equal = false;
                    break;
                }
            }
            System.out.println("direction " + direction.toString());
            if (!equal){
                System.out.println("middle found");
                this.middleIsland = island2DLocation;
            } else {
                island2DLocation.setDirection(direction);
                island2DLocation.calculateSpawn(firstIsland, yaw);
                System.out.println("calculated spawn");
            }
        }
        System.out.println("spawns and directions");
        //--max und min
        if (islandLocations.size() > 0){
            Island2DLocation island2DLocation = islandLocations.get(0);
            this.minX = island2DLocation.getMinX();
            this.minZ = island2DLocation.getMinZ();
            this.maxX = island2DLocation.getMaxX();
            this.maxZ = island2DLocation.getMaxZ();
        }
        for (Island2DLocation island2DLocation : islandLocations){
            this.minX = Math.min(this.minX, island2DLocation.getMinX());
            this.minZ = Math.min(this.minZ, island2DLocation.getMinZ());
            this.maxX = Math.max(this.maxX, island2DLocation.getMaxX());
            this.maxZ = Math.max(this.maxZ, island2DLocation.getMaxZ());
        }
        return true;
    }



    private boolean containsLocationInAnyIsland(Block2DLocation block2DLocation){
        return islandLocations.stream().anyMatch(island2DLocation -> island2DLocation.containsLocation(block2DLocation));
    }

    public void markNotAirBlocks(){
        for (Block2DLocation block2DLocation : notAirBlocks){
            world.getBlockAt(block2DLocation.getX(), 100, block2DLocation.getZ()).setType(Material.QUARTZ_BLOCK);
        }
        for (int i = 0; i < islandLocations.size(); i++){
            Island2DLocation island2DLocation = islandLocations.get(i);
            if (island2DLocation.getSpawnPoint() == null){
                continue;
            }
            world.getBlockAt(island2DLocation.getSpawnPoint().getX(), 101, island2DLocation.getSpawnPoint().getZ()).setType(Material.REDSTONE_BLOCK);
        }
    }

    private Island2DLocation getPositionsFromIsland(int x, int z, boolean useAsSpawn){
        Island2DLocation island2DLocation = new Island2DLocation(notAirBlocks, new Block2DLocation(x, z), useAsSpawn);
        islandLocations.add(island2DLocation);
        return island2DLocation;
    }




    private boolean isAirInYDirection(int x, int y, int z) {
        int maxY = y + 100;
        for (int itY = 0; itY <= maxY; itY++) {
            if (!isAirBlock(x, itY, z))
                return false;
        }
        return true;
    }

    private boolean isAirBlock(int x, int y, int z) {
        return this.world.getBlockAt(x, y, z).getType() == Material.AIR;
    }

}
