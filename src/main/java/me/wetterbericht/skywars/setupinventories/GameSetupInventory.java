package me.wetterbericht.skywars.setupinventories;

import me.wetterbericht.inventoryapi.api.InventoryBuilder;
import me.wetterbericht.inventoryapi.api.InventoryItem;
import me.wetterbericht.inventoryapi.api.anvilgui.AnvilTextGUI;
import me.wetterbericht.inventoryapi.utils.ItemCreator;
import me.wetterbericht.skywars.Skywars;

import me.wetterbericht.skywars.setupgame.SetupChestPosition;
import me.wetterbericht.skywars.setupgame.SetupGame;
import me.wetterbericht.skywars.setupgame.SetupTeamSpawn;
import me.wetterbericht.skywars.items.ChestType;
import me.wetterbericht.skywars.utils.SerializableLocation;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

public class GameSetupInventory {

    private static Inventory getErrorInv(String message) {
        InventoryBuilder inventoryBuilder = new InventoryBuilder(Skywars.PREFIX + "Error", 9);
        inventoryBuilder.fillAll(ItemCreator.createWool(message, DyeColor.RED, 1));
        inventoryBuilder.buildInv();
        return inventoryBuilder.getInventory();
    }


    //PlayersPerTeam - 0
    //AddNormalChest - 2
    //AddMiddleChest - 4
    //SpectatorSpawn - 6
    //AddTeamSpawn - 8

    public static void openGameSetupInventory(Player player, String name) {
        if (!Skywars.getInstance().getSetupGameLoader().gameExists(name)) {
            player.sendMessage(Skywars.PREFIX + "§7Dieses Game existiert §cnicht§7. Nutze §8: §e/creategame <name>");
            return;
        }
        SetupGame setupGame = Skywars.getInstance().getSetupGameLoader().loadGame(name);
        InventoryBuilder inventoryBuilder = new InventoryBuilder(Skywars.PREFIX + "Game §8> §e" + name, 9 * 3);
        inventoryBuilder.fillAll(ItemCreator.createGlass("", DyeColor.BLACK));
        //Items
        //PlayersPerTeam
        inventoryBuilder.setInventoryItem(1, new InventoryItem(ItemCreator.create(Material.EMERALD, "§eAnzahl der Spieler pro Team §7setzen.", setupGame.getPlayersPerTeam())).addListener((p, clickAction) -> showSetPlayersPerTeamAnvil(p, setupGame)));
       //SetSpectatorSpawn
        inventoryBuilder.setInventoryItem(4, new InventoryItem(ItemCreator.create(Material.ENDER_PEARL, "§eSpectator-Spawn §7setzen.")).addListener((p, clickAction) -> {
            setupGame.setSpectatorSpawn(new SerializableLocation(p.getLocation()));
            Skywars.getInstance().getSetupGameLoader().saveGame(setupGame);
            p.sendMessage(Skywars.PREFIX + "§eSpectator-Spawn §7gesetzt.");
            p.playSound(p.getLocation(), Sound.LEVEL_UP, 1F, 1F);
        }));
        //AddTeamSpawn
        inventoryBuilder.setInventoryItem(7, new InventoryItem(ItemCreator.create(Material.BED, "§eTeam-Spawn §7hinzufügen.")).addListener((p, clickAction) -> {
            setupGame.addTeam(new SetupTeamSpawn(setupGame.getTeams().size(), new SerializableLocation(p.getLocation())));
            Skywars.getInstance().getSetupGameLoader().saveGame(setupGame);
            p.sendMessage(Skywars.PREFIX + "§eTeam-Spawn §7hinzugefügt.");
            p.playSound(p.getLocation(), Sound.LEVEL_UP, 1F, 1F);
            Skywars.getInstance().getParticleManager().addEffect(player.getLocation().add(0, 0.5, 0), Effect.ENDER_SIGNAL);
        }));
        //pos1
        inventoryBuilder.setInventoryItem(10, new InventoryItem(ItemCreator.create(Material.NAME_TAG, "§ePosition 1 §7setzen.")).addListener((p, clickAction) -> setPosition(p, 1, p.getLocation(), setupGame)));
        //pos2
        inventoryBuilder.setInventoryItem(16, new InventoryItem(ItemCreator.create(Material.NAME_TAG, "§ePosition 2 §7setzen.")).addListener((p, clickAction) -> setPosition(p, 2, p.getLocation(), setupGame)));
        //Add Chests
        inventoryBuilder.setInventoryItem(13, new InventoryItem(ItemCreator.create(Material.CHEST, "§eKisten hinzufügen §7setzen.")).addListener((p, clickAction) -> addChests(p, setupGame)));
        //
        inventoryBuilder.setInventoryItem(22, new InventoryItem(ItemCreator.create(Material.BARRIER, "§eTeam-Spawns §7zurücksetzen.")).addListener((p, clickAction) -> {
            setupGame.clearTeams();
            Skywars.getInstance().getSetupGameLoader().saveGame(setupGame);
            p.sendMessage(Skywars.PREFIX + "§eTeam-Spawns §7zurückgesetzt.");
            p.playSound(p.getLocation(), Sound.LEVEL_UP, 1F, 1F);
        }));
        //build Inv
        inventoryBuilder.buildInv();
        player.openInventory(inventoryBuilder.getInventory());
    }

    private static void addChests(Player p, SetupGame setupGame) {
        if (setupGame.getPos1() == null || setupGame.getPos2() == null) {
            p.sendMessage(Skywars.PREFIX + "Du musst zuerst die Positionen 1 und 2 setzen.");
            return;
        }
        Location loc1 = setupGame.getPos1().toBukkitLocation();
        Location loc2 = setupGame.getPos2().toBukkitLocation();
        if (!loc1.getWorld().equals(loc2.getWorld())){
            p.sendMessage(Skywars.PREFIX + "Die Positionen müssen in der gleichen Welt sein.");
            return;
        }
        int minX = Math.min(loc1.getBlockX(), loc2.getBlockX());
        int maxX = Math.max(loc1.getBlockX(), loc2.getBlockX());
        int minZ = Math.min(loc1.getBlockZ(), loc2.getBlockZ());
        int maxZ = Math.max(loc1.getBlockZ(), loc2.getBlockZ());
        int minY = Math.min(loc1.getBlockY(), loc2.getBlockY());
        int maxY = Math.max(loc1.getBlockY(), loc2.getBlockY());
        setupGame.clearChests();
        int count = 0;
        for (int y = minY; y <= maxY; y++){
            for (int z = minZ; z <= maxZ; z++){
                for (int x = minX; x <= maxX; x++){
                    Block block = loc1.getWorld().getBlockAt(x,y,z);
                    if (block.getType() == Material.CHEST){
                        setupGame.addChest(new SetupChestPosition(ChestType.NORMAL, new SerializableLocation(block.getLocation())));
                        count++;
                    }
                    if (block.getType() == Material.TRAPPED_CHEST){
                        byte data = block.getData();
                        block.setType(Material.CHEST);
                        block.setData(data);
                        setupGame.addChest(new SetupChestPosition(ChestType.BETTER, new SerializableLocation(block.getLocation())));
                        count++;
                    }
                }
            }
        }
        Skywars.getInstance().getSetupGameLoader().saveGame(setupGame);
        p.sendMessage(Skywars.PREFIX + "Es wurden §e" + count + " §7Kisten hinzugefügt.");
    }

    private static void setPosition(Player player, int number, Location location, SetupGame setupGame) {
        if (number == 1)
            setupGame.setPos1(new SerializableLocation(location));
        if (number == 2)
            setupGame.setPos2(new SerializableLocation(location));
        player.sendMessage(Skywars.PREFIX + "Position §e" + number + " §7gesetzt.");
        player.playSound(player.getLocation(), Sound.LEVEL_UP, 1F, 1F);
        Skywars.getInstance().getSetupGameLoader().saveGame(setupGame);
    }

    private static void addChest(ChestType chestType, Player player, SetupGame setupGame) {
        setupGame.addChest(new SetupChestPosition(chestType, new SerializableLocation(player.getLocation())));
        player.sendMessage(Skywars.PREFIX + "ChestPosition hinzugefügt.");
        Skywars.getInstance().getSetupGameLoader().saveGame(setupGame);
        player.playSound(player.getLocation(), Sound.LEVEL_UP, 1F, 1F);
    }

    private static void showSetPlayersPerTeamAnvil(Player p, SetupGame setupGame) {
        new AnvilTextGUI(p, s -> {
            try {
                setupGame.setPlayersPerTeam(Integer.parseInt(s));
                Skywars.getInstance().getSetupGameLoader().saveGame(setupGame);
                p.sendMessage(Skywars.PREFIX + "Anzahl der Spieler pro Team gesetzt.");
                openGameSetupInventory(p, setupGame.getName());
                p.playSound(p.getLocation(), Sound.LEVEL_UP, 1F, 1F);
                return true;
            } catch (NumberFormatException e) {
                p.sendMessage(Skywars.PREFIX + "Bitte schreibe eine Zahl.");
                return false;
            }
        });
    }

}
