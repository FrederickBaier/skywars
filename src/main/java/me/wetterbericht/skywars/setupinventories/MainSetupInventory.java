package me.wetterbericht.skywars.setupinventories;

import me.wetterbericht.skywars.Skywars;
import me.wetterbericht.skywars.config.ConfigLoader;
import me.wetterbericht.skywars.config.SetupConfigData;
import me.wetterbericht.skywars.utils.SerializableLocation;
import me.wetterbericht.inventoryapi.api.anvilgui.AnvilTextGUI;
import me.wetterbericht.inventoryapi.api.InventoryBuilder;
import me.wetterbericht.inventoryapi.api.InventoryItem;
import me.wetterbericht.inventoryapi.utils.ItemCreator;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

public class MainSetupInventory {


    public static Inventory getMainSetupInventory() {
        InventoryBuilder inventoryBuilder = new InventoryBuilder(Skywars.PREFIX + "Main-Setup", 9);
        inventoryBuilder.fillAll(ItemCreator.createGlass("", DyeColor.BLACK));
        SetupConfigData setupConfigData = getConfigData();
        inventoryBuilder.setInventoryItem(0, new InventoryItem(ItemCreator.create(Material.ENDER_PEARL, "§eLobby-Location §7setzen")).addListener((player, clickType) -> {
            setupConfigData.setLobbyLocation(new SerializableLocation(player.getLocation()));
            saveConfigData(setupConfigData);
            player.sendMessage(Skywars.PREFIX + "§eLobby-Location §7gesetzt.");

        }));
        inventoryBuilder.setInventoryItem(4, new InventoryItem(ItemCreator.create(Material.DIAMOND, "§eAnzahl der Teams §7setzen", setupConfigData.getTeams())).addListener((player, clickType) -> showSetTeamsAnvil(player)));
        inventoryBuilder.setInventoryItem(8, new InventoryItem(ItemCreator.create(Material.EMERALD, "§eAnzahl der Spieler pro Team §7setzen", setupConfigData.getPlayersPerTeam())).addListener((player, clickType) -> showSetPlayersPerTeamAnvil(player)));
        inventoryBuilder.buildInv();
        return inventoryBuilder.getInventory();
    }



    /*
    private static void showSetLanguageAnvil(Player p) {
        new AnvilTextGUI(p, s -> {
            if (new LanguageManager().languageExists(s)) {
                SetupConfigData setupConfigData = getConfigData();
                setupConfigData.setLanguage(s);
                saveConfigData(setupConfigData);
                p.sendMessage(Skywars.PREFIX + "Sprache gesetzt.");
                return true;
            } else {
                p.sendMessage(Skywars.PREFIX + "Diese Sprache existiert nicht.");
                return false;
            }

        });
    }
    */

    private static void showSetPlayersPerTeamAnvil(Player p) {
        new AnvilTextGUI(p, s -> {
            try {
                SetupConfigData setupConfigData = getConfigData();
                setupConfigData.setPlayersPerTeam(Integer.parseInt(s));
                saveConfigData(setupConfigData);
                p.sendMessage(Skywars.PREFIX + "Anzahl der Spieler pro Team gesetzt.");
                p.openInventory(getMainSetupInventory());
                return true;
            } catch (NumberFormatException e) {
                p.sendMessage(Skywars.PREFIX + "Bitte schreibe eine Zahl.");
                return false;
            }
        });
    }

    private static void showSetTeamsAnvil(Player p) {
        new AnvilTextGUI(p, s -> {
            try {
                SetupConfigData setupConfigData = getConfigData();
                setupConfigData.setTeams(Integer.parseInt(s));
                saveConfigData(setupConfigData);
                p.sendMessage(Skywars.PREFIX + "Anzahl der Teams gesetzt.");
                p.openInventory(getMainSetupInventory());
                return true;
            } catch (NumberFormatException e) {
                p.sendMessage(Skywars.PREFIX + "Bitte schreibe eine Zahl.");
                return false;
            }
        });
    }

    private static SetupConfigData getConfigData() {
        SetupConfigData setupConfigData = new ConfigLoader().loadConfig();
        if (setupConfigData == null)
            return new SetupConfigData();
        return setupConfigData;
    }

    private static void saveConfigData(SetupConfigData setupConfigData) {
        new ConfigLoader().saveConfig(setupConfigData);
    }

}
