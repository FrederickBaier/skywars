package me.wetterbericht.skywars.kits;


import com.google.common.collect.Lists;
import lombok.Getter;
import me.wetterbericht.inventoryapi.api.InventoryBuilder;
import me.wetterbericht.inventoryapi.api.InventoryItem;
import me.wetterbericht.inventoryapi.utils.ItemBuilder;
import me.wetterbericht.inventoryapi.utils.ItemCreator;
import me.wetterbericht.skywars.BukkitPluginMain;
import me.wetterbericht.skywars.Skywars;
import me.wetterbericht.skywars.database.Stats;
import me.wetterbericht.skywars.external.adapter.SkywarsCoinsAdapter;
import me.wetterbericht.skywars.scoreboard.ScoreboardManager;
import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import java.util.ArrayList;
import java.util.HashMap;

public class KitManager {

    private SkywarsCoinsAdapter skywarsCoinsAdapter = Skywars.getInstance().getSkywarsModuleManager().getSkywarsCoinsAdapter();

    @Getter
    private KitsContainer kitsContainer = new KitsContainer();

    private HashMap<Player, Kit> playerKits = new HashMap<>();

    public KitManager() {

        kitsContainer.addKit(new Kit("Start", Material.STONE_PICKAXE, 0).addArmor(null).addArmor(null).addArmor(null)
                .addArmor(new ItemBuilder().setMaterial(Material.DIAMOND_BOOTS)).addItem(new ItemBuilder().setMaterial(Material.STONE_PICKAXE)).addLore("§8» §71x Steinspitzhacke"));
        kitsContainer.addKit(new Kit("Kämpfer", Material.IRON_SWORD, 200).addArmor(null).addArmor(null).addArmor(null)
                .addArmor(new ItemBuilder().setMaterial(Material.DIAMOND_BOOTS)).addItem(new ItemBuilder().setMaterial(Material.IRON_SWORD)).addLore("§8» §71x Eisenschwert")
                .addLore("§8» §71x Diamantstiefel"));
        loadKitsContainerFile();
    }

    public void loadKitsContainerFile() {
        KitsContainer loadedKitsContainer = new KitsFileHandler().loadKits();
        if (loadedKitsContainer == null) {
            new KitsFileHandler().saveToFile(kitsContainer);
        } else {
            kitsContainer = loadedKitsContainer;
        }
    }

    public ArrayList<Kit> getPlayerKits(Player player) {
        ArrayList<Kit> playerkits = Lists.newArrayList();
        Stats stats = Skywars.getInstance().getSkywarsModuleManager().getSkywarsStatsAdapter().getStats(player.getUniqueId());
        for (int i : stats.getPlayerKits()) {
            playerkits.add(kitsContainer.getKits().get(i));
        }
        return playerkits;
    }

    public void saveKits(Player player, ArrayList<Kit> playerkits) {
        ArrayList<Integer> intKits = Lists.newArrayList();
        for (Kit kit : playerkits) {
            intKits.add(kitsContainer.getKits().indexOf(kit));
        }
        Bukkit.getScheduler().runTaskAsynchronously(BukkitPluginMain.getInstance(), () -> {
            Stats stats = Skywars.getInstance().getSkywarsModuleManager().getSkywarsStatsAdapter().getStats(player.getUniqueId());
            stats.setPlayerKits(intKits);
            Skywars.getInstance().getSkywarsModuleManager().getSkywarsStatsAdapter().saveStats(stats);
        });

    }

    public Inventory getInventory(Player player) {
        ArrayList<Kit> playerKits = getPlayerKits(player);
        InventoryBuilder inventoryBuilder = new InventoryBuilder(Skywars.PREFIX + "§eKits §8(§6" + Skywars.getInstance().getSkywarsModuleManager().getSkywarsCoinsAdapter().getCoins(player) + " " + skywarsCoinsAdapter.getCoinsName() + "§8)"
                , 54);


        for (Kit kit : kitsContainer.getKits()) {
            if (playerKits.contains(kit)) {
                addBoughtKit(inventoryBuilder, kit);
            } else {
                addNotBoughtKit(inventoryBuilder, kit);
            }
        }
        inventoryBuilder.buildInv();
        return inventoryBuilder.getInventory();
    }

    private void addBoughtKit(InventoryBuilder inventoryBuilder, Kit kit) {
        inventoryBuilder.addInventoryItem(new InventoryItem(kit.getIcon().setName("§a" + kit.getName()).buildItemStack()).addListener((p, clickAction) -> selectKit(p, kit)));
    }

    private void addNotBoughtKit(InventoryBuilder inventoryBuilder, Kit kit) {
        inventoryBuilder.addInventoryItem(new InventoryItem(kit.getIcon().setName("§c" + kit.getName()).buildItemStack()).addListener((p, clickAction) -> p.openInventory(getBuyInventory(kit))));
    }

    public Inventory getBuyInventory(Kit kit) {
        InventoryBuilder inventoryBuilder = new InventoryBuilder("§e" + kit.getName() + " §8kaufen (§6" + kit.getPrice() + "§8)", 9);
        inventoryBuilder.fillAll(ItemCreator.createGlass(" ", DyeColor.BLACK));
        inventoryBuilder.setInventoryItem(0, new InventoryItem(ItemCreator.createWool("§cNein", DyeColor.RED, 1)).addListener((p, clickAction) -> p.closeInventory()));
        inventoryBuilder.setInventoryItem(8, new InventoryItem(ItemCreator.createWool("§aJa §8(§6" + kit.getPrice() + " " + skywarsCoinsAdapter.getCoinsName() +  "§8)", DyeColor.GREEN, 1)).addListener((p, clickAction) -> {
            if (skywarsCoinsAdapter.getCoins(p) < kit.getPrice()){
                p.sendMessage(Skywars.PREFIX + "Du hast nicht genügend " + skywarsCoinsAdapter.getCoinsName());
                p.closeInventory();
                return;
            }
            Skywars.getInstance().getSkywarsModuleManager().getSkywarsCoinsAdapter().removeCoins(p, kit.getPrice());
            ArrayList<Kit> playerKits = getPlayerKits(p);
            playerKits.add(kit);
            saveKits(p, playerKits);
            p.sendMessage(Skywars.PREFIX + "Du hast das Kit §e" + kit.getName() + " §7gekauft.");
            selectKit(p, kit);
        }));
        inventoryBuilder.buildInv();
        return inventoryBuilder.getInventory();
    }

    public void selectKit(Player player, Kit kit) {
        player.sendMessage(Skywars.PREFIX + "Du hast das Kit §e" + kit.getName() + " §7ausgewählt.");
        player.closeInventory();
        playerKits.put(player, kit);
        ScoreboardManager.updateScoreboard(player);
        Bukkit.getScheduler().runTaskLater(BukkitPluginMain.getInstance(), () -> Bukkit.getScheduler().runTaskAsynchronously(BukkitPluginMain.getInstance(), () -> {
            Stats stats = Skywars.getInstance().getSkywarsModuleManager().getSkywarsStatsAdapter().getStats(player.getUniqueId());
            if (stats != null) {
                stats.setKit(Skywars.getInstance().getKitManager().getKitsContainer().getKits().indexOf(Skywars.getInstance().getKitManager().getSelectedKit(player)));
                Skywars.getInstance().getSkywarsModuleManager().getSkywarsStatsAdapter().saveStats(stats);
            }
        }), 5);

    }

    public void selectDefaultKit(Player player) {
        player.sendMessage(Skywars.PREFIX + "Du hast das Kit §e" + kitsContainer.getKits().get(0).getName() + " §7ausgewählt.");
        player.closeInventory();
        playerKits.put(player, kitsContainer.getKits().get(0));
        ScoreboardManager.updateScoreboard(player);
    }

    public Kit getSelectedKit(Player player) {
        Kit kit = playerKits.get(player);
        if (kit == null)
            return kitsContainer.getKits().get(0);
        return kit;
    }

    public boolean hasSelectedKit(Player player) {
        return playerKits.containsKey(player);
    }


}
