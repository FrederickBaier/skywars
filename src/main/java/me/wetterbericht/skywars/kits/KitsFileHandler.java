package me.wetterbericht.skywars.kits;

import me.wetterbericht.json.JsonData;
import me.wetterbericht.skywars.Skywars;

import java.io.File;

public class KitsFileHandler {

    private File file = new File(Skywars.PLUGIN_MAIN_DIR + "kits.json");

    public void saveToFile(KitsContainer kitsFile){
        new JsonData().append("data", kitsFile).saveAsFile(file);
    }

    public KitsContainer loadKits(){
        if (!file.exists())
            return null;
        return JsonData.fromJsonFile(file).getObject("data", KitsContainer.class);
    }


}
