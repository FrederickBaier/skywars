package me.wetterbericht.skywars.kits;

import com.google.common.collect.Lists;
import lombok.Getter;

import java.util.ArrayList;

@Getter
public class KitsContainer {

    private ArrayList<Kit> kits = Lists.newArrayList();

    public void addKit(Kit kit){
        kits.add(kit);
    }

    public void removeKit(Kit kit){
        kits.remove(kit);
    }

    public Kit getKitByName(String name){
        return kits.stream().filter(kit -> kit.getName().equalsIgnoreCase(name)).findFirst().orElse(null);
    }

}
