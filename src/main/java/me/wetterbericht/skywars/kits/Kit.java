package me.wetterbericht.skywars.kits;

import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.Setter;
import me.wetterbericht.inventoryapi.utils.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;


public class Kit {

    @Getter
    private String name;
    @Getter
    private ArrayList<ItemBuilder> armor = Lists.newArrayList();
    @Getter
    private ArrayList<ItemBuilder> inventoryItems = Lists.newArrayList();

    private ArrayList<String> description = Lists.newArrayList();

    @Getter
    private int price;

    @Getter
    @Setter
    private Material iconMaterial;


    public Kit(String name, int price) {
        this.name = name;
        this.price = price;
    }

    public Kit(String name, Material iconMaterial, int price) {
        this.name = name;
        this.iconMaterial = iconMaterial;
        this.price = price;
    }

    public ArrayList<String> getDescription() {
        ArrayList<String> list = Lists.newArrayList();
        list.add("§6Ausrüstung");
        list.addAll(description);
        return list;
    }

    public ItemBuilder getIcon() {
        return new ItemBuilder().setMaterial(iconMaterial).setName("§e" + name).setLore(getDescription());
    }

    public Kit addArmor(ItemBuilder item) {
        armor.add(item);
        return this;
    }

    public Kit addItem(ItemBuilder item) {
        inventoryItems.add(item);
        return this;
    }

    public Kit addLore(String lore) {
        description.add(lore);
        return this;
    }

    public ItemStack[] getArmorContents() {
        ItemStack[] items = new ItemStack[4];
        for (int i = 0; i < 4; i++) {
            if (armor.get(armor.size() - 1 - i) == null)
                continue;
            items[i] = armor.get(armor.size() - 1- i).buildItemStack();
        }
        return items;
    }

}
