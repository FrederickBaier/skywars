package me.wetterbericht.skywars.command.commands;

import me.wetterbericht.inventoryapi.api.InventoryBuilder;
import me.wetterbericht.inventoryapi.api.InventoryItem;
import me.wetterbericht.inventoryapi.api.clicklistener.ClickListener;
import me.wetterbericht.inventoryapi.utils.ItemBuilder;
import me.wetterbericht.inventoryapi.utils.ItemCreator;
import me.wetterbericht.skywars.Skywars;
import me.wetterbericht.skywars.items.ChestItem;
import me.wetterbericht.skywars.items.ChestItemType;
import me.wetterbericht.skywars.items.ChestItemsFileHandler;
import me.wetterbericht.skywars.items.ChestType;
import org.bukkit.DyeColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

public class ChestItemsCommand implements CommandExecutor {
    //chestitems <NORMAL/BETTER> <>
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if (!(commandSender instanceof Player))
            return true;
        Player p = (Player) commandSender;
        if (!p.hasPermission("skywars.setup"))
            return true;
        if (args.length != 2) {
            sendHelp(p);
            return true;
        }
        ChestType chestType;
        try {
            chestType = ChestType.valueOf(args[0].toUpperCase());
        } catch (IllegalArgumentException e) {
            sendHelp(p);
            return true;
        }
        ChestItemType chestItemType;
        try {
            chestItemType = ChestItemType.valueOf(args[1].toUpperCase());
        } catch (IllegalArgumentException e) {
            sendHelp(p);
            return true;
        }
        p.openInventory(getInventory(chestType, chestItemType));
        return true;
    }

    public Inventory getInventory(ChestType chestType, ChestItemType chestItemType) {
        InventoryBuilder inventoryBuilder = new InventoryBuilder(Skywars.PREFIX + "Chest-Items", 54);
        Skywars.getInstance().getChestItems().getAll(chestType, chestItemType).forEach(chestItem -> inventoryBuilder.addInventoryItem(new InventoryItem(chestItem.getItemBuilder().buildItemStack()).addListener((p, clickAction) -> {
            p.openInventory(getDeleteInventory(chestItem, chestType));
        })));
        inventoryBuilder.buildInv();
        return inventoryBuilder.getInventory();
    }

    private Inventory getDeleteInventory(ChestItem chestItem, ChestType chestType) {
        InventoryBuilder inventoryBuilder = new InventoryBuilder(Skywars.PREFIX + "Item §cLöschen§7?", 9);
        inventoryBuilder.fillAll(ItemCreator.createGlass(" ", DyeColor.BLACK));
        inventoryBuilder.setInventoryItem(4, new InventoryItem(chestItem.getItemBuilder().buildItemStack()));
        inventoryBuilder.setInventoryItem(0, new InventoryItem(ItemCreator.createWool("§aJa", DyeColor.GREEN, 1)).addListener((p, clickAction) -> {
            Skywars.getInstance().getChestItems().removeChestItem(chestItem, chestType);
            new ChestItemsFileHandler().saveToFile(Skywars.getInstance().getChestItems());
            p.sendMessage(Skywars.PREFIX + "Item gelöscht.");
            p.closeInventory();
        }));
        inventoryBuilder.setInventoryItem(8, new InventoryItem(ItemCreator.createWool("§cNein", DyeColor.RED, 1)).addListener((p, clickAction) -> p.closeInventory()));
        inventoryBuilder.buildInv();
        return inventoryBuilder.getInventory();
    }

    private void sendHelp(Player p) {
        p.sendMessage(Skywars.PREFIX + "Use§8: §e/chestItems <NORMAL/BETTER> <ARMOR/WEAPON/FOOD/BLOCK/ORE/SPECIAL>");
    }
}
