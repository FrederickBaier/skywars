package me.wetterbericht.skywars.command.commands;


import me.wetterbericht.skywars.setupinventories.MainSetupInventory;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class MainSetupCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player))
            return true;
        Player p = (Player)sender;
        if (!p.hasPermission("skywars.setup"))
            return true;
        p.openInventory(MainSetupInventory.getMainSetupInventory());
        return true;
    }
}
