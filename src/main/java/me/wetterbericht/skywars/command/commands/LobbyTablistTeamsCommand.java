package me.wetterbericht.skywars.command.commands;

import me.wetterbericht.inventoryapi.api.InventoryBuilder;
import me.wetterbericht.inventoryapi.api.InventoryItem;
import me.wetterbericht.inventoryapi.api.clicklistener.ClickListener;
import me.wetterbericht.inventoryapi.utils.ItemBuilder;
import me.wetterbericht.inventoryapi.utils.ItemCreator;
import me.wetterbericht.skywars.Skywars;
import me.wetterbericht.skywars.items.ChestItemsFileHandler;
import me.wetterbericht.skywars.scoreboard.LobbyTabListTeam;
import me.wetterbericht.skywars.scoreboard.LobbyTablistTeamManagerFileHandler;
import me.wetterbericht.skywars.scoreboard.ScoreboardTabList;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.Inventory;

import java.util.Arrays;

public class LobbyTablistTeamsCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if (!(commandSender instanceof Player))
            return true;
        Player p = (Player) commandSender;
        if (!p.hasPermission("skywars.setup"))
            return true;
        p.openInventory(getInventory());
        return true;
    }

    private Inventory getInventory() {
        InventoryBuilder inventoryBuilder = new InventoryBuilder(Skywars.PREFIX + "Lobby Tablist Teams", 54);
        for (LobbyTabListTeam lobbyTabListTeam : ScoreboardTabList.getLobbyTabListTeamManager().getLobbyTabListTeams()) {
            inventoryBuilder.addInventoryItem(new InventoryItem(new ItemBuilder().setMaterial(Material.MAGMA_CREAM).setName("§e" + lobbyTabListTeam.getName())
                    .setLore(Arrays.asList("§8» §ePermission", "§8» §7" + lobbyTabListTeam.getPermission(), "§8» §ePrefix", "§8» §7" + lobbyTabListTeam.getPrefix())).buildItemStack())
                    .addListener((p, clickAction) -> p.openInventory(getDeleteTabListTeamInventory(lobbyTabListTeam))));
        }
        inventoryBuilder.fillAllEmptySlots(ItemCreator.createGlass(" ", DyeColor.BLACK));
        inventoryBuilder.buildInv();
        return inventoryBuilder.getInventory();
    }

    private Inventory getDeleteTabListTeamInventory(LobbyTabListTeam lobbyTabListTeam){
        InventoryBuilder inventoryBuilder = new InventoryBuilder(Skywars.PREFIX + "§e" + lobbyTabListTeam.getName() + " §7löschen?", 9);
        inventoryBuilder.fillAll(ItemCreator.createGlass(" ", DyeColor.BLACK));
        inventoryBuilder.setInventoryItem(0, new InventoryItem(ItemCreator.createWool("§aJa", DyeColor.GREEN, 1)).addListener((p, clickAction) -> {
            ScoreboardTabList.getLobbyTabListTeamManager().removeLobbyTablistTeam(lobbyTabListTeam);
            new LobbyTablistTeamManagerFileHandler().saveToFile(ScoreboardTabList.getLobbyTabListTeamManager());
            p.sendMessage(Skywars.PREFIX + "Lobby-Tablist-Team gelöscht.");
            p.closeInventory();
        }));
        inventoryBuilder.setInventoryItem(8, new InventoryItem(ItemCreator.createWool("§cNein", DyeColor.RED, 1)).addListener((p, clickAction) -> p.closeInventory()));
        inventoryBuilder.buildInv();
        return inventoryBuilder.getInventory();
    }
}
