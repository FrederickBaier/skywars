package me.wetterbericht.skywars.command.commands;

import me.wetterbericht.skywars.Skywars;
import me.wetterbericht.skywars.utils.GameState;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class StartCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (!(commandSender instanceof Player))
            return true;
        Player p = (Player) commandSender;
        if (!p.hasPermission("skywars.start")){
            p.sendMessage(Skywars.PREFIX + "Keine Rechte.");
            return true;
        }
        if (Skywars.getInstance().getGameState() != GameState.LOBBY){
            p.sendMessage(Skywars.PREFIX + "Das Spiel ist bereits gestartet.");
            return true;
        }
        if (Skywars.getInstance().getSkywarsExecutor().forceStart()){
            p.sendMessage(Skywars.PREFIX + "Das Spiel startet nun.");
        } else {
            p.sendMessage(Skywars.PREFIX + "Das Spiel konnte nicht gestartet werden.");
        }
        return true;
    }
}
