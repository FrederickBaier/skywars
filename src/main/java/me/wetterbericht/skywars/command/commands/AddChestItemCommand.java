package me.wetterbericht.skywars.command.commands;


import me.wetterbericht.inventoryapi.utils.ItemBuilder;
import me.wetterbericht.skywars.Skywars;
import me.wetterbericht.skywars.items.ChestItem;
import me.wetterbericht.skywars.items.ChestItemType;
import me.wetterbericht.skywars.items.ChestItemsFileHandler;
import me.wetterbericht.skywars.items.ChestType;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class AddChestItemCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if (!(commandSender instanceof Player))
            return true;
        Player p = (Player) commandSender;
        if (!p.hasPermission("skywars.setup"))
            return true;
        if (args.length != 2) {
            sendHelp(p);
            return true;
        }
        ChestType chestType;
        try {
            chestType = ChestType.valueOf(args[0].toUpperCase());
        } catch (IllegalArgumentException e) {
            sendHelp(p);
            return true;
        }
        ChestItemType chestItemType;
        try {
            chestItemType = ChestItemType.valueOf(args[1].toUpperCase());
        } catch (IllegalArgumentException e) {
            sendHelp(p);
            return true;
        }
        if (p.getItemInHand() == null || p.getItemInHand().getType() == Material.AIR) {
            sendHelp(p);
            return true;
        }
        Skywars.getInstance().getChestItems().addChestItem(new ChestItem(ItemBuilder.formItemStack(p.getItemInHand()), chestItemType), chestType);
        new ChestItemsFileHandler().saveToFile(Skywars.getInstance().getChestItems());
        p.sendMessage(Skywars.PREFIX + "Item gespeichert.");
        return true;
    }

    private void sendHelp(Player p) {
        p.sendMessage(Skywars.PREFIX + "Use§8: §e/addChestItem <NORMAL/BETTER> <ARMOR/WEAPON/FOOD/BLOCK/ORE/SPECIAL>");
        p.sendMessage("§8- §7Halte dabei das Item in der Hand.");
    }
}
