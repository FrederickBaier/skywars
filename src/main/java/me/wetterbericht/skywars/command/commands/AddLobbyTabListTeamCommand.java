package me.wetterbericht.skywars.command.commands;

import me.wetterbericht.inventoryapi.api.InventoryBuilder;
import me.wetterbericht.inventoryapi.api.InventoryItem;
import me.wetterbericht.inventoryapi.api.anvilgui.AnvilTextGUI;
import me.wetterbericht.inventoryapi.api.clicklistener.ClickListener;
import me.wetterbericht.inventoryapi.utils.ItemBuilder;
import me.wetterbericht.inventoryapi.utils.ItemCreator;
import me.wetterbericht.skywars.Skywars;
import me.wetterbericht.skywars.scoreboard.LobbyTabListTeam;
import me.wetterbericht.skywars.scoreboard.LobbyTablistTeamManagerFileHandler;
import me.wetterbericht.skywars.scoreboard.ScoreboardTabList;
import me.wetterbericht.skywars.setupgame.SetupGame;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.Inventory;

public class AddLobbyTabListTeamCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if (!(commandSender instanceof Player))
            return true;
        Player p = (Player) commandSender;
        if (!p.hasPermission("skywars.setup"))
            return true;
        p.openInventory(getInventory(null));
        return true;
    }

    private void sendHelp(Player p) {

    }

    private Inventory getInventory(LobbyTabListTeam tabListTeam) {
        LobbyTabListTeam lobbyTabListTeam;
        if (tabListTeam == null) {
            lobbyTabListTeam = new LobbyTabListTeam();
        } else {
            lobbyTabListTeam = tabListTeam;
        }
        InventoryBuilder inventoryBuilder = new InventoryBuilder(Skywars.PREFIX + "Tablist Team hinzufügen.", 9);
        inventoryBuilder.fillAll(ItemCreator.createGlass(" ", DyeColor.BLACK));
        inventoryBuilder.setInventoryItem(0, new InventoryItem(ItemCreator.create(Material.NAME_TAG, "§eNamen §7setzen.")).addListener((p, clickAction) -> showSetNameAnvil(p, lobbyTabListTeam)));
        inventoryBuilder.setInventoryItem(3, new InventoryItem(ItemCreator.create(Material.NAME_TAG, "§ePermission §7setzen.")).addListener((p, clickAction) -> showSetPermissionAnvil(p, lobbyTabListTeam)));
        inventoryBuilder.setInventoryItem(5, new InventoryItem(ItemCreator.create(Material.NAME_TAG, "§ePrefix §7setzen.")).addListener((p, clickAction) -> showSetPrefixAnvil(p, lobbyTabListTeam)));
        inventoryBuilder.setInventoryItem(8, new InventoryItem(ItemCreator.createWool("§aSpeichern", DyeColor.GREEN, 1)).addListener((p, clickAction) -> p.openInventory(getSaveInventory(lobbyTabListTeam))));
        inventoryBuilder.buildInv();
        return inventoryBuilder.getInventory();
    }

    public Inventory getSaveInventory(LobbyTabListTeam lobbyTabListTeam) {
        InventoryBuilder inventoryBuilder = new InventoryBuilder(Skywars.PREFIX + "Tablist Team speichern.", 9);
        inventoryBuilder.fillAll(ItemCreator.createGlass(" ", DyeColor.BLACK));
        inventoryBuilder.setInventoryItem(0, new InventoryItem(ItemCreator.createWool("§aTeam hinzufügen", DyeColor.GREEN, 1)).addListener((p, clickAction) -> {
            if (lobbyTabListTeam.getName() == null || lobbyTabListTeam.getPrefix() == null || lobbyTabListTeam.getPermission() == null){
                p.sendMessage(Skywars.PREFIX + "Lobby-Tablist-Team konnte nicht gespeichert werden.");
                return;
            }
            ScoreboardTabList.getLobbyTabListTeamManager().addLobbyTablistTeam(lobbyTabListTeam);
            new LobbyTablistTeamManagerFileHandler().saveToFile(ScoreboardTabList.getLobbyTabListTeamManager());
            p.closeInventory();
            p.sendMessage(Skywars.PREFIX + "Lobby-Tablist-Team gespeichert.");
        }));
        inventoryBuilder.setInventoryItem(8, new InventoryItem(ItemCreator.createWool("§6Speichern als * Permission Team", DyeColor.ORANGE, 1)).addListener((p, clickAction) -> {
            if (lobbyTabListTeam.getName() == null || lobbyTabListTeam.getPrefix() == null || lobbyTabListTeam.getPermission() == null){
                p.sendMessage(Skywars.PREFIX + "Lobby-Tablist-Team konnte nicht gespeichert werden.");
                return;
            }
            ScoreboardTabList.getLobbyTabListTeamManager().setAllPermissionTabListTeam(lobbyTabListTeam);
            new LobbyTablistTeamManagerFileHandler().saveToFile(ScoreboardTabList.getLobbyTabListTeamManager());
            p.closeInventory();
            p.sendMessage(Skywars.PREFIX + "Lobby-Tablist-Team gespeichert.");
        }));
        inventoryBuilder.buildInv();
        return inventoryBuilder.getInventory();
    }

    private void showSetNameAnvil(Player p, LobbyTabListTeam lobbyTabListTeam) {
        new AnvilTextGUI(p, s -> {
            lobbyTabListTeam.setName(s);
            p.sendMessage(Skywars.PREFIX + "Name gesetzt.");
            p.playSound(p.getLocation(), Sound.LEVEL_UP, 1F, 1F);
            p.openInventory(getInventory(lobbyTabListTeam));
            return true;

        });
    }

    private void showSetPermissionAnvil(Player p, LobbyTabListTeam lobbyTabListTeam) {
        new AnvilTextGUI(p, s -> {
            lobbyTabListTeam.setPermission(s);
            p.sendMessage(Skywars.PREFIX + "Permission gesetzt.");
            p.playSound(p.getLocation(), Sound.LEVEL_UP, 1F, 1F);
            p.openInventory(getInventory(lobbyTabListTeam));
            return true;

        });
    }

    private void showSetPrefixAnvil(Player p, LobbyTabListTeam lobbyTabListTeam) {
        new AnvilTextGUI(p, s -> {
            lobbyTabListTeam.setPrefix(s.replaceAll("&", "§"));
            p.sendMessage(Skywars.PREFIX + "Prefix gesetzt.");
            p.playSound(p.getLocation(), Sound.LEVEL_UP, 1F, 1F);
            p.openInventory(getInventory(lobbyTabListTeam));
            return true;

        });
    }
}
