package me.wetterbericht.skywars.command.commands;

import me.wetterbericht.json.JsonData;
import me.wetterbericht.skywars.Skywars;
import me.wetterbericht.skywars.setupgame.SetupGame;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.io.File;

public class CreateGameCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player))
            return true;
        Player p = (Player)sender;
        if (!p.hasPermission("skywars.setup"))
            return true;
        if (args.length != 1){
            p.sendMessage(Skywars.PREFIX + "Nutze §8: §e/creategame <name>");
            return true;
        }
        File file = new File(Skywars.GAMES_DIR + args[0] + ".json");
        if (file.exists()){
            p.sendMessage(Skywars.PREFIX + "Game §cexistiert bereits§7.");
            return true;
        }
        new JsonData().append("data", new SetupGame(args[0])).saveAsFile(file);
        p.sendMessage(Skywars.PREFIX + "Game §e" + args[0] + " §7erstellt.");
        return true;
    }
}
