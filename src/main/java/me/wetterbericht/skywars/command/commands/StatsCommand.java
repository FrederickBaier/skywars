package me.wetterbericht.skywars.command.commands;

import me.wetterbericht.skywars.Skywars;
import me.wetterbericht.skywars.database.Stats;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class StatsCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if (!(commandSender instanceof Player))
            return true;
        Player p = (Player) commandSender;
        if (!Skywars.getInstance().getSkywarsModuleManager().getSkywarsStatsAdapter().hasStats(p.getUniqueId())) {
            p.sendMessage(Skywars.PREFIX + "Dieser Spieler hat noch nie Skywars gespielt.");
            return true;
        }
        Stats stats = Skywars.getInstance().getSkywarsModuleManager().getSkywarsStatsAdapter().getStats(p.getUniqueId());
        sendStats(p, new int[]{stats.getKills(), stats.getDeaths(), stats.getPlayedGames()}, p.getName());

        return true;
    }

    public void sendStats(Player p, int[] data, String name) {
        p.sendMessage("§8---- §e" + name + " §8----");
        p.sendMessage("§eKills§8: §7" + data[0]);
        p.sendMessage("§eDeaths§8: §7" + data[1]);
        p.sendMessage("§ePlayedGames§8: §7" + data[2]);
    }

}
