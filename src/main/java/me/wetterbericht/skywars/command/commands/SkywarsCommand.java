package me.wetterbericht.skywars.command.commands;

import me.wetterbericht.skywars.Skywars;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SkywarsCommand implements CommandExecutor {


    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if (!(commandSender instanceof Player))
            return true;
        Player p = (Player) commandSender;
        if (!p.hasPermission("skywars.setup"))
            return true;
        p.sendMessage(Skywars.PREFIX + "Plugin developed by §eWetterbericht");
        p.sendMessage("§eCommands§8:");
        p.sendMessage("§8- §e/setup §8- §7Setzt das Plugin in den Setup-Modus.");
        p.sendMessage("§8- §e/mainsetup §8- §7Zeigt das Inventar für das Mainsetup.");
        p.sendMessage("§8- §e/creategame §8- §7Erstellt ein neues Spiel.");
        p.sendMessage("§8- §e/gamesetup §8- §7Zeigt das Inventar zum Bearbeiten eines Spiels.");
        p.sendMessage("§8- §e/addKit §8- §7fügt ein Kit hinzu.");
        p.sendMessage("§8- §e/delKit §8- §7löscht ein Kit.");
        p.sendMessage("§8- §e/addChestItem §8- §7fügt ein Item den Kisten hinzu.");
        p.sendMessage("§8- §e/chestItems §8- §7zeit alle Chest-Items an.");
        p.sendMessage("§8- §e/addLobbyTablistTeam §8- §7fügt ein neue Scorebaord Team für die Wartezeit hinzu.");
        p.sendMessage("§8- §e/lobbyTablistTeams §8- §7zeit alle Lobby-Tablist-Teams an.");
        p.sendMessage("§8- §e/start §8- §7Setzt den Lobby-Countdown auf 10 Sekunden.");
        p.sendMessage("§8- §e/coins §8- §7Zeigt deine Coins.");
        p.sendMessage("§8- §e/stats §8- §7Zeigt deine Stats.");
        return true;
    }
}
