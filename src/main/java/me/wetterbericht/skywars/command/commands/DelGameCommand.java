package me.wetterbericht.skywars.command.commands;

import me.wetterbericht.skywars.Skywars;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.io.File;

public class DelGameCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (!(sender instanceof Player)){
            return true;
        }
        Player player = (Player)sender;
        if (!player.hasPermission("skywars.setup"))
            return true;
        if (args.length != 1){
            player.sendMessage(Skywars.PREFIX + "Use§8: §e/delGame <Name>");
            return true;
        }
        File file = new File(Skywars.GAMES_DIR + args[0] + ".json");
        if (!file.exists()){
            player.sendMessage(Skywars.PREFIX + "Map existiert nicht.");
            return true;
        }
        file.delete();
        player.sendMessage(Skywars.PREFIX + "Map gelöscht.");
        return true;
    }
}
