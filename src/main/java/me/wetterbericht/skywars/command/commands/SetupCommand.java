package me.wetterbericht.skywars.command.commands;

import me.wetterbericht.skywars.Skywars;
import me.wetterbericht.skywars.utils.GameState;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetupCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if (!(commandSender instanceof Player))
            return true;
        Player p = (Player) commandSender;
        if (!p.hasPermission("skywars.setup"))
            return true;
        if (Skywars.getInstance().getGameState() != GameState.LOBBY){
            p.sendMessage(Skywars.PREFIX + "Das Plugin kann nur in der Lobbyphase in den Setup-Modus gesetzt werden.");
            return true;
        }
        Skywars.SETUP = true;
        Skywars.getInstance().getSkywarsModuleManager().getSkywarsCloudAdapter().changeToIngame();
        //CloudServer.getInstance().changeToIngame();
        for (Player all : Bukkit.getOnlinePlayers()){
            if (!all.hasPermission("skywars.setup"))
                all.kickPlayer(Skywars.PREFIX + "Das Spiel wird jetzt bearbeitet.");
        }
        p.sendMessage(Skywars.PREFIX + "Das Plugin ist nun im Setup-Modus");
        return true;
    }
}
