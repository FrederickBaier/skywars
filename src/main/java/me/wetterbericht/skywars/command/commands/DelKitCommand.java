package me.wetterbericht.skywars.command.commands;

import me.wetterbericht.skywars.Skywars;
import me.wetterbericht.skywars.kits.Kit;
import me.wetterbericht.skywars.kits.KitsFileHandler;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class DelKitCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if (!(commandSender instanceof Player))
            return true;
        Player p = (Player) commandSender;
        if (!p.hasPermission("skywars.setup"))
            return true;
        if (args.length != 1) {
            sendHelp(p);
            return true;
        }
        Kit kit = Skywars.getInstance().getKitManager().getKitsContainer().getKitByName(args[0]);
        if (kit == null){
            p.sendMessage(Skywars.PREFIX + "Dieses Kit existiert nicht.");
            return true;
        }
        Skywars.getInstance().getKitManager().getKitsContainer().removeKit(kit);
        new KitsFileHandler().saveToFile(Skywars.getInstance().getKitManager().getKitsContainer());
        Skywars.getInstance().getKitManager().loadKitsContainerFile();
        p.sendMessage(Skywars.PREFIX + "Kit entfernt.");
        return true;
    }

    private void sendHelp(Player p) {
        p.sendMessage(Skywars.PREFIX + "Use§8: §e/delKit <Name>");
    }
}
