package me.wetterbericht.skywars.command.commands;

import me.wetterbericht.skywars.Skywars;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CoinsCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (!(commandSender instanceof Player))
            return true;
        Player p = (Player)commandSender;
        p.sendMessage(Skywars.PREFIX + "Deine Coins§8: §e" + Skywars.getInstance().getSkywarsModuleManager().getSkywarsCoinsAdapter().getCoins(p));
        return true;
    }
}
