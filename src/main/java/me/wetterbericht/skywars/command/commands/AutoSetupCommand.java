package me.wetterbericht.skywars.command.commands;


import me.wetterbericht.skywars.Skywars;
import me.wetterbericht.skywars.autosetup.Block2DLocation;
import me.wetterbericht.skywars.autosetup.Island2DLocation;
import me.wetterbericht.skywars.autosetup.World2D;
import me.wetterbericht.skywars.items.ChestType;
import me.wetterbericht.skywars.schematic.IslandPosition;
import me.wetterbericht.skywars.schematic.MapSchematicFile;
import me.wetterbericht.skywars.schematic.MapSchematics;
import me.wetterbericht.skywars.schematic.Schematic;
import me.wetterbericht.skywars.setupgame.SetupChestPosition;
import me.wetterbericht.skywars.setupgame.SetupGame;
import me.wetterbericht.skywars.setupgame.SetupTeamSpawn;
import me.wetterbericht.skywars.utils.SerializableLocation;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AutoSetupCommand implements CommandExecutor {

    private Location pos1;

    private Location pos2;

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if (!(commandSender instanceof Player)) {
            return true;
        }
        Player p = (Player) commandSender;
        if (!p.hasPermission("skywars.setup"))
            return true;
        if (args.length == 1) {
            if (args[0].equalsIgnoreCase("pos1")) {
                this.pos1 = p.getLocation();
                p.sendMessage(Skywars.PREFIX + "Pos 1 gesetzt.");
            }
            if (args[0].equalsIgnoreCase("pos2")) {
                this.pos2 = p.getLocation();
                p.sendMessage(Skywars.PREFIX + "Pos 2 gesetzt.");
            }
            if (args[0].equalsIgnoreCase("pos1") || args[0].equalsIgnoreCase("pos2")) {
                if (pos1 != null && pos2 != null && pos1.getWorld().equals(pos2.getWorld())) {
                    p.sendMessage(Skywars.PREFIX + "Stelle dich nun an den Spawn eines Teams und mache /autosetup <name> <players per team> -usePositions");
                }
            }
            return true;
        }
        if (args.length == 3 && args[2].equalsIgnoreCase("-usePositions")){
            permformAutoSetup(args, p, true);
            return true;
        }
        if (args.length != 2) {
            p.sendMessage(Skywars.PREFIX + "/autosetup <name> <players per team>");
            return true;
        }
        permformAutoSetup(args, p, false);
        return true;
    }

    private void permformAutoSetup(String[] args, Player p, boolean usePositions) {
        if (usePositions){
            if (pos1 == null || pos2 == null || !pos2.getWorld().equals(pos1.getWorld())){
                p.sendMessage(Skywars.PREFIX + "Die Positionen müssen beide gestzt und in der gleich Welt sein.");
                return;
            }
        }
        Location location = p.getLocation().clone();
        if (location.add(0, -1, 0).getBlock().getType() == Material.AIR){
            p.sendMessage(Skywars.PREFIX + "§cEs darf keine Luft under dir sein.");
            return;
        }
        String name = args[0];
        String playersPerTeamString = args[1];
        int playersPerTeam = 0;
        try {
            playersPerTeam = Integer.parseInt(playersPerTeamString);
        } catch (NumberFormatException e) {
            p.sendMessage(Skywars.PREFIX + "/autosetup <name> <players per team>");
            return;
        }
        p.sendMessage(Skywars.PREFIX + "§eUmriss wird geladen...");
        SetupGame setupGame = new SetupGame(name);
        setupGame.setPlayersPerTeam(playersPerTeam);
        long ms = System.currentTimeMillis();
        World2D world2D = new World2D(p.getLocation());
        boolean success;
        if (usePositions)
            success = world2D.startDetection(Math.min(pos1.getBlockX(), pos2.getBlockX()), Math.max(pos1.getBlockX(), pos2.getBlockX()), Math.min(pos1.getBlockZ(), pos2.getBlockZ()), Math.max(pos1.getBlockZ(), pos2.getBlockZ()));
        else
            success = world2D.startDetection();
        if (!success) {
            if (usePositions){
                p.sendMessage(Skywars.PREFIX + "§cEs trat ein Fehler auf.");
                p.sendMessage(Skywars.PREFIX + "§cEs scheint so als wäre das automatische Setup bei dieser Map nicht möglich. Bitte benutze das manuelle.");
                return;
            }
            p.sendMessage(Skywars.PREFIX + "§cEs trat ein Fehler auf.");
            p.sendMessage(Skywars.PREFIX + "§cVersuche die Positionen einzeln zu setzen. (/autosetup pos1 und /autosetup pos2)");
            return;
        }
        //save chests
        int chestsOnMiddleIsland = 0;
        int minY = 2000, maxY = -2;
        for (Block2DLocation block2DLocation : world2D.getNotAirBlocks()) {
            int x = block2DLocation.getX();
            int z = block2DLocation.getZ();
            for (int y = 0; y <= 200; y++) {
                if (p.getWorld().getBlockAt(x, y, z).getType() != Material.AIR) {
                    minY = Math.min(minY, y);
                    maxY = Math.max(maxY, y);
                }
                if (p.getWorld().getBlockAt(x, y, z).getType() == Material.CHEST) {
                    if (world2D.getMiddleIsland() != null && world2D.getMiddleIsland().containsLocation(new Block2DLocation(x, z))) {
                        setupGame.addChest(new SetupChestPosition(ChestType.BETTER, new SerializableLocation(p.getWorld().getName(), x, y, z)));
                        chestsOnMiddleIsland++;
                        continue;
                    }
                    setupGame.addChest(new SetupChestPosition(ChestType.NORMAL, new SerializableLocation(p.getWorld().getName(), x, y, z)));
                }
            }
        }
        //spec spawn
        double specX = ((world2D.getMinX() + world2D.getMaxX()) / 2) + 0.5;
        double specZ = ((world2D.getMinZ() + world2D.getMaxZ()) / 2) + 0.5;
        int specY = location.getBlockY() + 80;
        setupGame.setSpectatorSpawn(new SerializableLocation(p.getWorld().getName(), specX, specY, specZ));
        //spawns
        int number = 0;
        for (Island2DLocation island2DLocation : world2D.getIslandLocations()) {
            if (island2DLocation.getSpawnPoint() == null)
                continue;
            setupGame.addTeam(new SetupTeamSpawn(number, new SerializableLocation(location.getWorld().getName(), island2DLocation.getSpawnPoint().getX() + 0.5, location.getBlockY() + 0.5, island2DLocation.getSpawnPoint().getZ() + 0.5, island2DLocation.getYaw(), 0)));
            number++;
        }
        if (setupGame.getTeams().size() < 2){
            p.sendMessage(Skywars.PREFIX + "§cEs wurde nur ein Team erkannt.");
            return;
        }
        Skywars.getInstance().getSetupGameLoader().saveGame(setupGame);


        //schematic erzeugen
        /*
        p.sendMessage(Skywars.PREFIX + "§eErzeuge Schematic...");
        Island2DLocation firstIsland = world2D.getIslandLocations().get(0);
        Island2DLocation middleisland = world2D.getMiddleIsland();
        Schematic islandSchematic = Schematic.createNewSchematic(world.getBlockAt(firstIsland.getMinX(), minY, firstIsland.getMinZ()).getLocation(), world.getBlockAt(firstIsland.getMaxX(), maxY, firstIsland.getMaxZ()).getLocation());
        Schematic middleSchematic = Schematic.createNewSchematic(world.getBlockAt(middleisland.getMinX(), minY, middleisland.getMinZ()).getLocation(), world.getBlockAt(middleisland.getMaxX(), maxY, middleisland.getMaxZ()).getLocation());
        //create middle location
        SerializableLocation middleLocation = new SerializableLocation(null, middleisland.getMinX(), minY, middleisland.getMinZ());
        //create all island locations
        List<IslandPosition> islandPositions = new ArrayList<>();
        for (Island2DLocation island2DLocation : world2D.getIslandLocations()){
            if (island2DLocation.getSpawnPoint() == null)
                continue;
            islandPositions.add(new IslandPosition(new SerializableLocation(null, island2DLocation.getMinX(), minY, island2DLocation.getMinZ()), island2DLocation.getDirection()));
        }
        MapSchematics mapSchematics = new MapSchematics(middleSchematic, islandSchematic, middleLocation, islandPositions);
        try {
            new MapSchematicFile(new File(Skywars.GAMES_DIR + setupGame.getName() + ".swschematic")).saveMapSchematic(mapSchematics);
        } catch (IOException e) {
            e.printStackTrace();
        }
        */
        world2D.markNotAirBlocks();
        p.sendMessage(Skywars.PREFIX + "§e" + setupGame.getName() + " §7wurde erstellt.");
        p.sendMessage(Skywars.PREFIX + "Kisten§8: §e" + setupGame.getChests().size() + " §7davon in der Mitte§8: §e" + chestsOnMiddleIsland);
        p.sendMessage(Skywars.PREFIX + "Teams§8: §e" + setupGame.getTeams().size());
        p.sendMessage(Skywars.PREFIX + "Spectator-Spawn: §eX:" + specX + " Y:" + specY + " Z:" + specZ);
        p.sendMessage(Skywars.PREFIX + "Dauer§8: §e" + (System.currentTimeMillis() - ms) + "ms");
    }


}
