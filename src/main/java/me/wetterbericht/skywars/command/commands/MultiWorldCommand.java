package me.wetterbericht.skywars.command.commands;

import me.wetterbericht.skywars.Skywars;
import me.wetterbericht.worldloader.WorldLoader;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class MultiWorldCommand implements CommandExecutor {


    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if (commandSender instanceof Player) {
            Player p = (Player) commandSender;
            if (p.hasPermission("multiworlds.use")) {
                if (args.length < 1) {
                    p.sendMessage(Skywars.PREFIX + "§7/mw §e<list/tp/save> [name]");
                    return true;
                }

                if (args[0].equalsIgnoreCase("list")) {
                    p.sendMessage(Skywars.PREFIX + " §eWorlds");
                    for (World w : Bukkit.getWorlds()) {
                        p.sendMessage("§8- §e" + w.getName());
                    }
                }


                if (args[0].equalsIgnoreCase("save")) {
                    if (args.length != 2) {
                        p.sendMessage(Skywars.PREFIX + "§7/mw §esave <name>");
                        return true;
                    }
                    World w = Bukkit.getWorld(args[1]);
                    if (w == null) {
                        p.sendMessage(Skywars.PREFIX + "§cDiese Welt existiert nicht");
                        return true;
                    }
                    String name = w.getName();
                    for (Player pl : w.getPlayers()){
                        pl.teleport(Bukkit.getWorlds().get(0).getSpawnLocation());
                    }
                    Bukkit.unloadWorld(w, true);
                    WorldLoader.load(name);
                    p.sendMessage(Skywars.PREFIX + "§7Welt gespeichert.");
                }


                if (args[0].equalsIgnoreCase("tp")) {
                    if (args.length != 2) {
                        p.sendMessage(Skywars.PREFIX + "§7/mw §etp <name>");
                        return true;
                    }
                    World w = Bukkit.getWorld(args[1]);
                    if (w == null) {
                        p.sendMessage(Skywars.PREFIX + "§cDiese Welt existiert nicht");
                        return true;
                    }
                    p.teleport(w.getSpawnLocation());
                }
            }
        }
        return true;
    }
}
