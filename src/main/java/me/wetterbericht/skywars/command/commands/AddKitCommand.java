package me.wetterbericht.skywars.command.commands;

import me.wetterbericht.inventoryapi.api.InventoryBuilder;
import me.wetterbericht.inventoryapi.api.InventoryItem;
import me.wetterbericht.inventoryapi.api.anvilgui.AnvilTextGUI;
import me.wetterbericht.inventoryapi.api.clicklistener.ClickListener;
import me.wetterbericht.inventoryapi.utils.ItemBuilder;
import me.wetterbericht.inventoryapi.utils.ItemCreator;
import me.wetterbericht.skywars.Skywars;
import me.wetterbericht.skywars.kits.Kit;
import me.wetterbericht.skywars.kits.KitsFileHandler;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class AddKitCommand implements CommandExecutor {

    //addkit <Name> <Preis>

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if (!(commandSender instanceof Player))
            return true;
        Player p = (Player) commandSender;
        if (!p.hasPermission("skywars.setup"))
            return true;
        if (args.length != 2) {
            sendHelp(p);
            return true;
        }
        int price;
        try {
            price = Integer.parseInt(args[1]);
        } catch (NumberFormatException e) {
            sendHelp(p);
            return true;
        }


        //save items
        Kit kit = new Kit(args[0], price);
        kit.addArmor(ItemBuilder.formItemStack(p.getInventory().getHelmet())).addArmor(ItemBuilder.formItemStack(p.getInventory().getChestplate()))
                .addArmor(ItemBuilder.formItemStack(p.getInventory().getLeggings())).addArmor(ItemBuilder.formItemStack(p.getInventory().getBoots()));
        for (int i = 0; i < p.getInventory().getContents().length; i++) {
            ItemStack itemStack = p.getInventory().getContents()[i];
            if (itemStack == null)
                continue;
            if(i == 0){
                kit.setIconMaterial(itemStack.getType());
                continue;
            }
            kit.addItem(ItemBuilder.formItemStack(itemStack));
        }
        if (kit.getIcon() == null){
            sendHelp(p);
            return true;
        }
        p.openInventory(getInventory(kit));
        return true;
    }

    public Inventory getInventory(Kit kit) {
        InventoryBuilder inventoryBuilder = new InventoryBuilder(Skywars.PREFIX + "Kit §8» §e" + kit.getName(), 9);
        inventoryBuilder.fillAll(ItemCreator.createGlass(" ", DyeColor.BLACK));
        inventoryBuilder.setInventoryItem(1, new InventoryItem(ItemCreator.create(Material.NAME_TAG, "§8» §eBeschreibungszeile §7hinzufügen")).addListener((p, clickAction) -> showAddDescriptionLineAnvil(p, kit)));
        inventoryBuilder.setInventoryItem(7, new InventoryItem(ItemCreator.createWool("§8» §aFertig", DyeColor.GREEN, 1)).addListener((p, clickAction) -> {
            Skywars.getInstance().getKitManager().getKitsContainer().addKit(kit);
            new KitsFileHandler().saveToFile(Skywars.getInstance().getKitManager().getKitsContainer());
            Skywars.getInstance().getKitManager().loadKitsContainerFile();
            p.closeInventory();
            p.sendMessage(Skywars.PREFIX + "Kit gespeichert.");
        }));
        inventoryBuilder.buildInv();
        return inventoryBuilder.getInventory();
    }

    public void showAddDescriptionLineAnvil(Player player, Kit kit) {
        new AnvilTextGUI(player, s -> {
            kit.addLore("§8» §7" + s.replaceAll("&", "§"));
            player.openInventory(getInventory(kit));
            return true;
        });
    }

    public void sendHelp(Player p) {
        p.sendMessage(Skywars.PREFIX + "Use§8: §e/addKit <Name> <Preis>");
        p.sendMessage("§8- §7Ordne die Items in deinem Inventar so wie sie später im Kit sein sollen.");
        p.sendMessage("§8- §7Beim Ausführen wird das Item, welches auf dem ersten Slot liegt als Icon verwendet.");
        p.sendMessage("§8- §7Das Item, welches auf dem ersten Slot ist wird §cnicht §7Teil des Kits sein.");
    }


}
