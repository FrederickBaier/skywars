package me.wetterbericht.skywars.command.commands;


import me.wetterbericht.skywars.Skywars;
import me.wetterbericht.skywars.setupinventories.GameSetupInventory;
import me.wetterbericht.skywars.setupinventories.MainSetupInventory;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class GameSetupCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player))
            return true;
        Player p = (Player)sender;
        if (!p.hasPermission("skywars.setup"))
            return true;
        if (args.length != 1){
            p.sendMessage(Skywars.PREFIX + "Nutze §8: §e/gamesetup <name>");
            return true;
        }
        GameSetupInventory.openGameSetupInventory(p, args[0]);
        return true;
    }
}
