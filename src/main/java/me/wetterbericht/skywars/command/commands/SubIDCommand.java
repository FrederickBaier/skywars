package me.wetterbericht.skywars.command.commands;

import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SubIDCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (!(commandSender instanceof Player))
            return true;
        Player p = (Player)commandSender;
        Block block = p.getLocation().add(0,-1,0).getBlock();
        p.sendMessage(block.getTypeId() + " : " + block.getData());
        return true;
    }
}
