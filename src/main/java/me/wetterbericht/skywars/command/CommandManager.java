package me.wetterbericht.skywars.command;

import me.wetterbericht.skywars.BukkitPluginMain;
import me.wetterbericht.skywars.command.commands.*;

public class CommandManager {

    private BukkitPluginMain bukkitPluginMain = BukkitPluginMain.getInstance();

    public void registerCommands(){
        bukkitPluginMain.getCommand("mainsetup").setExecutor(new MainSetupCommand());
        bukkitPluginMain.getCommand("creategame").setExecutor(new CreateGameCommand());
        bukkitPluginMain.getCommand("gamesetup").setExecutor(new GameSetupCommand());
        bukkitPluginMain.getCommand("addkit").setExecutor(new AddKitCommand());
        bukkitPluginMain.getCommand("delKit").setExecutor(new DelKitCommand());
        bukkitPluginMain.getCommand("addChestItem").setExecutor(new AddChestItemCommand());
        bukkitPluginMain.getCommand("chestItems").setExecutor(new ChestItemsCommand());
        bukkitPluginMain.getCommand("addLobbyTablistTeam").setExecutor(new AddLobbyTabListTeamCommand());
        bukkitPluginMain.getCommand("lobbyTablistTeams").setExecutor(new LobbyTablistTeamsCommand());
        bukkitPluginMain.getCommand("start").setExecutor(new StartCommand());
        bukkitPluginMain.getCommand("coins").setExecutor(new CoinsCommand());
        bukkitPluginMain.getCommand("stats").setExecutor(new StatsCommand());
        bukkitPluginMain.getCommand("skywars").setExecutor(new SkywarsCommand());
        bukkitPluginMain.getCommand("setup").setExecutor(new SetupCommand());
        bukkitPluginMain.getCommand("autosetup").setExecutor(new AutoSetupCommand());
        bukkitPluginMain.getCommand("mw").setExecutor(new MultiWorldCommand());
        bukkitPluginMain.getCommand("delgame").setExecutor(new DelGameCommand());
    }

}
