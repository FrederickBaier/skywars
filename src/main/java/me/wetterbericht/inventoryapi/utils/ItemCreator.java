package me.wetterbericht.inventoryapi.utils;

import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.Wool;

import java.util.ArrayList;


public class ItemCreator {
	

	
	public static ItemStack create(Material mat, String name,int amount, String lore1, String lore2) {
		ItemStack item = new ItemStack(mat);
		item.setAmount(amount);
		ItemMeta meta = item.getItemMeta();
		if (name != null)
			meta.setDisplayName(name);
		ArrayList<String> list = new ArrayList<String>();
		if (lore1 != null)
			list.add(lore1);
		if (lore2 != null)
			list.add(lore2);
		meta.setLore(list);
		item.setItemMeta(meta);
		return item;
	}
	public static ItemStack addEchantment(ItemStack i, Enchantment e, int level){
		i.getItemMeta().addEnchant(e, level, true);
		return i;
	}

	public static ItemStack create(Material mat, String name, int amount) {
		return create(mat, name,amount, null, null);
	}
	public static ItemStack create(Material mat, String name) {
		return create(mat, name,1, null, null);
	}

	public static ItemStack createGlass(String name, DyeColor color) {
		ItemStack item = new ItemStack(Material.STAINED_GLASS_PANE, 1, color.getData());
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(name);
		item.setItemMeta(meta);
		return item;
	}
	
	public static ItemStack createWool(String name, DyeColor color, int amount) {
		Wool wool = new Wool(color);
		ItemStack item = wool.toItemStack();
		item.setAmount(amount);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(name);
		item.setItemMeta(meta);
		return item;
	}
	
	public static ItemStack createWoolWithLore(String name, DyeColor color, int amount, ArrayList<String> lore) {
		Wool wool = new Wool(color);
		ItemStack item = wool.toItemStack();
		item.setAmount(amount);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(name);
		meta.setLore(lore);
		item.setItemMeta(meta);
		return item;
	}
	
	public static ItemStack createGlassBlock(String name, DyeColor color) {
		ItemStack item = new ItemStack(Material.GLASS, 1, color.getData());
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(name);
		item.setItemMeta(meta);
		return item;
	}
}
