package me.wetterbericht.inventoryapi.utils;

@FunctionalInterface
public interface Callback<R, T> {

    R done(T t);

}
