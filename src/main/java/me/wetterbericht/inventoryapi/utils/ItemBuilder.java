package me.wetterbericht.inventoryapi.utils;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.MaterialData;

import java.util.HashMap;
import java.util.List;

@Getter
public class ItemBuilder {

    private String name = null;

    private Material material;

    private int amount = 1;

    private byte data = 0;

    private HashMap<String, Integer> enchantments = new HashMap<>();

    private List<String> lore;

    public ItemBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public ItemBuilder setMaterial(Material material) {
        this.material = material;
        return this;
    }

    public ItemBuilder setAmount(int amount) {
        this.amount = amount;
        return this;
    }

    public ItemBuilder setData(byte data) {
        this.data = data;
        return this;
    }

    public ItemBuilder addEnchantment(Enchantment ench, int level) {
        enchantments.put(ench.getName(), level);
        return this;
    }

    public ItemBuilder setLore(List<String> list){
        this.lore = list;
        return this;
    }

    public ItemStack buildItemStack() {
        ItemStack itemStack = new ItemStack(material, amount);
        MaterialData materialData = itemStack.getData();
        materialData.setData(data);
        itemStack = materialData.toItemStack(amount);
        ItemMeta meta = itemStack.getItemMeta();
        if (name != null)
            meta.setDisplayName(name);
        meta.setLore(lore);
        for (String s : enchantments.keySet()) {
            meta.addEnchant(Enchantment.getByName(s), enchantments.get(s), true);
        }
        itemStack.setItemMeta(meta);

        itemStack.setData(materialData);
        return itemStack;
    }

    public static ItemBuilder formItemStack(ItemStack itemStack){
        if (itemStack == null)
            return null;
        ItemBuilder itemBuilder = new ItemBuilder();
        itemBuilder.setMaterial(itemStack.getType());
        itemBuilder.setAmount(itemStack.getAmount());
        itemBuilder.setData(itemStack.getData().getData());
        ItemMeta itemMeta = itemStack.getItemMeta();
        if (itemMeta.hasDisplayName())
            itemBuilder.setName(itemMeta.getDisplayName());
        for (Enchantment enchantment : itemMeta.getEnchants().keySet()) {
            itemBuilder.addEnchantment(enchantment, itemMeta.getEnchantLevel(enchantment));
        }
        return itemBuilder;
    }


}

