package me.wetterbericht.inventoryapi.api;

import lombok.Getter;
import me.wetterbericht.inventoryapi.api.clicklistener.ClickListener;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

public class InventoryItem {

    @Getter
    private ItemStack itemStack;

    private ArrayList<ClickListener> clickListeners = new ArrayList<>();

    public InventoryItem(ItemStack itemStack){
        this.itemStack = itemStack;
    }

    public void onClick(Player p, ClickType clickAction){
        clickListeners.forEach(clickListener -> clickListener.onClick(p, clickAction));
    }

    public me.wetterbericht.inventoryapi.api.InventoryItem addListener(ClickListener clickListener){
        clickListeners.add(clickListener);
        return this;
    }

}
