package me.wetterbericht.inventoryapi.api.clicklistener;


import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;

@FunctionalInterface
public interface ClickListener {

   void onClick(Player p, ClickType clickAction);

}
