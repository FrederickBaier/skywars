package me.wetterbericht.inventoryapi.api.anvilgui;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public interface IAnvilGUI {

    void open();

    Player getPlayer();

    void setSlot(AnvilSlot slot, ItemStack item);

}
