package me.wetterbericht.inventoryapi.api.anvilgui.versions;

import me.wetterbericht.inventoryapi.api.anvilgui.AnvilClickEvent;
import me.wetterbericht.inventoryapi.api.anvilgui.AnvilClickEventHandler;
import me.wetterbericht.inventoryapi.api.anvilgui.AnvilSlot;
import me.wetterbericht.inventoryapi.api.anvilgui.IAnvilGUI;
import me.wetterbericht.skywars.BukkitPluginMain;
import net.minecraft.server.v1_8_R3.*;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.HashMap;

/**
 * Created by chasechocolate.
 */
public class AnvilGUIv1_8_R3 implements IAnvilGUI {
    private class AnvilContainer extends ContainerAnvil {
        public AnvilContainer(EntityHuman entity){

            super(entity.inventory, entity.world,new BlockPosition(0, 0, 0), entity);
        }

        @Override
        public boolean a(EntityHuman entityhuman){
            return true;
        }
    }







    private Player player;

    private AnvilClickEventHandler handler;

    private HashMap<AnvilSlot, ItemStack> items = new HashMap<AnvilSlot, ItemStack>();

    private Inventory inv;

    private Listener listener;

    public AnvilGUIv1_8_R3(Player player, final AnvilClickEventHandler handler){
        this.player = player;
        this.handler = handler;

        this.listener = new Listener(){
            @EventHandler
            public void onInventoryClick(InventoryClickEvent event){
                if(event.getWhoClicked() instanceof Player){
                    Player clicker = (Player) event.getWhoClicked();

                    if(event.getInventory().equals(inv)){
                        event.setCancelled(true);

                        ItemStack item = event.getCurrentItem();
                        int slot = event.getRawSlot();
                        String name = "";

                        if(item != null){
                            if(item.hasItemMeta()){
                                ItemMeta meta = item.getItemMeta();

                                if(meta.hasDisplayName()){
                                    name = meta.getDisplayName();
                                }
                            }
                        }

                        AnvilClickEvent clickEvent = new AnvilClickEvent(AnvilSlot.bySlot(slot), name);

                        handler.onAnvilClick(clickEvent);

                        if(clickEvent.getWillClose()){
                            event.getWhoClicked().closeInventory();
                        }

                        if(clickEvent.getWillDestroy()){
                            destroy();
                        }
                    }
                }
            }

            @EventHandler
            public void onInventoryClose(InventoryCloseEvent event){
                if(event.getPlayer() instanceof Player){
                    Player player = (Player) event.getPlayer();
                    Inventory inv = event.getInventory();

                    if(inv.equals(AnvilGUIv1_8_R3.this.inv)){
                        inv.clear();
                        destroy();
                    }
                }
            }

            @EventHandler
            public void onPlayerQuit(PlayerQuitEvent event){
                if(event.getPlayer().equals(getPlayer())){
                    destroy();
                }
            }
        };

        Bukkit.getPluginManager().registerEvents(listener, BukkitPluginMain.getInstance()); //Replace with instance of main class
    }

    @Override
    public Player getPlayer(){
        return player;
    }

    @Override
    public void setSlot(AnvilSlot slot, ItemStack item){
        items.put(slot, item);
    }

    @Override
    public void open(){
        EntityPlayer p = ((CraftPlayer) player).getHandle();

        AnvilContainer container = new AnvilContainer(p);

        //Set the items to the items from the inventory given
        inv = container.getBukkitView().getTopInventory();

        for(AnvilSlot slot : items.keySet()){
            inv.setItem(slot.getSlot(), items.get(slot));
        }

        //Counter stuff that the game uses to keep track of inventories
        int c = p.nextContainerCounter();

        //Send the packet
        p.playerConnection.sendPacket(new PacketPlayOutOpenWindow(c, "minecraft:anvil", new ChatMessage("Repairing", new Object[]{}), 0));

        //Set their active container to the container
        p.activeContainer = container;

        //Set their active container window id to that counter stuff
        p.activeContainer.windowId = c;

        //Add the slot listener
        p.activeContainer.addSlotListener(p);
    }

    public void destroy(){
        player = null;
        handler = null;
        items = null;

        HandlerList.unregisterAll(listener);

        listener = null;
    }
}