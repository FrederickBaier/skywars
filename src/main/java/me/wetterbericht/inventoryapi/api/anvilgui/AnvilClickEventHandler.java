package me.wetterbericht.inventoryapi.api.anvilgui;

public interface AnvilClickEventHandler {

    void onAnvilClick(AnvilClickEvent clickEvent);

}
