package me.wetterbericht.inventoryapi.api.anvilgui;
import me.wetterbericht.inventoryapi.utils.Callback;
import me.wetterbericht.inventoryapi.utils.ItemCreator;
import me.wetterbericht.skywars.utils.ReflectionUtils;
import org.bukkit.Material;
import org.bukkit.entity.Player;


public class AnvilTextGUI extends ReflectionUtils{
    private static Class<?> anvilGuiClass = null;
    static {
        try {
           anvilGuiClass = Class.forName("me.wetterbericht.inventoryapi.api.anvilgui.versions.AnvilGUI" + ReflectionUtils.VERSION);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
    public AnvilTextGUI(Player player, Callback<Boolean, String> complete) {
        IAnvilGUI iAnvilGUI = (IAnvilGUI) invokeConstructor(anvilGuiClass, new Class[]{Player.class, AnvilClickEventHandler.class}, player, (AnvilClickEventHandler) event -> {
            if (event.getSlot() == AnvilSlot.OUTPUT && complete.done(event.getName())){
                event.setWillClose(false);
                event.setWillDestroy(true);
            } else {
                event.setWillDestroy(false);
                event.setWillClose(false);
            }
        });

        iAnvilGUI.setSlot(AnvilSlot.INPUT_LEFT, ItemCreator.create(Material.NAME_TAG, "Text hier."));
        iAnvilGUI.open();
    }
}
