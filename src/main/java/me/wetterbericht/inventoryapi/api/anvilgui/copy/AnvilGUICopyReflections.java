package me.wetterbericht.inventoryapi.api.anvilgui.copy;

import me.wetterbericht.skywars.BukkitPluginMain;
import me.wetterbericht.skywars.utils.ReflectionUtils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.HashMap;

/**
 * Created by chasechocolate.
 */
public class AnvilGUICopyReflections extends ReflectionUtils {

    private static Class<?> containerAnvilClazz = ReflectionUtils.reflectMinecraftClazz("ContainerAnvil");
    private static Class<?> entityHumanClazz = ReflectionUtils.reflectMinecraftClazz("EntityHuman");
    private static Class<?> playerInventoryClazz = ReflectionUtils.reflectMinecraftClazz("PlayerInventory");
    private static Class<?> worldClazz = ReflectionUtils.reflectMinecraftClazz("World");
    private static Class<?> blockPositionClazz = ReflectionUtils.reflectMinecraftClazz("BlockPosition");
    private static Class<?> packetPlayOutOpenWindowClazz = ReflectionUtils.reflectMinecraftClazz("PacketPlayOutOpenWindow");
    private static Class<?> packetClazz = ReflectionUtils.reflectMinecraftClazz("Packet");
    private static Class<?> entityPlayerClazz = ReflectionUtils.reflectMinecraftClazz("EntityPlayer");
    private static Class<?> craftPlayerClazz = ReflectionUtils.reflectCraftClazz("entity.CraftPlayer");
    private static Class<?> chatMessageClazz = ReflectionUtils.reflectMinecraftClazz("ChatMessage");
    private static Class<?> entityClazz = ReflectionUtils.reflectMinecraftClazz("Entity");
    private static Class<?> iCraftingClazz = ReflectionUtils.reflectMinecraftClazz("ICrafting");
    private static Class<?> iChatBaseComponentClazz = ReflectionUtils.reflectMinecraftClazz("IChatBaseComponent");
    private static Class<?> containerClazz = ReflectionUtils.reflectMinecraftClazz("Container");



    private class AnvilContainer {
       // net.minecraft.server.v1_8_R3.WorldServer
        public Object containerAnvil;
        public AnvilContainer(Object entity) {
            Object inventory = getValue(entityHumanClazz, entity, "inventory");
            Object world = getValue(entityClazz, entity, "world");
            Object blockPosition = invokeConstructor(blockPositionClazz, new Class[]{int.class, int.class, int.class}, 0, 0, 0);
            containerAnvil = invokeConstructor(containerAnvilClazz, new Class[]{playerInventoryClazz, worldClazz, blockPositionClazz, entityHumanClazz}, inventory, world, blockPosition, entity);
        }



    }

    public enum AnvilSlot {
        INPUT_LEFT(0),
        INPUT_RIGHT(1),
        OUTPUT(2);

        private int slot;

        private AnvilSlot(int slot) {
            this.slot = slot;
        }

        public int getSlot() {
            return slot;
        }

        public static AnvilSlot bySlot(int slot) {
            for (AnvilSlot anvilSlot : values()) {
                if (anvilSlot.getSlot() == slot) {
                    return anvilSlot;
                }
            }

            return null;
        }
    }

    public class AnvilClickEvent {
        private AnvilSlot slot;

        private String name;

        private boolean close = true;
        private boolean destroy = true;

        public AnvilClickEvent(AnvilSlot slot, String name) {
            this.slot = slot;
            this.name = name;
        }

        public AnvilSlot getSlot() {
            return slot;
        }

        public String getName() {
            return name;
        }

        public boolean getWillClose() {
            return close;
        }

        public void setWillClose(boolean close) {
            this.close = close;
        }

        public boolean getWillDestroy() {
            return destroy;
        }

        public void setWillDestroy(boolean destroy) {
            this.destroy = destroy;
        }
    }

    public interface AnvilClickEventHandler {
        void onAnvilClick(AnvilClickEvent event);
    }

    private Player player;

    private AnvilClickEventHandler handler;

    private HashMap<AnvilSlot, ItemStack> items = new HashMap<AnvilSlot, ItemStack>();

    private Inventory inv;

    private Listener listener;

    public AnvilGUICopyReflections(Player player, final AnvilClickEventHandler handler) {
        this.player = player;
        this.handler = handler;

        this.listener = new Listener() {
            @EventHandler
            public void onInventoryClick(InventoryClickEvent event) {
                if (event.getWhoClicked() instanceof Player) {
                    Player clicker = (Player) event.getWhoClicked();

                    if (event.getInventory().equals(inv)) {
                        event.setCancelled(true);

                        ItemStack item = event.getCurrentItem();
                        int slot = event.getRawSlot();
                        String name = "";

                        if (item != null) {
                            if (item.hasItemMeta()) {
                                ItemMeta meta = item.getItemMeta();

                                if (meta.hasDisplayName()) {
                                    name = meta.getDisplayName();
                                }
                            }
                        }

                        AnvilClickEvent clickEvent = new AnvilClickEvent(AnvilSlot.bySlot(slot), name);

                        handler.onAnvilClick(clickEvent);

                        if (clickEvent.getWillClose()) {
                            event.getWhoClicked().closeInventory();
                        }

                        if (clickEvent.getWillDestroy()) {
                            destroy();
                        }
                    }
                }
            }

            @EventHandler
            public void onInventoryClose(InventoryCloseEvent event) {
                if (event.getPlayer() instanceof Player) {
                    Player player = (Player) event.getPlayer();
                    Inventory inv = event.getInventory();

                    if (inv.equals(AnvilGUICopyReflections.this.inv)) {
                        inv.clear();
                        destroy();
                    }
                }
            }

            @EventHandler
            public void onPlayerQuit(PlayerQuitEvent event) {
                if (event.getPlayer().equals(getPlayer())) {
                    destroy();
                }
            }
        };

        Bukkit.getPluginManager().registerEvents(listener, BukkitPluginMain.getInstance()); //Replace with instance of main class
    }

    public Player getPlayer() {
        return player;
    }

    public void setSlot(AnvilSlot slot, ItemStack item) {
        items.put(slot, item);
    }

    public void open() {
        System.out.println(player);
        System.out.println(craftPlayerClazz);
        Object craftPlayer = craftPlayerClazz.cast(player);
        Object entityPlayer = invokeMethod(craftPlayerClazz, craftPlayer, "getHandle", new Class[]{});
        AnvilContainer anvilContainer = new AnvilContainer(entityPlayer);
        Object bukkitView = invokeMethod(containerAnvilClazz, anvilContainer.containerAnvil, "getBukkitView", new Class[]{});
        inv = (Inventory) invokeMethod(bukkitView.getClass(), bukkitView, "getTopInventory", new Class[]{});


        for (AnvilSlot slot : items.keySet()) {
            inv.setItem(slot.getSlot(), items.get(slot));
        }

        //Counter stuff that the game uses to keep track of inventories
        int c = (int) invokeMethod(entityPlayerClazz, entityPlayer, "nextContainerCounter", new Class[]{});
        Object playerConnection = getValue(entityPlayerClazz, entityPlayer, "playerConnection");
        Object chatMessage = invokeConstructor(chatMessageClazz, new Class[]{String.class, Object[].class}, "Repairing", new Object[]{});
        Object packet = invokeConstructor(packetPlayOutOpenWindowClazz, new Class[]{int.class, String.class, iChatBaseComponentClazz, int.class}, c, "minecraft:anvil", chatMessage, 0);
        invokeMethod(playerConnection.getClass(), playerConnection, "sendPacket", new Class[]{packetClazz}, packet);
        //playerConnection.getClass().getDeclaredMethod("sendPacket", ReflectionUtils.reflectCraftClazz("Packet")).invoke(playerConnection, packet);
        setValue(entityHumanClazz, entityPlayer, anvilContainer.containerAnvil, "activeContainer");


        setValue(containerClazz, anvilContainer.containerAnvil, c, "windowId");


        invokeMethod(containerAnvilClazz, anvilContainer.containerAnvil, "addSlotListener", new Class[]{iCraftingClazz}, entityPlayer);




    }

    public void destroy() {
        player = null;
        handler = null;
        items = null;

        HandlerList.unregisterAll(listener);

        listener = null;
    }
}