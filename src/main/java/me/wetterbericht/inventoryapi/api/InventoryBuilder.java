package me.wetterbericht.inventoryapi.api;


import lombok.Getter;
import lombok.Setter;
import me.wetterbericht.skywars.BukkitPluginMain;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;

public class InventoryBuilder implements Listener {

    @Setter
    @Getter
    private String title;

    @Getter
    @Setter
    private int size;

    public InventoryBuilder(String title, int size){
        this.size = size;
        this.title = title;
    }

    private HashMap<Integer, InventoryItem> inventoryItems = new HashMap<>();

    @Getter
    private Inventory inventory;

    public void setInventoryItem(int i, InventoryItem inventoryItem){
        inventoryItems.put(i, inventoryItem);
    }

    public void fillAll(ItemStack itemStack){
        for (int i = 0; i < size; i++){
            setInventoryItem(i, new InventoryItem(itemStack));
        }
    }

    public void fillAllEmptySlots(ItemStack itemStack){
        for (int i = 0; i < size; i++){
            if (inventoryItems.get(i) == null){
                setInventoryItem(i, new InventoryItem(itemStack));
            }
        }
    }

    public void addInventoryItem(InventoryItem inventoryItem){
        for (int i = 0; i < size; i++){
            if (inventoryItems.get(i) == null){
                setInventoryItem(i, inventoryItem);
                return;
            }
        }
    }

    public void buildInv(){
        inventory = Bukkit.createInventory(null, size, title);
        for (Integer integer : inventoryItems.keySet()) {
            inventory.setItem(integer, inventoryItems.get(integer).getItemStack());
        }
        BukkitPluginMain.getInstance().getServer().getPluginManager().registerEvents(this, BukkitPluginMain.getInstance());
    }

    @EventHandler
    public void on(InventoryClickEvent e){
        if (e.getClickedInventory() == null)
            return;
        if (!e.getClickedInventory().equals(inventory))
            return;
        e.setCancelled(true);
        InventoryItem inventoryItem = inventoryItems.get(e.getSlot());
        if (inventoryItem != null){
            inventoryItem.onClick((Player) e.getWhoClicked(), e.getClick());
        }
    }

    @EventHandler
    public void on(InventoryCloseEvent e){
        if (!e.getInventory().equals(inventory))
            return;
        HandlerList.unregisterAll(this);
    }

}
