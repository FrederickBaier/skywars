package me.wetterbericht.scoreboardapi;

import org.bukkit.entity.Player;

import java.util.HashMap;

public class SimplePlayerScoreboardManager {

    private HashMap<Player, SimpleScoreboard> simpleScoreboards = new HashMap<>();


    public SimpleScoreboard getNewScoreboard(Player player, String title){
        SimpleScoreboard simpleScoreboard = new SimpleScoreboard(title);
        simpleScoreboards.put(player, simpleScoreboard);
        simpleScoreboard.sendSidebar(player);
        return simpleScoreboard;
    }

    public SimpleScoreboard getScorebaord(Player player){
        return simpleScoreboards.containsKey(player) ? simpleScoreboards.get(player) : getNewScoreboard(player, " ");
    }

    public boolean hasScoreboard(Player player){
        return simpleScoreboards.containsKey(player);
    }

}
