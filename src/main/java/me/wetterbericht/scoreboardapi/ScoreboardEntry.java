package me.wetterbericht.scoreboardapi;


import lombok.Getter;
import net.minecraft.server.v1_8_R3.*;

@Getter
public class ScoreboardEntry {

    private SimpleScoreboard simpleScoreboard;

    private String teamName;

    private String text;

    private String changeableText;

    private int score;

    public ScoreboardEntry(SimpleScoreboard simpleScoreboard, String teamName, String text, String changeableText, int score) {
        this.simpleScoreboard = simpleScoreboard;
        this.teamName = teamName;
        this.text = text;
        this.changeableText = changeableText;
        this.score = score;
    }


    public Packet getTeamAddPacket() {
        ScoreboardTeam scoreboardTeam = new ScoreboardTeam(simpleScoreboard.getScoreboard(), this.teamName);
        scoreboardTeam.setSuffix(this.changeableText);
        scoreboardTeam.getPlayerNameSet().add(this.text + this.teamName);
        return new PacketPlayOutScoreboardTeam(scoreboardTeam, 0);
    }

    public Packet getTeamSuffixChangePacket() {
        ScoreboardTeam scoreboardTeam = new ScoreboardTeam(simpleScoreboard.getScoreboard(), this.teamName);
        scoreboardTeam.setSuffix(this.changeableText);
        scoreboardTeam.getPlayerNameSet().add(this.text + this.teamName);
        return new PacketPlayOutScoreboardTeam(scoreboardTeam, 2);
    }

    public Packet getTeamRemovePacket() {
        ScoreboardTeam scoreboardTeam = new ScoreboardTeam(simpleScoreboard.getScoreboard(), this.teamName);
        scoreboardTeam.setSuffix(this.changeableText);
        return new PacketPlayOutScoreboardTeam(scoreboardTeam, 1);
    }

    public Packet getAddAndScoreChangePacket() {
        ScoreboardScore scoreboardScore = new ScoreboardScore(this.simpleScoreboard.getScoreboard(), this.simpleScoreboard.getObjective(), this.text + this.teamName);
        scoreboardScore.addScore(this.score);
        return new PacketPlayOutScoreboardScore(scoreboardScore);
    }

    public Packet getRemovePacket() {
        return new PacketPlayOutScoreboardScore(this.text + this.teamName);
    }

    public ScoreboardEntry setScore(int score) {
        this.score = score;
        this.simpleScoreboard.sendPacketToViewers(getAddAndScoreChangePacket());
        return this;
    }

    public void setChangeableText(String changeableText) {
        if (changeableText.length() > 16)
            throw new IllegalArgumentException("The text can not be longer than 16 characters.");
        this.changeableText = changeableText;
        this.simpleScoreboard.sendPacketToViewers(getTeamSuffixChangePacket());
    }


}
