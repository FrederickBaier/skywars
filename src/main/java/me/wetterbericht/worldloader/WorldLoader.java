package me.wetterbericht.worldloader;

import java.io.File;
import java.io.IOException;

import me.wetterbericht.skywars.BukkitPluginMain;
import org.apache.commons.io.FileUtils;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.World.Environment;
import org.bukkit.WorldCreator;





public class WorldLoader {

	public static void loadWorlds(){
		
		File file = new File("worlds");
		file.mkdirs();
		for (File f : file.listFiles()){
			try {
				FileUtils.copyDirectory(f, new File(f.getName()));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		Bukkit.getScheduler().runTaskLater(BukkitPluginMain.getInstance(), () -> {
			for (File f : file.listFiles()){
				if (f.isDirectory()){
					load(f.getName());
					System.out.println("[Skywars] Welt: " + f.getName() + " geladen.");
				}
			}

		}, 10);
	}
	
	public static void load(String name){
		Bukkit.createWorld(WorldCreator.name(name).environment(Environment.NORMAL));
	}
	
}
